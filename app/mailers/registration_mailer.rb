class RegistrationMailer < ActionMailer::Base
  default_url_options[:host] = 'viewcy.com'
  default from: "noreply@viewcy.com"
  
  def thanks(registration)
    @registration = registration
    mail(to: registration.user_email, subject: 'Thank you for registering')
  end
end
