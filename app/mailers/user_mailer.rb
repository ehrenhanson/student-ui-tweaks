class UserMailer < ActionMailer::Base
  default_url_options[:host] = 'viewcy.com'
  default from: "noreply@viewcy.com"
  
  def reset_password(user)
    @user = user    
    mail(to: @user.email, subject: '[Viewcy] Reset Password')
  end
  
  def invite(school, user, email)
    @user = user
    @school = school    
    mail(to: email, subject: '[Viewcy] You are invited')
  end
end
