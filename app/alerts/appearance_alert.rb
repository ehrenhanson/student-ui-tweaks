class AppearanceAlert < Alert::Base
  def self.scope(school)
    fields = 'users.id AS user_id, users.name AS user_name, courses.name AS course_name,
      appearances.created_at AS alert_created_at'
      
    school.appearances.joins(:user, :course).select(fields)
  end
  
  def format
    "#{user_link} attended #{course_name}"
  end
end