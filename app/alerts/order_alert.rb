class OrderAlert < Alert::Base
  def self.scope(school)
    fields = 'users.id AS user_id, users.name AS user_name, products.name AS product_name,
      order_items.price AS purchase_amount, orders.created_at AS alert_created_at'
      
    school.orders.joins(:user, :order_item => :product).select(fields)
  end
  
  def format
    "#{user_link} purchased #{product_name} for $#{purchase_amount}"
  end
end