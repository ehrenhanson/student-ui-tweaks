json.id @school.id
json.name @school.name
json.description @school.description

json.address_address @school.address_address
json.address_city @school.address_city
json.phone @school.phone
json.email @school.email

json.logo_image @school.logo_image if @school.logo_image?
json.header_image @school.header_image if @school.header_image?