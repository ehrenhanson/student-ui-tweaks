json.id appearance.id
json.url api_v1_school_appearance_url(user_id: params[:user_id], course_id: params[:course_id], school_id: params[:school_id], id: appearance, format: :json)

json.name appearance.name
json.email appearance.email
json.notes appearance.notes
json.payment_status appearance.payment_status

json.created_at appearance.created_at
json.updated_at appearance.updated_at