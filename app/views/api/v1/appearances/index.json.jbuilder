json.objects @appearances, partial: 'api/v1/appearances/appearance', as: :appearance

json.prev api_v1_school_appearances_url(user_id: params[:user_id], course_id: params[:course_id], school_id: params[:school_id], format: :json, page: page - 1) if prev?
json.next api_v1_school_appearances_url(user_id: params[:user_id], course_id: params[:course_id], school_id: params[:school_id], format: :json, page: page + 1) if next?(@appearances)