json.objects @schools, partial: 'api/v1/schools/school', as: :school

json.prev api_v1_schools_url(format: :json, page: page - 1) if prev?
json.next api_v1_schools_url(format: :json, page: page + 1) if next?(@schools)
