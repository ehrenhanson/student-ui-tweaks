json.id school.id
json.url api_v1_school_url(school, :json)

json.name school.name
json.identifier school.identifier
json.description school.description

json.timezone do
  json.name school.timezone
  json.identifier school.tz.tzinfo.identifier
  json.utc_offset school.tz.utc_offset
end

json.email school.email
json.phone school.phone
json.gplus_url school.gplus_url
json.facebook_url school.facebook_url
json.twitter_url school.twitter_url
json.logo_image_url URI.join(root_url, school.logo_image.url).to_s
json.header_image_url URI.join(root_url, school.header_image.url).to_s
json.role school.role.to_str if school.role

if school.address
  json.address do
    json.partial! 'api/v1/addresses/address', address: school.address
  end
end

json.created_at school.created_at
json.updated_at school.updated_at
