json.objects @courses, partial: 'api/v1/courses/course', as: :course

json.prev api_v1_school_courses_url(params[:school_id], format: :json, page: page - 1) if prev?
json.next api_v1_school_courses_url(params[:school_id], format: :json, page: page + 1) if next?(@courses)