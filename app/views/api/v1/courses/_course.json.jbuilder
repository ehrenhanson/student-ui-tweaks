json.id course.id
json.url api_v1_school_course_url(course.school, course)

json.name course.name
json.description course.description
json.level course.level

json.created_at course.created_at
json.updated_at course.updated_at