json.id credit.id
json.url api_v1_school_user_credit_url(current_school, @user, credit, :json)

json.product_name credit.product_name
json.value credit.value
json.remaining credit.remaining

json.created_at credit.created_at
json.updated_at credit.updated_at