json.objects @products, partial: 'api/v1/products/product', as: :product

json.prev api_v1_school_products_url(course_id: params[:course_id], school_id: params[:school_id], format: :json, page: page - 1) if prev?
json.next api_v1_school_products_url(course_id: params[:course_id], school_id: params[:school_id], format: :json, page: page + 1) if next?(@products)
