json.id product.id
json.url api_v1_school_product_url(params[:school_id], product, :json)

json.name product.name
json.description product.description
json.credit_value product.credit_value
json.price product.price

json.course_ids product.course_ids

json.created_at product.created_at
json.updated_at product.updated_at