json.objects @events, partial: 'api/v1/events/event', as: :event

json.prev api_v1_school_events_between_url(current_school,
  start_date - date_span - 1, start_date - 1, :json)
json.next api_v1_school_events_between_url(current_school,
  end_date + 1, end_date + date_span + 1, :json)