json.id event.to_param
json.url api_v1_school_event_url(event.school, event, :json)
json.parent_url api_v1_school_event_url(event.school, event.id, :json) unless event.parent?

json.name event.name
json.description event.description
json.location event.location
json.course_id event.course_id
json.instructor_id event.instructor_id

if current_user && current_user.role > Role.student
  json.repeat_type event.repeat_type
  json.repeat_interval event.repeat_interval
  json.repeat_until event.repeat_until
end

json.excluded event.excluded?
json.starts_at event.starts_at
json.ends_at event.ends_at