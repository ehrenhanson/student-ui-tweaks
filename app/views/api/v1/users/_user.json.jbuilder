json.id user.id
json.url api_v1_user_url(user, school_id: params[:school_id], format: :json)

json.name user.name
json.first_name user.first_name
json.last_name user.last_name
json.email user.email
json.created_at user.created_at
json.updated_at user.updated_at
json.role user.role.to_str