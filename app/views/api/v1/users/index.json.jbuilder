json.objects @users, partial: 'api/v1/users/user', as: :user

json.prev api_v1_school_users_url(params[:school_id], format: :json, page: page - 1) if prev?
json.next api_v1_school_users_url(params[:school_id], format: :json, page: page + 1) if next?(@users)