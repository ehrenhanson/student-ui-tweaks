json.objects @registrations do |registration|
  json.rsvp registration.rsvp?
  
  json.user do
    json.partial! 'api/v1/users/user', user: registration.user
  end
end