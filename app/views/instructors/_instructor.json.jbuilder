json.id instructor.id
json.name instructor.name
json.email instructor.email
json.profile_image_url instructor.profile_image_url
json.description instructor.description

json.courses instructor.courses do |course|
  json.id course.to_param
  json.name course.name
  json.description course.description
end