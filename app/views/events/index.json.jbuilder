json.events do
  json.array! @events, partial: 'events/event', as: :event
end
json.next course_events_url(params[:course_id], start: @events.last.starts_on) if @events.last