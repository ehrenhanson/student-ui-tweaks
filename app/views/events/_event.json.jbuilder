json.id event.to_param
json.name event.name
json.starts_at local_time(event.starts_at)
json.ends_at local_time(event.ends_at)

json.instructor do |instructor|
  instructor.id event.instructor_id
  instructor.name event.instructor_name
end

json.registered event.registered
