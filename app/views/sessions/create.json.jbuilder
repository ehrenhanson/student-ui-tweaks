json.user do
  json.partial! 'api/v1/users/user', user: @user
end

json.schools @schools, partial: 'api/v1/schools/school', as: :school
json.active_school @user.active_school_id
json.authenticity_token form_authenticity_token