json.array! @regions do |region|
  json.name region.name
  json.type region.type
end