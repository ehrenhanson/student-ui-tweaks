json.element_id params[:element_id]

json.event do
  json.id @state.event.to_param
  json.name @state.event.name
end

if address = @state.user.billing_address || @state.user.build_billing_address
  json.address do
    json.name address.name
    json.address address.address
    json.address_2 address.address_2
    json.city address.city
    json.state address.state
    json.country address.country
    json.zipcode address.zipcode
  end
end

json.credits @state.credits do |credit|
  json.id credit.to_param
  json.name credit.product_name
  json.remaining credit.remaining
end

json.products @state.products do |product|
  json.id product.to_param
  json.name product.name
  json.price format_price(product.price)
end

json.countries @state.countries do |country|
  json.name country.name
  json.code country.code
end