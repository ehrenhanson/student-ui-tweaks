json.id course.to_param
json.name course.name
json.description course.description

json.class_description course.class_description
json.event_description course.event_description
json.product_description course.product_description

json.instructors course.instructors, partial: 'instructors/instructor', as: :instructor
json.events Event::RegisteredEnumerator.new(course.events, @registrations, start_date, end_date), partial: 'events/event', as: :event