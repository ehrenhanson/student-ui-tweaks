module EventsHelper
  def local_time(time)
    time.strftime('%Y-%m-%dT%H:%M:%S') if time
  end
end
