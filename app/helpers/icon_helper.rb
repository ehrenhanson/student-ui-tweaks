module IconHelper
  ICON_NAMES = %w(email phone gplus facebook twitter info check checked calendar star pin email2 phone2 add star downarrow uparrow submit closecalendar)
  ICONS = Hash[ICON_NAMES.zip('a'..'z')].with_indifferent_access.freeze
  
  # Map an icon symbol to the corresponding character in the icons
  # font (app/assets/icons-webfont). email => a, phone => b, etc.
  #
  # If the font layout changes, the above defintions must also
  # reflect those changes.
  def icon(identifier, options = {})
    icon = ICONS[identifier]
    
    class_names = Array(options.delete(:class))
    class_names << 'icons'
    
    content_tag(:span, icon, options.merge(class: class_names.join(' '))) if icon
  end
  
  def icon_button(identifier)
    button_tag(icon(identifier))
  end
end