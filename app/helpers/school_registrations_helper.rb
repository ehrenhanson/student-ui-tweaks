module SchoolRegistrationsHelper
  def timezone_options
    ActiveSupport::TimeZone.all.map do |zone|
      [ "#{zone.utc_offset / 3600} #{zone.name}", zone.name ]
    end
  end
end