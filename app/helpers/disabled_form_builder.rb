class DisabledFormBuilder < SimpleForm::FormBuilder
  def input(attribute_name, options = {}, &block)
    options.merge!(disabled: @options[:disabled])
    super
  end
end