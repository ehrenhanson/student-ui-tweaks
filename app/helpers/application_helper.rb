module ApplicationHelper
  include DateHelper
  include MoneyHelper
  include IconHelper
  
  def title(title = nil)
    @_title = title || @title || @_title
    [ @_title, 'Viewcy' ].compact.join(' | ')
  end
  
  def embed_json(template, object, options = {})
    if object.present?
      if options.delete(:partial)
        options[:partial] = template
        options[:object] = object
      else
        options[:template] = template
      end
        
      raw(render(options.merge!(formats: :json)))
    else
      'null'
    end
  end
end
