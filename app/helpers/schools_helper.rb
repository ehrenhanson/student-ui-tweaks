module SchoolsHelper
  POSITIONS = %w(left right)
  
  def part(collection, *dependencies, &block)
    if collection.present?
      # Keep track of how many times the part method has been called.
      # Each subsequent call should change the position context of
      # the returned part.
      @_part_position = @_part_position.to_i.next
    
      if collection.present? && dependencies.any?(&:present?)
        class_name = "col #{POSITIONS[@_part_position - 1] || :extra}"
      else
        class_name = 'col one'
      end
    
      content_tag(:div, class: class_name, &block)
    end
  end
end
