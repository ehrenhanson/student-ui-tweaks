module DateHelper
  def format_date_header(date)
    prefix = case date
    when Date.today
      'Today'
    when Date.today + 1
      'Tomorrow'
    else
      date.strftime('%A')
    end
    
    (prefix + ' ' + content_tag(:span, date.strftime('%m/%d'), class: 'date')).html_safe
  end
  
  def format_date_and_time_range(start_time, end_time)
    [ start_time.strftime('%b. %e, %Y'), format_time_range(start_time, end_time) ].join(' ')
  end
  
  def format_time_range(start_time, end_time)
    [ format_time(start_time), format_time(end_time) ].join(' - ')
  end
  
  def format_time(time)
    time.strftime('%l:%M %p')
  end
end