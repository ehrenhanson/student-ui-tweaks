module MoneyHelper
  def format_price(price)
    if price.respond_to?(:to_money)
      price.to_money.format
    end  
  end
end