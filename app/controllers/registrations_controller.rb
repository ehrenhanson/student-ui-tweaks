class RegistrationsController < ApplicationController
  include DateRange

  def new
    @state = RegistrationState.new(current_school, current_user!, params[:event_id], start_date)
  end
  
  def create
    @manager = RegistrationManager.new(current_school, current_user!, registration_manager_params)
    
    if @manager.save
      RegistrationMailer.thanks(@manager.registration).deliver
      render json: {}, status: :created
    else
      logger.debug("Unprocessable entity: #{@manager.errors.to_a}")
      render json: @manager.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    current_user!.registrations.where(registration_destruction_params).destroy_all
    render json: {}, status: :ok
  end

  private
    def registration_manager_params
      params.slice(:card, :address, :event_id, :credit_id, :product_id).permit!
    end
    
    def registration_destruction_params
      params.permit(:event_id).merge(starts_on: start_date)
    end
end
