class BraintreeController < ApplicationController
  def accept
    notification = Braintree::WebhookNotification.parse(params[:bt_signature], params[:bt_payload])
    
    case notification.kind
    when Braintree::WebhookNotification::Kind::SubMerchantAccountApproved
      School.discover_merchant(notification.merchant_account.id).approve!
    when Braintree::WebhookNotification::Kind::SubMerchantAccountDeclined
      School.discover_merchant(notification.merchant_account.id).decline!
    end
    
    head :ok
  end
end
