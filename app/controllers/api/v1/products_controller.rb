class Api::V1::ProductsController < Api::V1::ApiController
  authorize :faculty
  
  def index
    @products = current_scope.products.limit(limit).offset(offset)
  end
  
  def show
    @product = current_scope.products.find(params[:id])
  end
  
  def create
    @product = current_school.products.build(product_params)    
    
    if @product.save
      render 'show', status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end
  
  def update
    @product = current_school.products.find(params[:id])
    
    if @product.update_attributes(product_params)
      head :no_content
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    current_school.products.destroy(params[:id])
    head :no_content
  end
  
  private
    def product_params
      params.require(:product).permit(:name, :description, :credit_value, :price, :unlimited, :course_ids => [])
    end
  
    def current_course
      Course.find(params[:course_id])
    end
    
    def current_scope
      if params[:course_id]
        current_course
      else
        current_school
      end
    end
end
