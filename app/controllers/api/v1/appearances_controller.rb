class Api::V1::AppearancesController < Api::V1::ApiController
  def index
    @appearances = current_scope.appearances.limit(limit).offset(offset)
  end
  
  def show
    @appearance = current_scope.appearances.find(params[:id])
  end
  
  private
    def current_course
      if current_user.role.admin?
        Course.find(params[:course_id])
      else
        current_user.affiliated_courses.min_role(Role.faculty).find(params[:course_id])
      end
    end
    
    def current_affiliated_user
      if current_user.role.admin?
        User.find(params[:user_id])
      else
        current_user.affiliated_users.min_role(Role.faculty).find(params[:user_id])
      end
    end
  
    def current_scope
      if params[:user_id].present?
        current_affiliated_user
      else
        current_course
      end
    end
end
