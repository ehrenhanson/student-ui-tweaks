class Api::V1::ApiController < ApplicationController
  authorize :user
  wrap_parameters exclude: []
  append_view_path "#{Rails.root}/app/views"
  
  skip_around_filter :set_local_zone
  
  private
    def log_validation_errors(model)
      if model.respond_to?(:errors)
        logger.debug("Unprocessable entity: #{model.errors.to_a}")
      end
    end
end