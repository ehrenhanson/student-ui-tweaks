class Api::V1::CreditsController < ApplicationController
  include SchoolContext
  
  authorize :faculty
  
  before_filter :set_user
  
  def index
    @credits = @user.credits.find_remaining
  end
  
  def show
    @credit = @user.credits.find_remaining.find(params[:id])
  end
  
  def update
    @credit = @user.credits.find(params[:id])
    
    if @credit.update_attributes(credit_params)
      head :ok
    else
      render json: @credit.errors, status: :unprocessable_entity
    end
  end
  
  private
    def set_user
      @user = current_school.users.find(params[:user_id])  
    end
    
    def credit_params
      params.require(:credit).permit(:value, :unlimited)
    end
end
