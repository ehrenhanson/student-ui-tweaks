class Api::V1::SchoolAttachmentsController < ApplicationController
  VALID_ATTACHMENT_ATTRIBUTES = %w(header_image logo_image)
  
  include SchoolContext
  
  authorize :owner
  
  def create
    @school = School.find(params[:school_id])
    
    if @school.update_attributes(attachment_params)
      render status: :created, formats: :json
    else
      render json: @school.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @school = School.find(params[:school_id])
    
    if VALID_ATTACHMENT_ATTRIBUTES.include?(params[:id]) && @school.update_attribute(params[:id], nil)
      head :ok
    else
      render json: @school.errors, status: :unprocessable_entity
    end
  end
  
  private
    def attachment_params
      params.permit(*VALID_ATTACHMENT_ATTRIBUTES)
    end
end
