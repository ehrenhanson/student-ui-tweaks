class Api::V1::SchoolsController < Api::V1::ApiController
  include SchoolContext

  authorize :admin, only: :create
  authorize :owner, only: :update
  
  def index
    @schools = scoped_schools.limit(limit).offset(offset)
  end
  
  def show
    @school = scoped_schools.find(params[:id])
  end
  
  def create
    @school = School.new(school_params)
    
    if @school.save
      render 'show', status: :created, location: api_v1_school_url(@school)
    else
      log_validation_errors(@school)
      render json: @school.errors, status: :unprocessable_entity
    end
  end
  
  def update
    @school = scoped_schools.find(params[:id])
    
    if @school.update_attributes(school_params)
      head :no_content
    else
      log_validation_errors(@school)
      render json: @school.errors, status: :unprocessable_entity
    end
  end
  
  private
    def school_params
      params.require(:school).permit(:name, :identifier, :description, address_attributes: [ :address, :address_2, :city, :state, :zipcode, :country ])
    end
    
    def school_identifier
      params[:id] || super
    end
end
