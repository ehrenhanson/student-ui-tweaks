class Api::V1::UsersController < Api::V1::ApiController
  include SchoolContext

  rescue_from Role::InvalidRole, with: :bad_request
  authorize :faculty
  
  def index
    @users = scoped_users.filter(params[:role]).limit(limit).offset(offset)
  end
  
  def show
    @user = scoped_users.find(params[:id])
  end
  
  def create
    @user = User.new(user_params)
    @user.affiliations.build(school_id: current_school.id, role: role_param)
      
    if @user.save
      render 'show', status: :created, location: api_v1_school_user_url(params[:school_id], @user)
    else
      log_validation_errors(@user)
      render json: @user.errors, status: :unprocessable_entity
    end
  end
  
  def invite
    source = scoped_users.find(params[:id])
    @user = (User.find_by_email(params[:email]) || source).merge(source)
    
    if @user.invite
      UserMailer.invite(current_school, @user, params[:email]).deliver      
      head :no_content
    else
      log_validation_errors(@user)
      render json: @user.errors, status: :unprocessable_entity
    end
  end
  
  def update
    @user = User.find(params[:id])
    
    if current_user.role.admin? || current_user == @user
      if @user.update_attributes(user_params)
        head :no_content
      else
        log_validation_errors(@user)
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      head :forbidden
    end
  end
  
  private
    def scoped_users
      # Only admins can access all the users without being scoped by the school
      return User.all if params[:school_id].blank? && current_user!.role.admin?
      
      school = current_school_with_role
      users  = school.users.with_role
      
      if school.role <= Role.student
        users.where(id: current_user!)
      else
        users
      end
    end
    
    def user_params
      user_params = params.require(:user).permit(:name, :first_name, :last_name)
    end
    
    def role_param
      role = Role.unstringify(params.require(:user).fetch(:role, Role.student.to_str))
      raise SessionExpired if role > current_user.role
      role
    end
end
