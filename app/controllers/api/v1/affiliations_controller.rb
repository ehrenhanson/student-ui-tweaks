class Api::V1::AffiliationsController < Api::V1::ApiController
  def active
    current_user.affiliations.update_active_school(params[:school_id])
    head :no_content
  end
end
