class Api::V1::ExclusionsController < ApplicationController
  include DateRange
  
  authorize :faculty
  before_filter :set_event
  rescue_from DateParser::InvalidDate, with: :bad_request
  
  def create
    @exclusion = @event.exclusions.build(exclude_on: start_date)
    
    if @exclusion.save
      head :created
    else
      render json: @exclusion.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @exclusion = @event.exclusions.find_by!(exclude_on: start_date)
    @exclusion.destroy
    head :no_content
  end
  
  private
    def set_event
      @event = Event.find(params[:event_id])
    end

    # Define start_date input parameter for DateRange
    def start_date_param
      params[:start] || params[:event_id].to_s[/.+?\-(.*)/, 1]
    end
end
