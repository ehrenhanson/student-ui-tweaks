class Api::V1::CoursesController < Api::V1::ApiController
  authorize :faculty, only: [ :create, :update, :destroy ]
  
  def index
    @courses = current_school.courses.limit(limit).offset(offset)
  end
  
  def show
    @course = current_school.courses.find(params[:id])
  end
  
  def create
    @course = current_school.courses.build(course_params)
    
    if @course.save
      render 'show', status: :created, location: api_v1_school_course_url(@course.school, @course)
    else
      log_validation_errors(@course)
      render json: @course.errors, status: :unprocessable_entity
    end
  end
  
  def update
    @course = current_school.courses.find(params[:id])
    
    if @course.update_attributes(course_params)
      head :no_content
    else
      log_validation_errors(@course)
      render json: @course.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @course = current_school.courses.destroy(params[:id])
    head :no_content
  end
  
  private
    def course_params
      params.require(:course).permit(:name, :description, :level)
    end
end
