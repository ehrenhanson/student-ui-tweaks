class Api::V1::RegistrationsController < ApplicationController
  include SchoolContext
  include DateRange
  
  def index
    @registrations = current_event.registrations
  end
  
  private
    def current_event
      current_school.events.find_on(params[:event_id], (start_date if params[:start]))
    end
end
