class Api::V1::EventsController < Api::V1::ApiController
  include DateRange
  
  authorize :faculty, except: [ :index, :show ]
  
  def index
    @events = current_school.events.between(start_date, end_date, include_exclusions: true)
  end
  
  def show
    @event = current_school.events.find_on(params[:id], (start_date if params[:start]))
  end
  
  def create
    @event = current_course.events.build(event_params)
    
    if @event.save
      render 'show', status: :created, location: api_v1_school_event_url(@event, @event.starts_on)
    else
      log_validation_errors(@event)
      render json: @event.errors, status: :unprocessable_entity
    end
  end
  
  def update
    @event = current_school.events.find(params[:id])
    
    if @event.update_attributes(event_params)
      head :no_content
    else
      log_validation_errors(@event)
      render json: @event.errors, status: :unprocessable_entity
    end
  end
  
  def destroy
    @event = current_school.events.find(params[:id])
    @event.destroy
    head :no_content
  end
  
  private
    # Number of days to span for DateRange
    def date_span
      14
    end

    def event_params
      params.require(:event).permit(
        :name, :description, :location, :instructor_id, :starts_at, :ends_at,
        :repeat_type, :repeat_interval, :repeat_until
      )
    end
    
    def current_course
      current_school.courses.find(params[:course_id])
    end
end
