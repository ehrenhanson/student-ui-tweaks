class Api::V1::AlertsController < Api::V1::ApiController
  def index
    @alerts = current_school_with_role.alerts.filter(max_age)
  end
  
  private
    def max_age
      DateParser.new(params[:max_age] || 1.day.ago).to_time
    end
end
