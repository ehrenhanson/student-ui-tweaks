class ResetPasswordsController < ApplicationController
  def show
    @user = User.find_by_reset_password_token(params[:id])    
    redirect_to new_reset_password_url unless @user  
  end
  
  def create
    @user = User.find_by_email(params[:email])
    
    if @user
      @user.activate_reset_password_token!
      UserMailer.reset_password(@user).deliver
    else
      render 'new'      
    end
  end
  
  def update
    @user = User.find_by_reset_password_token!(params[:id])    
    @user.password = params[:password]
    @user.password_confirmation = params[:password_confirmation]
    @user.reset_password_token = nil
    
    if @user.save
      redirect_to new_session_url
    else
      render 'show'
    end
  end
end
