class Registrations::RegionsController < ApplicationController
  class CountryNotFound < Exception
  end
  
  before_filter :set_country
  rescue_from CountryNotFound, with: :bad_request
  
  def index
    @regions = @country.subregions
  end
  
  private
    def set_country
      @country = Carmen::Country.coded(params[:id]) || raise(CountryNotFound, "#{params[:id]} was not found")
    end
end
