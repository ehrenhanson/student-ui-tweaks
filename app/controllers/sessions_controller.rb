class SessionsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  
  def show
    @user = current_user!
    @schools = scoped_schools(@user)
    
    render 'create'
  end
  
  def create
    if @user = authenticated_user
      self.current_user = @user
      
      respond_to do |format|
        format.html { redirect_to school_root_url }
        format.json { @schools = scoped_schools }
      end
    else
      respond_to do |format|
        format.html { redirect_to new_session_url }
        format.json { head :forbidden }
      end
    end
  end
  
  def failure
    redirect_to new_session_url
  end
  
  def destroy
    reset_session
    redirect_to school_root_url
  end
  
  private
    def authenticated_user
      @authenticated_user ||= if params[:provider]
        User.authenticate_with_omniauth(request.env['omniauth.auth'])
      else
        User.authenticate(params[:email], params[:password])
      end
    end
end
