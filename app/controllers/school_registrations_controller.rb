class SchoolRegistrationsController < ApplicationController
  before_filter :set_user
  
  def new
    @school_registration = SchoolRegistration.new(@user)
  end
  
  def create
    @school_registration = SchoolRegistration.new(@user, school_registration_params)
    
    if @school_registration.save
      redirect_to school_root_url(subdomain: @school_registration.school_identifier)
    else
       render 'new'
    end
  end
  
  private
    def set_user
      @user = current_user || User.new
    end
  
    def school_registration_params
      params.require(:school_registration).permit(
        :ssn, :tax_id, :account_number, :routing_number,
        user_attributes: [
          :name, :email, :date_of_birth, :password,
          billing_address_attributes: [ :address, :address_2, :city, :state, :country, :zipcode ]
        ],
        school_attributes: [
          :name, :legal_name, :identifier, :description, :timezone, :email, :phone,
          :gplus_url, :facebook_url, :twitter_url, :logo_image, :header_image, :tos_accepted,
          address_attributes: [ :address, :address_2, :city, :state, :country, :zipcode ]
        ]
      )
    end
end
