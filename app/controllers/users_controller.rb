class UsersController < ApplicationController
  ALLOWABLE_ATTRIBUTES = [ :name, :email, :password, :terms_accepted ]
  wrap_parameters :user, include: ALLOWABLE_ATTRIBUTES
  
  def create
    @user = User.new(user_params)
    
    if @user.register
      session[:user_id] = @user.id
      render status: :created
    else
      logger.debug("Unprocessable entity: #{@user.errors.to_a}")
      render json: @user.errors, status: :unprocessable_entity
    end
  end
  
  private
    def user_params
      params.require(:user).permit(*ALLOWABLE_ATTRIBUTES)
    end
end
