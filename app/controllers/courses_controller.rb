class CoursesController < ApplicationController
  include DateRange
  include Registrations
  
  before_filter :set_courses
  before_filter :set_registrations
  
  def index
    # load_courses
  end
  
  def show    
    @course = @courses.find(params[:id])
  end

  protected
    # Number of days to span for DateRange
    def date_span
      14
    end
  
  private
    def set_courses
      @courses = current_school.courses.includes(:instructors)
    end
end
