class ApplicationController < ActionController::Base
  include Authorization
  include Pagination
  include SchoolContext
  include Timezone
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session  
  
  def bad_request(exception = nil)
    message = exception.message if exception
    
    respond_to do |type|
      type.html { render text: "Invalid request: #{message}", status: :bad_request }
      type.json { render json: { message: "Invalid request: #{message}" }, status: :bad_request }
    end
  end
end
