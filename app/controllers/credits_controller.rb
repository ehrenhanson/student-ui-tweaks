class CreditsController < ApplicationController
  protect_from_forgery except: :index
  
  include SchoolContext
  
  def index
    @credits = current_user!.credits.of_school(current_school).find_remaining
  end
end
