module Timezone
  extend ActiveSupport::Concern
  
  DEFAULT_TIME_ZONE = 'Eastern Time (US & Canada)'
  
  included do
    around_filter :set_local_zone
  end
  
  private
    def set_local_zone(&block)
      zone = current_school_timezone || DEFAULT_TIME_ZONE
      
      logger.debug("Using time zone: #{zone}")
      Time.use_zone(zone, &block)
    end
    
    def current_school_timezone
      current_school.timezone
    rescue ActiveRecord::RecordNotFound      
    end
end