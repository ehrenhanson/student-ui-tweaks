module EventSet
  def ready_events_for_calendar(scope, start_date = Date.today, end_date = Date.today + 14)
    @events = scope.includes(:course, :instructor).between(start_date, end_date)
    @registration_set = user_registration_set(start_date, end_date)
  end
  
  def user_registration_set(start_date, end_date)
    if current_user
      current_user.registrations.set(start_date, end_date)
    else
      Set.new
    end
  end
end