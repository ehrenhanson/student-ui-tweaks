module DateRange
  extend ActiveSupport::Concern
  
  included do
    rescue_from DateParser::InvalidDate, with: :bad_request    
    helper_method :start_date, :end_date, :date_span
  end

  protected
    def date_span
      0
    end
  
  private
    def default_start_date
      Date.today if date_span > 0
    end
    
    def default_end_date
      start_date + date_span if date_span > 0
    end
  
    def start_date_param
      params[:start] || default_start_date
    end
    
    def end_date_param
      params[:end] || default_end_date
    end
  
    def start_date
      @start_date ||= DateParser.new(start_date_param).to_date
    end
  
    def end_date
      @end_date ||= DateParser.new(end_date_param).to_date
    end
end