module SchoolContext
  extend ActiveSupport::Concern
  
  included do
    layout :layout_context
    helper_method :current_school
  end

  def layout_context
    if school_identifier.present?
      current_school
      'school'
    end
  rescue ActiveRecord::RecordNotFound
  end
  
  def school_identifier
    params[:school_id] || request.subdomain.split('.').first
  end
  
  def current_school
    @school ||= School.enabled.discover(school_identifier)
  end
  
  def current_school_with_role
    user = current_user!
    
    if user.role.admin?
      School.as_role(:admin).discover(params[:school_id] || school_identifier)
    else
      user.schools.with_role.discover(params[:school_id] || school_identifier)
    end    
  end
  
  def scoped_schools(user = current_user!)
    if user.role.admin?
      School.as_role(:admin)
    else
      user.schools.with_role
    end
  end
end