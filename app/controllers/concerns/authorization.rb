module Authorization
  extend ActiveSupport::Concern
  
  class SessionExpired < Exception
  end
  
  included do
    helper_method :current_user
    rescue_from SessionExpired, with: :session_expired
    before_filter :setup_user_session_from_remember_cookie
  end
  
  module ClassMethods
    def authorize(*roles)
      options = roles.extract_options!
      
      roles.each do |role|
        role = Role.send(role)
        before_filter(options) do |controller|
          logger.debug("Authorizing #{controller.current_user!.email} (role: #{controller.current_user!.role.to_str}) for #{role.to_str} role")
          controller.session_expired if controller.current_user!.role < role
        end
      end
    end
  end
  
  def current_user
    @current_user ||= User.with_school(school_identifier).find_by(id: session[:user_id])
  end
  
  def current_user!
    current_user || raise(SessionExpired, "Cannot find user session with id=#{session[:user_id]}")
  end
  alias_method :user_required, :current_user!
  
  def current_user=(user)
    session[:user_id] = user.id

    cookies.permanent.signed[:remember] = {
      value: [ @user.id, @user.login_token ],
      httponly: true,
      domain: :all
    }
  end
  
  def session_expired
    respond_to do |format|
      format.js { render 'shared/login' }      
      format.json { head :forbidden }
      format.any { redirect_to new_session_url }
    end
  end
  
  def reset_session
    current_user.reset_login_token! if current_user    
    super
  end
  
  private
    def setup_user_session_from_remember_cookie
      unless session[:user_id]
        id, token = cookies.signed[:remember]
        user = User.find_by(id: id, login_token: token)
        session[:user_id] = user.id if user
      end
    end
end