module Pagination
  extend ActiveSupport::Concern
  
  DEFAULT_LIMIT = 30
  
  included do
    helper Helper
    helper_method :page
  end
  
  module Helper
    def prev?
      page > 1
    end
    
    def next?(collection)
      collection.size > 0
    end
  end
  
  def limit
    (params[:limit] || DEFAULT_LIMIT).to_i
  end
  
  def offset
    (page - 1) * limit
  end
  
  def page
    [ params[:page].to_i, 1 ].max
  end
end