module Orders
  extend ActiveSupport::Concern
  
  def current_order
    @current_order ||= current_user.orders.find_by(school_id: current_school, id: session[:order_id])
  end
  
  def initialize_order
    Order.create!(school: current_school, user: current_user).tap do |order|
      session[:order_id] = order.id
    end
  end
  
  def reset_order
    session[:order_id] = nil
  end
end