module Registrations
  extend ActiveSupport::Concern
  
  private
    def set_registrations
      if current_user
        @registrations = current_user.registrations.under_school(current_school)
      end
    end
end