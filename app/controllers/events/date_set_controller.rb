class Events::DateSetController < ApplicationController
  include DateRange
  
  before_filter :set_course
  
  def index
    @date_set = @course.events.between(start_date, end_date).to_date_set
  end
  
  private
    def set_course
      @course = current_school.courses.find(params[:course_id])
    end
  
    def date_span
      1.month
    end
    
    def default_start_date
      Date.today.beginning_of_month
    end
end