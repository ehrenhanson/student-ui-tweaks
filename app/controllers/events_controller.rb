class EventsController < ApplicationController
  include DateRange
  include Registrations
  
  before_filter :set_registrations
  
  def index
    @events = current_school.courses.find(params[:course_id]).events    
    @events = Event::CountedEnumerator.new(@events, @registrations, start_date, end_date)
    @events.filter = proc { |event| event.registered } if only_registered?
  end

  protected
    def date_span
      30
    end  
  
  private
    def only_registered?
      params[:registered] == 'true' && current_user
    end
end
