class SchoolsController < ApplicationController
  include DateRange
  include Registrations
  
  before_filter :set_registrations

  def show
    @courses = current_school.courses.includes(:instructors)
  end
  
  private
    def date_span
      14
    end
end
