window.Scheduler = {}

#= require ./page
#= require ./sync
#= require ../registration/initializer
#= require ./models/observer
#= require ./routers/main_router
#= require ./views/layout_view

$(document).ready ->
  router = new Scheduler.MainRouter
  Backbone.history.start(pushState: true)
