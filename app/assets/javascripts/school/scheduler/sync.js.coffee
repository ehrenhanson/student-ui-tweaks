#= require ./models/observer

sync = Backbone.sync
Backbone.csrfToken = $("meta[name=csrf-token]").attr("content")

Backbone.Model.SyncObserver =
  count: 0

  start: ->
    Backbone.Model.SyncObserver.count++
    Scheduler.Observer.trigger("model:sync:start") if Backbone.Model.SyncObserver.count == 1
      
  stop: ->
    Backbone.Model.SyncObserver.count--
    Scheduler.Observer.trigger("model:sync:stop") if Backbone.Model.SyncObserver.count == 0

Backbone.sync = (method, model, options) ->
  options.beforeSend = (xhr) ->
    xhr.setRequestHeader("X-CSRF-Token", Backbone.csrfToken)
  
  options.error = (resp, status, options) ->
    switch resp.status
      when 403
        Scheduler.Observer.trigger("login:display", @)
      when 422
        model.trigger("invalid", resp.responseJSON)
    
  Backbone.Model.SyncObserver.start()
  complete = options.complete
  options.complete = ->
    complete() if complete?
    Backbone.Model.SyncObserver.stop()
  
  sync(method, model, options)
