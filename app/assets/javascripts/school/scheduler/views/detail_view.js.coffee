#= ./school_detail_view
#= ./course_detail_view
#= ./authentication_button_view
#= ./login_view
#= ./signup_view

class Scheduler.DetailView extends Backbone.View
  el: "#detail"
  events:
    "click .brand": "openHome"
  
  initialize: ->
    $(window).resize(@render)
    
    @authenticationButtonView = new Scheduler.AuthenticationButtonView
    @listenTo(Scheduler.Observer, "school:show", @openSchool)
    @listenTo(Scheduler.Observer, "course:show", @openCourse)
    @listenTo(Scheduler.Observer, "login:display", @openLogin)
    @listenTo(Scheduler.Observer, "signup:display", @openSignup)
  
  render: =>
    @popupView.remove() if @popupView?
    
    @authenticationButtonView.render()
    @$el.find("#content").html(@view.render().$el) if @view?
    @fixate()
    @
  
  fixate: =>
    @$el.removeClass("fixed")    
    if @$el.height() <= $(window).height()
      @$el.addClass("fixed")
  
  openSchool: (view, model) =>
    @view.remove() if @view?
    @view = new Scheduler.SchoolDetailView(model: model)
    @render()
  
  openCourse: (view, model) =>
    @view.remove() if @view?
    @view = new Scheduler.CourseDetailView(model: model)
    @render()
  
  openHome: (event) =>
    event.preventDefault()
    Backbone.history.navigate("", trigger: true)

  openLogin: =>
    @openPopup(new Scheduler.LoginView)
  
  openSignup: =>
    @openPopup(new Scheduler.SignupView)

  openPopup: (view) ->
    if @popupView?
      if @popupView.close
        @popupView.close()
      else
        @popupView.remove()

    @popupView = view
    @$el.append(@popupView.render().$el)