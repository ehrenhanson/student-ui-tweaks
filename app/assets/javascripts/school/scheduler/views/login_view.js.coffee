#= require ../models/observer

class Scheduler.LoginView extends Backbone.View
  id: "login"
  template: """
  	<h2>
      <% if (!failed) { %>
    		Welcome.
      <% } else { %>
        Login failed. Please try again.
      <% } %>
  	</h2>

    <form>
  		<p>
        <input type="text" name="email" placeholder="Email Address">
  		</p>

  		<p>
        <input type="password" name="password" placeholder="Password">
  		</p>

  		<p>
        <button>
          >
        </button>
  		</p>
    </form>
    
    <button class="new-account">
      Create an Account
    </button>
  """
  events:
    "submit form": "login"
    "click .new-account": "openSignup"
    
  initialize: ->
    @loginFailed = false
    super
  
  render: =>
    @$el.html(_.template(@template)(failed: @loginFailed))
    @
    
  close: ->
    @$el.addClass("close")
    window.setTimeout((=> @remove()), 500)

  login: (event) =>
    event.preventDefault()
    Scheduler.User.authenticate(@$el.find("[name=email]").val(), @$el.find("[name=password]").val(),
      success: @success, fail: @failure)
    
  success: (model) =>
    Scheduler.Observer.trigger("login:success", @, model)
    @$el.addClass("close")
    _.delay(_.bind(@remove, this), 500)
  
  failure: =>
    Scheduler.Observer.trigger("login:failure", @)
    @loginFailed = true
    @render()
    
  openSignup: (event) =>
    event.preventDefault()
    Scheduler.Observer.trigger("signup:display", @)