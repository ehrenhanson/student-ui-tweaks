class Scheduler.ThrobberView extends Backbone.View
  id: "throbber"
  template: """
    <div class="container">
      <div class="loader">
      </div>
    </div>
  """
  
  render: =>
    @$el.html(@template)
    @
