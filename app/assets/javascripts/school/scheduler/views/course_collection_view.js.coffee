#= require ./course_view
#= require ../models/course

class Scheduler.CourseCollectionView extends Scheduler.CollectionView
  className: "collection courses"
  preloadedData: data.courses if data?
  
  initialize: ->
    super
    @listenTo(@collection, "sync", @render)
    @collection.fetch() unless @preloadedData

  renderView: (model) ->
    @append(new Scheduler.CourseView(model: model))