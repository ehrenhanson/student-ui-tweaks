class Scheduler.BookButtonView extends Backbone.View
  registeredTemplate: """
    Booked
  """
  
  unregisteredTemplate: """
    Book
  """
  
  events:
    "click": "clickHandler"

  initialize: (options) ->
    super
    @parent = options.parent
    @listenTo(@model, "sync", @render)

  render: =>
    if @model.get("registered")
      @$el.html(@registeredTemplate)
    else
      @$el.html(@unregisteredTemplate)
    @
    
  clickHandler: =>
    unless @model.get("registered")
      @parent.registerEvent()
    else
      @parent.unregisterEvent()