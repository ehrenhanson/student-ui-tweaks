#= require ../models/observer

class Scheduler.AuthenticationButtonView extends Backbone.View
  el: "#authentication"
  loggedInTemplate: """
    <span class="name">
      <%- name %>
    </span>
    &nbsp;&nbsp;
    <span class="logout">
      <a href="/auth/logout">
        Logout
      </a>
    </span>
  """
  loggedOutTemplate: """
    <span class="button login">
      Login
    </span>
  """
  events:
    "click .login": "openLogin"
  
  initialize: ->
    @model = new Scheduler.User(data.user) if data.user?
    @listenTo(Scheduler.Observer, "login:success", @update)    
  
  render: =>
    if @model
      template = @loggedInTemplate
      templateData = @model.toJSON()
    else
      template = @loggedOutTemplate
      templateData = {}

    @$el.html(_.template(template)(templateData))
    @
  
  update: (_, @model) =>
    data.user = @model
    @render()
    
  openLogin: (event) =>
    event.preventDefault()
    Scheduler.Observer.trigger("login:display", @)
