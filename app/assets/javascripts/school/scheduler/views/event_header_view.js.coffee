#= require ../models/observer

class Scheduler.EventHeaderView extends Backbone.View
  className: "item event-header"
  template: """
    <div class="button view-calendar">      
      View Calendar
    </div>
    
    <% if (authenticated) { %>
      <div class="button view-booked">
        View Booked
      </div>
    <% } %>
  """
  events:
    "click .view-calendar": "viewCalendar"
    "click .view-booked": "viewBooked"
  
  initialize: ->
    super
    @model = new Scheduler.User(data.user) if data.user?
    @listenTo(Scheduler.Observer, "login:success", @update)    
    
  render: =>
    @$el.html(_.template(@template)(authenticated: @model?))
    @
    
  update: (_, model) =>
    @model = model
    @render()

  viewCalendar: =>
    Scheduler.Observer.trigger("calendar:show", @)
  
  viewBooked: =>
    Scheduler.Observer.trigger("calendar:toggleBooked", @)
