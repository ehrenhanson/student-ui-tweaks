class Scheduler.CollectionView extends Backbone.View
  initialize: ->
    @views = []

  renderView: (model) ->

  render: ->
    @reset()
    @prerender()

    for model in @collection.models
      @renderView(model)

    @postrender()
    @
    
  prerender: ->
  
  postrender: ->
    
  append: (view) ->
    @$el.append(view.render().$el)
    @views.push(view)
    
  reset: ->
    view.remove() for view in @views
    @$el.empty()    
    return

  remove: ->
    @reset()
    super
