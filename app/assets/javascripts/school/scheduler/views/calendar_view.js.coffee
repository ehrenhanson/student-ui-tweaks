#= require moment
#= require ../vendor/clndr
#= require ../models/observer

transition = ->
  availableTransitions =
    "OTransition": "oTransitionEnd"
    "MSTransition": "msTransitionEnd"
    "MozTransition": "transitionend"
    "WebkitTransition": "webkitTransitionEnd"
    "transition": "transitionEnd"
  
  el = document.createElement('DIV')  
  for transition, style of availableTransitions
    return style if el.style[transition] != undefined

class Scheduler.CalendarView extends Backbone.View
  className: "calendar"
  template: """
    <div class="close">X</div>
    <div class="control"></div>          
  """
  cldrTemplate: """
    <div class="clndr-controls">
      <div class="clndr-previous-button">&lsaquo;</div>
      <div class="month"><%- month %></div>
      <div class="clndr-next-button">&rsaquo;</div>
    </div>
    <div class="clndr-grid">
      <div class="days-of-the-week">
        <% _.each(daysOfTheWeek, function(day) { %>
          <div class="header-day">
            <span><%- day %></span>
          </div>
        <% }) %>
        <div class="days">
          <% _.each(days, function(day) { %>
            <div class="button <%- day.classes %>" data-date="<%- day.date %>">
              <span><%- day.day %></span>
            </div>
          <% }) %>
        </div>
      </div>
    </div>
  """
  events:
    "click .close": "requestClose"
  
  initialize: ->
    super
    
    @$clndr = $("<div/>")
    @clndr = @$clndr.clndr
      template: @cldrTemplate
      clickEvents:
        click: @dateChanged
        onMonthChange: @monthChanged
        
    @sync()

  render: =>
    @$el.html(_.template(@template)())
    @$el.find(".control").html(@$clndr)
    @

  dateChanged: (target) =>
    Scheduler.Observer.trigger("calendar:date", @, target.date)
    @requestClose()
  
  monthChanged: (moment) =>
    @sync()

  sync: =>
    date = @clndr.month.format("YYYY-MM-DD")
    $.get("/courses/#{@model.id}/events/date_set?start=#{date}", (data) =>
      @clndr.setEvents(_.map(data, (date) -> { date: date }))
    )
    
  requestClose: =>
    Scheduler.Observer.trigger("calendar:hide", @)

  close: ->
    @$el.addClass("close")
    _.delay(_.bind(@remove, this), 500)
