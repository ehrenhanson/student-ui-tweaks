#= require ./event_header_view
#= require ./event_footer_view
#= require ./event_date_view
#= require ./event_view

class Scheduler.EventCollectionView extends Scheduler.CollectionView
  className: "collection events"
  noEventsTemplate: """
    <p>
      There are no publicly scheduled classes at this time. Contact our school for more information.
    </p>
    
    <p class="email">
      <%- email %>
    </p>
    
    <p class="phone">
      <%- phone %>
    </p>
  """  
    
  initialize: ->
    super
    @school = new Scheduler.School(data.school)
    @listenTo(@collection, "sync", @render)
    @listenTo(Scheduler.Observer, "calendar:date", @dateChanged)
    @listenTo(Scheduler.Observer, "calendar:toggleBooked", @toggleBooked)
    @listenTo(Scheduler.Observer, "login:success", @reload)
    @listenTo(Scheduler.Observer, "registration:done", @reload)
  
  render: =>
    if @collection.length > 0
      super
    else
      @reset()
      @prerender()
      @$el.append(_.template(@noEventsTemplate)(@school.toJSON()))
      @
      
  reset: ->
    delete @lastStart
    super

  prerender: ->
    @append(new Scheduler.EventHeaderView)
    
  postrender: ->
    view = new Scheduler.EventFooterView(collection: @collection)
    @append(view)
    
  renderView: (model) ->
    start = model.get("startsAt")
    
    if !@lastStart? || @lastStart.diff(start, "days") != 0
      @append(new Scheduler.EventDateView(start: start))
    
    @append(new Scheduler.EventView(model: model))
    @lastStart = start

  dateChanged: (_, date) =>
    @collection.query.start = date
    @collection.fetch()
  
  toggleBooked: =>
    @collection.query.registered = !@collection.query.registered
    @collection.fetch()
    
  reload: =>
    @collection.fetch()