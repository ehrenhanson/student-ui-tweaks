class Scheduler.EventView extends Backbone.View
  className: "item event"
  template: """
    <div class="time">
      <p>
        <%- time %>
      </p>
    </div>
  
    <div class="instructor">
      <p>
        <%- instructor.name %>
      </p>
    </div>
  
    <div class="booking">      
    </div>
  """

  container: """
    <div class="event-details"></div>
    <div class="registration-container"></div>
  """

  initialize: ->
    super
    @bookButtonView = new Scheduler.BookButtonView(model: @model, parent: @)
    @$el.html(_.template(@container)())
    @listenTo(@model, "change", @render)
    
  render: =>
    @bookButtonView.undelegateEvents()
    
    variables =
      time: @model.get("startsAt").format("h:mmA")

    variables = _.extend(variables, @model.toJSON())  
    @$el.find(".event-details").html(_.template(@template)(variables))
    
    @$el.find(".booking").html(@bookButtonView.render().$el)
    @bookButtonView.delegateEvents()
    
    @

  registerEvent: =>
    @registrationView.remove() if @registrationView?
    @registrationView = new Registration.RegistrationView(event: @model)
    @$el.find(".registration-container").html(@registrationView.render().$el)
    
  unregisterEvent: =>
    @model.unregister()
  
  remove: ->
    @bookButtonView.remove()
    @registrationView.remove() if @registrationView?
    super
    