class Scheduler.SignupView extends Backbone.View
  id: "new-account"
  template: """
		<h2>
			Sign Up Today.
		</h2>

    <form>
      <p>
        <input type="text" name="name" placeholder="Name">
        <span class="error_name"></span>
      </p>
      
      <p>
        <input type="text" name="email" placeholder="Email">
        <span class="error_email"></span>
      </p>
      
      <p>
        <input type="password" name="password" placeholder="Password">
        <span class="error_password"></span>
      </p>
      
      <p>
        <label class="checkbox">
          <input type="checkbox" name="terms_accepted" value="true">
          I agree to the Viewcy Terms of Service
        </label>
        
        <span class="error_terms_accepted"></span>
      </p>
      
      <p>
        <button>
          Create Account
        </button>
      </p>
    </form>
  """
  events:
    "submit form": "submit"
    
  initialize: ->
    super
    @model = new Scheduler.User
    @listenTo(@model, "invalid", @render)
    
  
  render: (errors = null) =>
    @$el.html(@template)
    
    for key, value of @model.attributes
      @$el.find("[name=#{key}]").val(value)
    @$el.find("[name=terms_accepted]").attr("checked", @model.get("terms_accepted"))

    if errors?
      for name, message of errors
        @$el.find(".error_#{name}").html("#{_.capitalize(name)} #{message}")      

    @

  submit: (event) =>
    event.preventDefault()

    @model.set
      name: @$el.find("[name=name]").val()
      email: @$el.find("[name=email]").val()
      password: @$el.find("[name=password]").val()
      terms_accepted: (@$el.find("[name=terms_accepted]").prop("checked"))

    @model.save(null, success: =>
      @remove()
      Scheduler.Observer.trigger("login:success", @, @model)
    )