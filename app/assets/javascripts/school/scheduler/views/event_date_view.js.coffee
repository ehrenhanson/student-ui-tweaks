class Scheduler.EventDateView extends Backbone.View
  className: "item event-date"

  initialize: (options) ->
    @start = options.start || moment()
    super
  
  render: =>
    @$el.html(@start.format("dddd, MMMM D, YYYY"))
    @
