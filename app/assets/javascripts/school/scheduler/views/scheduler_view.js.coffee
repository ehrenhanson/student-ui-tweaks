#= require ./course_collection_view
#= require ./event_collection_view
#= require ./calendar_view

class Scheduler.SchedulerView extends Backbone.View
  el: "#scheduler"
  
  initialize: ->
    @$el.empty()
    @listenTo(Scheduler.Observer, "school:show", @closeCalendar)
    @listenTo(Scheduler.Observer, "course:show", @changeActiveCourse)
    @listenTo(Scheduler.Observer, "courses:show", @openCourseCollection)
    @listenTo(Scheduler.Observer, "events:show", @openEventCollection)
    @listenTo(Scheduler.Observer, "calendar:show", @openCalendar)
    @listenTo(Scheduler.Observer, "calendar:hide", @closeCalendar)
    @listenTo(Scheduler.Observer, "model:sync:start", @openLoading)
    @listenTo(Scheduler.Observer, "model:sync:stop", @closeLoading)

  render: =>    
    @$el.append(@view.render().$el) if @view?
    @
    
  openCourseCollection: (view, collection) =>
    @view.remove() if @view?
    @view = new Scheduler.CourseCollectionView(collection: collection)
    @render()
    
  openEventCollection: (view, collection) =>
    @view.remove() if @view?
    @view = new Scheduler.EventCollectionView(collection: collection)
    @render()

  openCalendar: =>
    return if @calendarView?
    @calendarView = new Scheduler.CalendarView(model: @course)
    @$el.append(@calendarView.render().$el)
    
  closeCalendar: =>
    @calendarView.close() if @calendarView?
    @calendarView = null
    
  openLoading: =>
    @closeLoading()
    @throbber = new Scheduler.ThrobberView
    @$el.append(@throbber.render().$el)
  
  closeLoading: =>
    @throbber.remove() if @throbber?
    
  changeActiveCourse: (_, course) =>
    @closeCalendar()
    @course = course