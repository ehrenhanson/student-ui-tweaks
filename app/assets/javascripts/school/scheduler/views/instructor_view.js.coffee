#= require ../models/observer

class Scheduler.InstructorView extends Backbone.View
  template: """
		<div class="instructor-expanded">
      <div class="button close">
        X
      </div>
      
			<div class="profile-image">
				<img src="<%- profile_image_url %>" alt="Profile Image">
			</div>
		
			<div class="details">
				<p class="name">
					<%- name %>
				</p>
		
				<p class="email">
					<a href="mailto:<%- email %>">
						<%- email %>
					</a>
				</p>
        
        <div class="description">
          <%- description %>
        </div>
    
        <div class="classes">
          <h3>
            Classes
          </h3>
          
          <ul>
            <% _.each(courses, function(course) { %>
              <li class="button" data-course-id="<%- course.id %>">
                <%- course.name %>
              </li>
            <% }) %>
          </ul>
        </div>
			</div>
		</div>
  """
  events:
    "click .close": "close"
    "click [data-course-id]": "openCourse"
  
  render: =>
    @$el.html(_.template(@template)(@model.toJSON()))
    @
    
  close: =>
    @remove()
    Scheduler.Observer.trigger("instructors:display", @)
    
  openCourse: (event) =>
    event.preventDefault()
    id = $(event.currentTarget).data("course-id")
    Backbone.history.navigate("/courses/#{id}", trigger: true)