class Scheduler.EventFooterView extends Backbone.View
  className: "item event-footer"
  template: """
    <p class="more">
      <a href="#scheduler">
        More
      </a>
    </p>
  """
  events:
    "click": "next"
    
  render: =>
    @$el.html(@template) if @collection.next  
    @
    
  next: =>
    @collection.fetchNext()