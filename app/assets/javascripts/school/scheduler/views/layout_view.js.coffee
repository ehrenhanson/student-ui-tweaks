#= require ./detail_view
#= require ./scheduler_view

class Scheduler.LayoutView extends Backbone.View
  id: "#layout"
  
  initialize: ->
    @detailView = new Scheduler.DetailView
    @schedulerView = new Scheduler.SchedulerView
  
  render: =>
    @detailView.render()
    @schedulerView.render()
    @
