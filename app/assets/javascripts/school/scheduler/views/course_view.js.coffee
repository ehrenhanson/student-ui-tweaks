class Scheduler.CourseView extends Backbone.View
  className: "item button course"
  template: """
    <h2>
      <%- name %>
    </h2>
    
    <p>
      <%- class_description %>
    </p>
  """
  events:
    "click": "showCourse"
  
  render: =>
    @$el.html(_.template(@template)(@model.toJSON()))
    @
  
  showCourse: =>
    Backbone.history.navigate("courses/#{@model.id}", trigger: true)