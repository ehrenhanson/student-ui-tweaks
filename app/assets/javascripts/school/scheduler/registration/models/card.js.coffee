#= require ./validated_model

class Registration.Card extends Registration.ValidatedModel
  defaults: ->
    number: null
    cvv: null
    exp_month: new Date().getMonth().toString()
    exp_year: (new Date().getFullYear() + 5).toString()

  requiredFields: [ "number", "cvv", "exp_month", "exp_year" ]
