#= require ./region

class Registration.RegionCollection extends Backbone.Collection
  model: Registration.Region
  
  url: ->
    if code = @country.get("code")
      "/register/regions/#{code}"

  toSelectOptions: ->
    @map (region) ->
      { display: region.get("name"), value: region.get("name") }