#= require ./address
#= require ./card
#= require ./country_collection

class Registration.RegistrationState extends Backbone.Model
  url: '/register'
  
  defaults: ->
    event: {}
    credits: []
    products: []
    credit: null
    product: null
    address: null
  
  initialize: ->
    super
    @states = ["selectProduct"]
    @modelInstances = {}
  
  fetchEvent: (event, options) ->
    url = _.result(@, "url")
    url = [ url, event.id, event.dateId ].join("/")
    @fetch(_.extend(options, url: url))
    
  validate: ->
    errors = []
    validateAssociated = (attribute) =>
      if object = @get(attribute)
        return null unless object.validate?
        if objectErrors = object.validate()
          errors = errors.concat(objectErrors)
      else
        errors.push("#{attribute} is required")

    for state in @states
      switch state
        when "inputAddress"
          validateAssociated("address")
        when "inputPayment"
          validateAssociated("card")

    return errors if errors.length > 0
  
  toJSON: ->
    attributes =
      card: _.toJSON(@get("card"))
      address: _.toJSON(@get("address"))
      
    if event = @get("event")
      attributes.event_id = event.id
      
    if credit = @get("credit")
      attributes.credit_id = credit.id
      
    if product = @get("product")    
      attributes.product_id = product.id

    attributes
  
#!- Data transformations

  models:
    address: Registration.Address
    card: Registration.Card
    countries: Registration.CountryCollection
    
  get: (name) ->
    value = super(name)
    
    if @models[name]
      @modelInstances[name] ||= new @models[name](value)
    else
      value
    
#!- Registration process states

  transitions:
    selectProduct: (callback) ->
      if @get("product")
        callback("inputAddress")
      else
        @save(null, success: (=> callback("confirmation")), error: @failedTransition)
    inputAddress:
      "inputPayment"
    inputPayment: (callback) ->
      @save(null, success: (=> callback("confirmation")), error: @failedTransition)
    confirmation:
      "done"
    error: (callback) ->
      @set("product", null)
      @save(null, success: (=> callback("confirmation")), error: @failedTransition)

  currentState: ->
    @states[@states.length - 1]
  
  previousState: ->
    @states[@states.length - 2]

  transition: (options) ->
    state = @currentState()
    state = if _.isFunction(@transitions[state])
      @transitions[state].call(this, @transitionTo)
    else
      @transitionTo(@transitions[state])

  transitionTo: (state) =>
    @states.push(state)
    @trigger("transition", @, state)
    
  failedTransition: (model, response, options) =>
    @errors = response.responseJSON
    @transitionTo("error")

  transitionBack: ->
    @states.pop()
