class Registration.ValidatedModel extends Backbone.Model
  validate: ->
    errors = []
        
    for field in @requiredFields
      errors.push("#{field} is required") if _.isEmpty(@get(field))

    return errors if errors.length > 0
