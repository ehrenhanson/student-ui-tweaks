#= require ./validated_model

class Registration.Address extends Registration.ValidatedModel
  requiredFields: [ "name", "address", "city", "state", "country", "zipcode" ]
