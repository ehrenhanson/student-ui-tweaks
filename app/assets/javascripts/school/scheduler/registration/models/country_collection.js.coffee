#= require ./country

class Registration.CountryCollection extends Backbone.Collection
  model: Registration.Country
  
  toSelectOptions: ->
    @map (country) ->
      { display: country.get("name"), value: country.get("code") }