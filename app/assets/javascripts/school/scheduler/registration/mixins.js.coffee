_.mixin
  curry: (f, args...) ->
    -> f(args...)
  
  toJSON: (object) ->
    if object && _.isFunction(object.toJSON)
      object.toJSON()
    else
      object    

_.helpers =
  selectTag: (name, options) ->
    el = $("<select/>").attr(name: name)

    for o in options
      option = $("<option/>").attr(value: o.value).html(o.display || o.value)
      el.append(option)

    el.wrapAll("<div/>").parent().html()

  selectDigitRange: (name, first, last) ->
    options = for i in [first..last]
      { value: i }
    _.helpers.selectTag(name, options)
