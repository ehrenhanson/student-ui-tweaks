#= require ./product_view
#= require ./address_view
#= require ./product_view
#= require ./confirmation_view
#= require ../../models/observer

class Registration.RegistrationView extends Backbone.View
  id: "registration-view"
  
  template: """
    <div id="prev-view">
      <button id="prev" class="icons">t</button>
    </div>
    
    <div id="content-view"></div>
    
    <div id="next-view">
      <button id="next" class="icons">r</button>
    </div>
  """

  events:
    "click #prev": "previousStep"
    "click #next": "nextStep"
  
  states:
    selectProduct: -> new Registration.ProductView(model: @model)
    inputAddress: -> new Registration.AddressView(model: @model)
    inputPayment: -> new Registration.PaymentView(model: @model)
    confirmation: -> new Registration.ConfirmationView(model: @model)
    error: -> new Registration.ErrorView(model: @model)
    
  initialize: (options) ->
    super
    @views = {}
    @isLoading = true
    @event = options.event
    @model = new Registration.RegistrationState
    @listenTo(@model, "transition", @finalizeTransition)

    @model.fetchEvent(options.event, success: @loaded)

  render: (options = {}) =>
    if @isLoading
      @$el.html("Loading...")
    else if view = @currentView()
      @$el.html(@template)
        
      $contentView = @$el.find("#content-view")
      view.setElement($contentView).render()
    
      if options.from
        $transitionView = $("<div id='content-view-transition'/>")
        $contentView.before($transitionView)
        options.from.setElement($transitionView).render()
        @animateOut($transitionView, $contentView, options.from, options.reverse)

      @toggleNavigationButtons()      
    @
  
  animateOut: ($old, $new, fromView, reverse = false) =>    
    width = $old.parent().width()
    height = Math.max($old.height(), $new.height()) + 40
    factor = if reverse then -1 else 1
    
    $old.parent().css(position: "relative", overflow: "hidden", height: height)
    $old.add($new).css(position: "absolute", width: "100%")
    $new.offset(_.extend($old.position(), left: width * factor))
    
    $old.animate(left: -(width * factor))
    $new.animate({ left: 0 }, complete: =>
      $new.css(position: "static").parent().css(height: "auto")
      @removeView(fromView)      
    )
  
  loading: =>
    @isLoading = true
    @render()
  
  loaded: =>
    @isLoading = false
    @render()
    
  previousStep: =>
    fromView = @currentView() 
    @model.transitionBack()
    @render(from: fromView, reverse: true)
    
  nextStep: =>
    if @model.isValid()
      @loading()
      @model.transition()
    else
      alert("Unable to proceed: #{@model.validationError.join(", ")}")
  
  finalizeTransition: =>
    switch @model.currentState()
      when "done"
        Scheduler.Observer.trigger("registration:done", @)    
      when "confirmation"
        @event.set("registered", true)
        window.setTimeout(@nextStep, 5000)

    @loaded()
    @render(from: @currentView(@model.previousState()))

  currentView: (state = @model.currentState()) =>
    @views[state] ||= _.extend(@states[state].call(this), parent: this) if @states[state]
  
  removeView: (viewToRemove) =>
    viewToRemove.remove()
    
    for state, view of @views
      if view == viewToRemove
        delete @views[state]
  
  toggleNavigationButtons: =>    
    @$el.find("button").show()
    
    switch @model.currentState()
      when "selectProduct"
        @$el.find("#prev").hide()
      when "confirmation", "error"
        @$el.find("#prev").hide()
        @$el.find("#next").hide()
