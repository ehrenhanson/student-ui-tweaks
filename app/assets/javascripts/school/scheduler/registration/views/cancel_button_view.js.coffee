class Registration.CancelButtonView extends Backbone.View
  template: "<a href='#'>Cancel</a>"
  events:
    "click": "callClickHandler"

  render: =>
    @$el.addClass("cancel")
    @$el.html(@template)
    @

  callClickHandler: (event) =>
    event.preventDefault()
    @clickHandler()