#= require ./template_view

class Registration.ConfirmationView extends Registration.TemplateView
  template: """
    <h2>
      See you in class
    </h2>
  """