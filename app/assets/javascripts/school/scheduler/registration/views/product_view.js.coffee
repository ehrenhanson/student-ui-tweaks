#= require ./template_view

class Registration.ProductView extends Registration.TemplateView
  template: """
<form>
  <ul id="registration-form-items">
    <li>    
      <label class="checkbox">
        <input type="radio" name="option" value="rsvp" checked="checked"<%- testEmpty(credit, product) %>>
        RSVP Only
      </label>
    </li>
  
    <% _.each(credits, function(_credit) { %>
      <li>
        <label class="checkbox">
          <input type="radio" name="option" value="credit|<%- _credit.id %>"<%- testChecked(credit, _credit) %>>
          <%- _credit.name %> (<%- _credit.remaining %> remaining)
        </label>
      </li>
    <% }) %>
    
    <% _.each(products, function(_product) { %>
      <li>
        <label class="checkbox">
          <input type="radio" name="option" value="product|<%- _product.id %>"<%- testChecked(product, _product) %>>
          Pay for
          <%- _product.name %>
          (<%- _product.price %>)
        </label>
      </li>
    <% }) %>  
  </ul>
</form>
"""
  helpers:
    testChecked: (a, b) ->
      "checked='checked'" if a == b
    testEmpty: (args...) ->
      "checked='checked'" unless _.some(args)

  events:
    "click input[name=option]": "optionChanged"
    
  optionChanged: =>
    value = @$el.find("input[name=option]:checked").val()
    return unless value?
    
    @model.set("product", null)
    @model.set("credit", null)
    
    [ type, id ] = value.split("|")
    switch type
      when "product", "credit"
        model = _.find(@model.get("#{type}s"), (elem) -> elem.id == id)
        @model.set(type, model)
