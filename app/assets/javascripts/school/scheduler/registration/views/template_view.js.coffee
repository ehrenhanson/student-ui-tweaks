class Registration.TemplateView extends Backbone.View
  initialize: ->
    @listenTo(@model, "change", @render) if @model?
  
  render: =>
    if _.isFunction(@template)
      template = @template
    else
      template = _.template(@template)

    @$el.html(template(@attrs()))
    @

  attrs: =>      
    attrs = {}
    attrs = _.clone(@model.attributes) if @model?
    attrs = _.extend(attrs, @helpers) if @helpers?
    attrs
