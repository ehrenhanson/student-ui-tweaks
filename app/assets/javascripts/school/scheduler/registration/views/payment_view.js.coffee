#= require ./template_view

class Registration.PaymentView extends Registration.InputView
  property: "card"
  template: """
  <form>
    <fieldset>
      <legend>
        Card Details
      </legend>

      <p>
        Buying <%- product.name %> (<%- product.price %>)
      </p>
      
      <input name="number" placeholder="Card Number">
      <input name="cvv" placeholder="CVV">
      <%= _.helpers.selectDigitRange("exp_month", 1, 12) %>
      <%= _.helpers.selectDigitRange("exp_year", new Date().getFullYear(), new Date().getFullYear() + 20) %>
    </fieldset>
  </form>
  """
