#= require ./cancel_button_view

class Registration.PopupView extends Backbone.View
  el: "#dropdown"

  @instance: ->
    return PopupView.currentInstance if PopupView.currentInstance?
    PopupView.currentInstance = new PopupView
    
  @replace: (klass) ->
    (event) ->
      event.preventDefault()
      PopupView.instance().replaceChild(new klass(context: this))
        
  initialize: ->
    super
    @cancelButtonView = new CancelButtonView(el: "#dropdown-cancel-button")
    @cancelButtonView.clickHandler = @close
    @$childContent = $("#dropdown-child-content")
    
  replaceChild: (@childView) =>
    @close(complete: @render)
  
  render: (content = null) =>
    @cancelButtonView.render()

    if content != null
      @$childContent.html(content)
      @open()
    else if @childView?
      @$childContent.html(@childView.render().el)
      @open() unless _.result(@childView, "remote")

    @
    
  open: =>
    toggleLogo(true)
    @$el.stop().show().css(top: -@$el.height()).animate({ top: 0 }, easing: "easeOutBounce")
    
  close: (options) =>
    toggleLogo(false)
    @$el.slideUp(options)
  
  pushIntoNavigationView: (view) =>
    unless @childView? && @childView.constructor.name == "NavigationView"
      @replaceChild(new NavigationView)
    
    @childView.push(view)
    
  remove: =>
    @cancelButton.remove()
    super

# Helper method to allow the logo to change to white when the dialog is visible
toggleLogo = (dialogActive) ->
  src = if dialogActive
    "/assets/logo-white.png"
  else
    "/assets/logo.png"    
  $("#global-logo img").attr(src: src)
