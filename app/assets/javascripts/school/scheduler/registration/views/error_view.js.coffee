class Registration.ErrorView extends Registration.TemplateView
  template: """
    <h2>
      Oops :(
    </h2>
  
    <p>
      <%- errors %>
    </p>
    
    <p>
      <button id="rsvp-only">RSVP Only</button>
      <button id="try-again">Try Paying Again</button>
    </p>
  """
  
  events:
    "click #rsvp-only": "rsvp"
    "click #try-again": "tryAgain"
  
  attrs: =>    
    if _.isObject(@model.errors)
      errors = []
      for key, message of @model.errors
        errors.push(message)
        
    _.extend(super, errors: errors)

  rsvp: =>
    @parent.nextStep()
  
  tryAgain: =>
    @parent.previousStep()