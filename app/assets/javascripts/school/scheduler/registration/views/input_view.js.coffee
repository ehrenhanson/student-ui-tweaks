#= require ./template_view

class Registration.InputView extends Registration.TemplateView
  events:
    "change :input": "inputChange"
    
  initialize: ->
    super
    @object = @model.get(@property)
  
  render: =>
    super
    for key, value of @object.attributes
      @$el.find("[name=#{key}]").val(value)
  
  inputChange: (event) =>
    el = $(event.target)
    @object.set(el.attr("name"), el.val())