#= require ./input_view
#= require ./../models/country

class Registration.AddressView extends Registration.InputView
  property: "address"
  template: """
  <form>
    <fieldset>
      <legend>
        Billing Address
      </legend>

      <p>
        Buying <%- product.name %> (<%- product.price %>)
      </p>
      
      <input name="name" placeholder="Name">
      <input name="address" placeholder="Address">
      <input name="address_2" placeholder="Address 2">
      <input name="city" placeholder="City">
      <input name="state" placeholder="State">
      <%= _.helpers.selectTag("country", new Registration.CountryCollection(countries).toSelectOptions()) %>
      <input name="zipcode" placeholder="Zipcode">
    </fieldset>
  </form>
  """
  
  events:
    "change :input": "inputChange"
    "change [name=country]": "countryChanged"
  
  render: ->
    super
    @countryChanged()
  
  countryChanged: =>
    if value = $("[name=country]").val()
      @country = new Registration.Country
      @country.set("code", value)
      @country.regions().fetch(success: @update)
  
  update: (collection) =>
    el = $("[name=state]")
    value = el.val()
    
    if collection.length > 0
      el.replaceWith(_.helpers.selectTag("state", collection.toSelectOptions()))
    else
      el.replaceWith("<input name='state' placeholder='State'/>")
    
    $("[name=state]").val(value)