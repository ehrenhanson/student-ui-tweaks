#= require ../models/observer

class Scheduler.MainRouter extends Backbone.Router
  routes:
    "": "school"
    "courses/:id": "course"

  initialize: ->
    new Scheduler.LayoutView().render()

  school: =>
    model = new Scheduler.School(data.school)
    collection = new Scheduler.CourseCollection(data.courses)
    
    Scheduler.Observer.trigger("school:show", @, model)
    Scheduler.Observer.trigger("courses:show", @, collection)
    
  course: (id) =>
    if data.course? && data.course.id == id
      model = new Scheduler.Course(data.course)
    else
      model = new Scheduler.Course(id: id)
      model.fetch()
    
    Scheduler.Observer.trigger("course:show", @, model)
    Scheduler.Observer.trigger("events:show", @, model.events())
    model.events().fetch()