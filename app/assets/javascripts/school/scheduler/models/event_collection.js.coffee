#= require ./event

encode = (object) ->
  pairs = []
  
  for key, value of object
    if value._isAMomentObject
      value = value.format("YYYY-MM-DD")
    
    key = encodeURIComponent(key)
    value = encodeURIComponent(value)
    
    pairs.push("#{key}=#{value}")
    
  pairs.join("&")
  
class Scheduler.EventCollection extends Backbone.Collection
  model: Scheduler.Event
  
  initialize: (parent, models = null, options = null) ->
    super(models, options)    
    @query = {}
    @parent = parent
    @listenTo(@parent, "sync", @triggerSync)
  
  url: ->
    "/courses/#{@parent.id}/events?#{encode(@query)}"
  
  fetchNext: ->
    @query = {}
    @fetch(url: @next)

  parse: (attributes) ->
    @next = attributes.next
    attributes.events

  triggerSync: =>
    @set(@parent.attributes.events)
    @trigger("sync", @)