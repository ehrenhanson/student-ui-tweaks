class Scheduler.User extends Backbone.Model
  url: "/users"
  defaults:
    name: null
    email: null
    password: null
    terms_accepted: false
  
  @authenticate: (email, password, options) ->
    data =
      email: email
      password: password

    $.post("/auth/login.json", data).success((json) ->
      options.success(new Scheduler.User(json.user)) if options.success?
    ).fail(->
      options.fail() if options.fail?
    )
