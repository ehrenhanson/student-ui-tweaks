class Scheduler.CourseFilter
  constructor: (@collection) ->
  
  setDate: (@date) ->
    @collection.fetch()
    
  reset: ->
    @setDate(null)
    
  present: ->
    @date
  
  description: ->
    @date.format() if @date?
    
  params: ->
    "start=" + @date.format("YYYY-MM-DD") if @date?