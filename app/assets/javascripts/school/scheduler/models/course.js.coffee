#= require ./event_collection

class Scheduler.Course extends Backbone.Model
  urlRoot: "/courses"
  defaults:
    name: null
    description: null
    class_description: null
    event_description: null
    product_description: null
    instructors: []
    
  initialize: ->
    @eventCollection = new Scheduler.EventCollection(@)
    @instructorCollection = new Scheduler.InstructorCollection
  
  events: =>
    @eventCollection.set(@attributes.events)
    @eventCollection

  instructors: =>
    @instructorCollection.set(@attributes.instructors)
    @instructorCollection
