#= require ./course

class Scheduler.CourseCollection extends Backbone.Collection
  model: Scheduler.Course  
  urlRoot: "/courses"