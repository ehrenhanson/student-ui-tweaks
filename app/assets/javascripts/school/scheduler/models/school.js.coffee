class Scheduler.School extends Backbone.Model
  defaults:
    name: null
    description: null
    email: null
    phone: null
    address_address: null
    address_city: null
    logo_image: null
    header_image: null
