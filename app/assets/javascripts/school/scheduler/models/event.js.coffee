class Scheduler.Event extends Backbone.Model
  initialize: (attributes) ->
    super
    @attributes.startsAt = moment(attributes.starts_at)
    @attributes.endsAt = moment(attributes.ends_at)
    @dateId = @attributes.startsAt.format("YYYY-MM-DD")
    
  unregister: ->
    $.ajax
      url: "/unregister/#{@id}/#{@dateId}"
      method: "POST"
      beforeSend: (xhr) ->
        xhr.setRequestHeader('X-CSRF-Token', Backbone.csrfToken)
      success: =>
        @set("registered", false)
