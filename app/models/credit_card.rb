class CreditCard
  include ActiveModel::Model

  attr_accessor :number
  attr_accessor :expiration_month
  attr_accessor :expiration_year
  
  def attributes=(attributes)
    attributes.each do |key,value|
      public_send("#{key}=", value)
    end
  end
  
  def to_transaction_params
    {
      number: number,
      expiration_month: expiration_month,
      expiration_year: expiration_year
    }
  end
end