class Payment
  class PaymentError < StandardError; end
  
  cattr_accessor(:gateway) { Braintree::Transaction }
  attr_reader :order, :user, :card  
  
  def initialize(order, card)
    @order = order
    @user = order.user
    @card = card
  end
  
  def process!
    response = if user.vault_token
      gateway.sale(vault_params)
    else
      gateway.sale(card_present_params)
    end
    
    response.success? || raise(PaymentError, error_message(response))
  end
  
  protected
    def error_message(response)
      message = response.errors.map(&:message).to_sentence
      message = 'Unknown payment error' if message.blank?
      message
    end
    
    def card_present_params
      {
        amount: order.total,
        customer: {
          first_name: user.billing_address_first_name,
          last_name: user.billing_address_last_name,
        },
        credit_card: {
          number: card[:number],
          expiration_date: "#{card[:exp_month]}/#{card[:exp_year]}",
          cvv: card[:cvv]
        },
        billing: {
          street_address: user.billing_address_address,
          extended_address: user.billing_address_address_2,
          locality: user.billing_address_city,
          region: user.billing_address_state,
          postal_code: user.billing_address_zipcode,
          country_code_alpha2: user.billing_address_country
        },
        options: {
          store_in_vault: true,
          add_billing_address_to_payment_method: true
        }
      }
    end
    
    def vault_params
      {
        amount: total,
        payment_method_token: user.vault_token
      }
    end
end