class User < ActiveRecord::Base
  include Role
  include AddressValidation
  include FullName
  include Role::Property
  include User::Authentication
  include User::Tokens
  include User::RegistrationState
  include User::Merge
  include User::Filter

  has_one :billing_address, BillingAddress::DEFAULT_SCOPE, as: :addressable
  has_one :active_affiliation, -> { where(active: true) }, class_name: 'Affiliation'
  has_one :active_school, through: :active_affiliation, source: :school
  has_many :affiliations, dependent: :destroy, inverse_of: :user
  has_many :schools, through: :affiliations, extend: User::Schools
  has_many :credits, dependent: :destroy
  has_many :orders, dependent: :nullify
  has_many :registrations, dependent: :destroy
  has_many :affiliated_courses, through: :schools, source: :courses, extend: User::Affiliations
  has_many :affiliated_affiliations, through: :schools, source: :affiliations
  has_many :affiliated_users, through: :affiliated_affiliations, source: :user, extend: User::Affiliations
  has_many :events, foreign_key: :instructor_id, dependent: :destroy
  has_many :courses, through: :events
  
  has_secure_password validations: false
  has_attached_file :profile_image, styles: { thumb: [ '40x40', :png ] }, processors: [ :profile ],
    url: '/system/users/:attachment/:id_partition/:style/:filename'
    
  validates :name, presence: true
  validates :email, uniqueness: true, email: true, allow_blank: true
  validates_attachment :profile_image, content_type: { content_type: %w(image/jpeg image/jpg image/png) }
  validates_address :billing_address
  
  accepts_nested_attributes_for :billing_address
  define_merge_associations :billing_address, :events, :credits, :affiliations, :registrations, :orders  

  delegate :id, to: :active_school, prefix: true, allow_nil: true
  
  def profile_image_url
    profile_image.url(:thumb)
  end

  # Delegate all billing_address prefixed messages without knowing the names ahead of time
  def method_missing(method, *args, &block)
    if method.to_s =~ /billing_address_(.+)/
      billing_address.send($1, *args, &block) if billing_address
    else
      super
    end
  end
end
