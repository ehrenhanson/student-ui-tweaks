class BillingAddress < Address  
  DEFAULT_SCOPE = -> { where(address_type: 'billing') }
  default_scope DEFAULT_SCOPE
  
  after_initialize do |address|
    address.address_type = 'billing'
  end
end