class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  belongs_to :event
  has_one :credit
  
  before_save :set_price
  
  validates :product, presence: true

  delegate :user, to: :order
  delegate :name, :price, to: :product, prefix: true, allow_nil: true
  
  concerning :Price do
    def price
      read_attribute(:price) || product_price
    end
  
    protected
      def set_price
        read_attribute(:price) || write_attribute(:price, product.price)
      end
  end
  
  concerning :Purchasing do
    def generate_credit!
      create_credit!(
        user: user,
        product: product,
        value: product.credit_value,
        unlimited: product.unlimited
      )
    end
  end
end
