class Address < ActiveRecord::Base
  include FullName
  
  attr_accessor :address_required
  
  belongs_to :addressable, polymorphic: true
  
  validates :address, :city, :state, :zipcode, :country, presence: true, if: :required?
  
  def name
    super || (addressable.name if addressable.respond_to?(:name))
  end
  
  protected
    def required?
      filled? || address_required
    end
  
    def filled?
      %w(address city state zipcode).any? { |attribute| read_attribute(attribute).present? }
    end
end
