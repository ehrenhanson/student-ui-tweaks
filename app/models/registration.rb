class Registration < ActiveRecord::Base
  include Registration::RegisteredSet
  
  belongs_to :user
  belongs_to :event
  belongs_to :credit
  
  scope :between, ->(start_date, end_date) { where(starts_on: start_date..end_date) }
  scope :under_school, ->(school) { joins(:event => :course).where(:'courses.school_id' => school) }
  
  delegate :email, to: :user, prefix: true
  delegate :name, to: :event, prefix: true
  
  def assumed_event
    @assumed_event ||= event.assume(starts_on)
  end
  
  def rsvp?
    !credit
  end
end
