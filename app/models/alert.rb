class Alert
  include Enumerable
  
  ALERTS = [ 'OrderAlert', 'AppearanceAlert' ]
  
  attr_reader :school, :max_age
  delegate :each, :size, to: :alerts
  
  def initialize(school, max_age = 1.day.ago)
    @school, @max_age = school, max_age
  end
  
  def filter(max_age)
    @max_age = max_age
    @alerts = nil
    
    self
  end
  
  def alerts
    @alerts ||= ALERTS.each.with_object([]) do |alert_class_name,alerts|
      previously_found_alert = nil
      alert_class = alert_class_name.constantize
      
      alert_class.find(school, max_age).each do |alert|
        # When successive alerts have the same content, group the user protion
        # of the alert into a single grouped output. i.e. "2 users did this"
        if alert.same_as?(previously_found_alert)
          previously_found_alert.bump!
        else
          previously_found_alert = alert
          alerts << alert
        end
      end
    end.map(&:call).sort_by! { |date,_| date }
  end
end