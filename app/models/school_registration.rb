class SchoolRegistration
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include ActiveModel::Callbacks
  include SchoolRegistration::Merchant
  
  attr_reader :affiliation
  attr_accessor :ssn, :tax_id, :account_number, :routing_number
  
  class RecordValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      unless value.valid?
        value.errors.each do |key,message|
          record.errors.add("#{attribute}.#{key}", message)
        end
      end
    end
  end
  
  validates :ssn, :tax_id, :account_number, :routing_number, presence: true
  validates :affiliation, record: true
  
  delegate :user, :school, to: :affiliation
  delegate :attributes, :attributes=, to: :user, prefix: true
  delegate :attributes, :attributes=, :identifier, to: :school, prefix: true
  
  def initialize(user, attributes = nil)
    @affiliation = Affiliation.new(user: user, school: School.new, role: Role.owner)
    user.billing_address_required = true
    self.attributes = attributes if attributes
  end
  
  def attributes=(attributes = {})
    attributes.each do |key,value|
      send("#{key}=", value) if respond_to?("#{key}=")
    end
  end
  
  def persisted?
    false
  end
  
  def save!(options = {})
    Affiliation.transaction do
      raise(ActiveRecord::RecordInvalid.new(self)) unless valid?(options)
      affiliation.save!(options)
      register_merchant_account!
    end      
  end
  
  def save(options = {})
    save!(options)
  rescue ActiveRecord::RecordInvalid
    false
  end
end
