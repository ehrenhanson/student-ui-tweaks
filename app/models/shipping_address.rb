class ShippingAddress < Address
  DEFAULT_SCOPE = -> { where(address_type: 'shipping') }
  default_scope DEFAULT_SCOPE
  
  after_initialize do |address|
    address.address_type = 'shipping'
  end
end