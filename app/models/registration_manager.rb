class RegistrationManager
  extend ActiveModel::Naming
  
  attr_reader :school, :user, :registration, :errors
  
  # RegistrationManager.human_attribute_name is called by the ActiveModel::Errors class
  # when passed attributes from other models during rescue calls. Passing the message on
  # to ActiveRecord aliviates the NoMethodError.
  def self.human_attribute_name(*args)
    ActiveRecord::Base.human_attribute_name(*args)
  end
  
  def initialize(school, user, attributes)
    @school, @user, @attributes = school, user, attributes
    @errors = ActiveModel::Errors.new(self)
  end
  
  def event
    @event ||= @school.events.find_on(@attributes[:event_id])
  end
  
  def save
    @registration = user.registrations.build(event: event_instance, starts_on: event.starts_on, credit: find_or_purchase_credit)

    user.address_required = product?
    user.billing_address_attributes = address

    user.save!
  rescue ActiveRecord::RecordInvalid => error
    error.record.errors.each do |attribute,message|
      errors.add(attribute, message)
    end
    false
  rescue ActiveRecord::RecordNotFound, Payment::PaymentError => error
    errors.add(:base, "Registration failed: '#{error.message}'")
    false
  end
  
  protected
    def find_or_purchase_credit
      case
      when product?
        order = user.orders.build(school: school)
        order.build_order_item(product: school.products.find(product_id))
        order.save_and_generate_credit!(card)
      when credit?
        user.credits.find(credit_id)
      end
    end
  
    def event_instance
      event.event_instance
    end
    
    def product?
      product_id
    end
    
    def product_id
      @attributes[:product_id]
    end
    
    def credit?
      credit_id
    end
    
    def credit_id
      @attributes[:credit_id]
    end
    
    def card
      @attributes[:card]
    end
    
    def address
      @attributes[:address] || {}
    end
end