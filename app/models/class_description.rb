class ClassDescription
  attr_reader :course, :events, :products
  
  DAY_NAMES = %w(Sunday Monday Tuesday Wednesday Thursday Friday Saturday).freeze
  DAY_ENUMS = DAY_NAMES.map { |day| Event.const_get(day.upcase) }.freeze  

  NONE = 0
  
  DAILY = 1
  WEEKLY = 2
  MONTHLY = 4
  UPCOMING = 8
  
  UNLIMITED = 1
  MULTIPLE = 2
  SINGLE = 4
  
  def initialize(course, date = Date.today)
    @course = course
    @events = course.events
    @products = course.products
    @date = date
  end
  
  def event_part
    @event_part ||= begin
      query = Event
          .select('SUM(DISTINCT repeat_type) AS repeat_type_value, SUM(DISTINCT repeat_interval) AS repeat_interval_value')
          .where('course_id = ? AND repeat_type > ? AND (repeat_until <= ? OR repeat_until IS NULL)', course,
            Event::REPEAT_NONE, @date)
          
      type, interval = events.connection.select_one(query.to_sql).values_at('repeat_type_value', 'repeat_interval_value')
  
      if test(type, Event::REPEAT_DAILY)
        [ DAILY, 'Daily classes available' ]
      elsif test(type, Event::REPEAT_WEEKLY)
        [ WEEKLY, 'Weekly: ' << days(interval) ]
      elsif test(type, Event::REPEAT_MONTHLY)
        [ MONTHLY, 'Monthly classes available' ]
      elsif event = course.next_event(@date.to_time)
        [ UPCOMING, "Next class on #{event.starts_on.to_s(:long_ordinal)}" ]
      else
        [ NONE, 'No classes are scheduled at this time' ]
      end
    end
  end
  
  def product_part
    @product_part ||= begin
      product = products.cheapest.first    
      return [ NONE, 'No credits are available for purchase at this time' ] unless product

      price = product.price.to_money.format

      if product.unlimited?
        [ UNLIMITED, "Full pass for #{price}" ]
      elsif product.credit_value > 1
        [ MULTIPLE, "#{product.credit_value} classes for #{price}" ]
      else
        [ SINGLE, "#{price}/class" ]
      end
    end
  end
  
  def event_description
    event_part.last
  end
  
  def product_description
    product_part.last
  end
  
  def full_description
    event_type, event_description = event_part
    product_type, product_description = product_part
    
    if event_type == NONE || product_type == NONE
      course.description
    else
      "#{event_description}, #{product_description}"
    end
  end
  
  private  
    def test(bitfield, mask)
      bitfield && bitfield & mask > 0
    end
    
    def days(interval)
      days = DAY_ENUMS.each.with_index.inject([]) do |acc,(mask,index)|
        acc << DAY_NAMES[index] if test(interval, mask)
        acc
      end.to_sentence
    end
end