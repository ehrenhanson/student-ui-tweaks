class RegistrationState
  attr_reader :school, :user, :event_id, :start_date
  
  def initialize(school, user, event_id, start_date)
    @school = school
    @user = user
    @event_id = event_id
    @start_date = start_date
    
    # Place the school in RSVP-only mode unless in the approved state
    unless school.approved?
      @credits = []
      @products = []
    end
  end
  
  def event
    @event ||= school.events.find_on(event_id, start_date)
  end
  
  def credits
    @credits ||= user.credits.of_course(event.course).find_remaining.unused
  end
  
  def products
    @products ||= school.products.excluding(credits)
  end
  
  def countries
    @countries ||= Carmen::Country.all
  end
end