class Credit < ActiveRecord::Base
  include Credit::Remaining
  
  belongs_to :product
  belongs_to :user
  has_many :placements, through: :product
  has_many :courses, through: :placements
  
  validates :product, :user, :value, presence: true
  
  scope :of_school, ->(school) { joins(product: { placements: :course }).where(courses: { school_id: school }).distinct }
  scope :of_course, ->(course) { joins(product: :placements).where(placements: { course_id: course }) }
  
  delegate :name, to: :product, prefix: true, allow_nil: true
  
  def product=(product)
    self.value ||= product.credit_value if product
    super
  end
  
  concerning :SchoolAffiliations do
    included do
      after_create :join_school  
    end
    
    protected
      def join_school
        Affiliation.create(school: product.school, user: user, role: Role.student)
      rescue ActiveRecord::RecordNotUnique
      end
  end
end
