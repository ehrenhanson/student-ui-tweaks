class FilteredEnumerator
  include Enumerable
  
  def initialize(collection, &filter)
    @collection, @filter = collection, filter
  end
  
  def each
    return to_enum unless block_given?
    
    @collection.each do |item|
      yield(item) if @filter.call(item)
    end
  end
end