class Appearance < ActiveRecord::Base
  belongs_to :course
  belongs_to :affiliation
  has_one :user, through: :affiliation
end
