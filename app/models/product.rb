class Product < ActiveRecord::Base
  include Product::Credits
  
  belongs_to :school
  
  has_many :placements, dependent: :destroy
  has_many :courses, through: :placements
  has_many :credits, dependent: :destroy
  
  scope :cheapest, -> { order(arel_table[:price]) }
  
  before_save :save_course_associations
  
  validates :name, :credit_value, :price, presence: true
  
  def course_ids
    @course_ids ||= placements.select(:course_id).map(&:course_id)
  end
  
  def course_ids=(ids)
    ids = Array.wrap(ids)
    course_ids = placements.select(:course_id).map(&:course_id)
    
    @inclusive_course_set = (ids - course_ids)
    @exclusive_course_set = (course_ids - ids)

    @course_ids = ids
  end
  
  protected
    def save_course_associations    
      placements.where(course_id: @exclusive_course_set).destroy_all if @exclusive_course_set
      @inclusive_course_set.each { |course_id| placements.build(course_id: course_id) } if @inclusive_course_set
    end
end
