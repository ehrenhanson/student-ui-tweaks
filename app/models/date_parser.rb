class DateParser
  class InvalidDate < ArgumentError; end
  
  def initialize(date)
    @date = date
  end
  
  def to_date
    case @date
    when Date
      @date
    when Time
      @date.to_date
    when String
      Date.parse(@date)
    when Array
      Date.parse(@date.join('-'))
    else
      raise ArgumentError, %(input must be a Date, Time, or String)
    end
  rescue ArgumentError => error
    raise InvalidDate, error.message
  end
  
  def to_time
    case @date
    when Date
      @date.to_time
    when Time
      @date
    when String
      Time.parse(@date)
    when Array
      Time.parse(@date.join('-'))
    else
      raise ArgumentError, %(input must be a Date, Time, or String)
    end
  rescue ArgumentError => error
    raise InvalidDate, error.message
  end
end