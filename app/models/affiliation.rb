class Affiliation < ActiveRecord::Base
  include Role
  include Role::Property
  
  belongs_to :user, inverse_of: :affiliations
  belongs_to :school
  
  validates :school, :user, presence: true
  validates :user, uniqueness: { scope: :school }
  validates_associated :school, on: :create
  
  delegate :identifier, to: :school, prefix: true
  accepts_nested_attributes_for :user, :school
  
  def self.update_active_school(school_id)
    transaction do
      update_all(active: false)
      where(school_id: school_id).update_all(active: true)
    end
  end
end
