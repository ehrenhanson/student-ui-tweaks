class RoleFilter
  class Token
    # Match filter string. Each token is separated by a command and can contain an
    # optional operator greater than or less than character.
    # 
    # Toekn descriptions:
    #   >student -> Returns all users who are a student or above
    #   <faculty -> Returns all users who are a faculty member or below
    #   student  -> Returns all users who are students
    #
    # Example filters:
    #   student,faculty -> Returns all users who are students or faculty
    #   student,>owner -> Returns all users who are students and owners or above
    #
    # Supplying an invalid role will raise Role::InvalidRole.
    ROLE_MATCH = /([<>])?(.+?)(,|$)/
    
    def self.parse(roles)
      roles.to_s.scan(ROLE_MATCH).map do |operation,role,_|
        new(operation, role)
      end
    end
    
    def initialize(operation, role)
      @operation, @role = operation, Role.unstringify!(role)
    end
    
    def call
      attribute = Arel.sql(Babelq.translate.greatest('users.role', 'affiliations.role'))
      
      case @operation
      when '>'
        attribute.gteq(@role.to_int)
      when '<'
        attribute.lteq(@role.to_int)
      else
        attribute.eq(@role.to_int)
      end
    end
  end
  
  def initialize(context, roles)
    @tokens = Token.parse(roles)
    @context = context.joins(:affiliations)
  end
 
  def all
    if @tokens.present?
      conditions = @tokens.map(&:call).reduce(:or)
    end
    
    @context.distinct.where(conditions)
  end
end