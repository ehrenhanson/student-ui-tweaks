class School < ActiveRecord::Base
  include Role::Property
  include AddressValidation
  include MerchantState
  
  has_one :owner_affiliation, -> { where(role: Role.owner) }, class_name: 'Affiliation'
  has_one :owner, through: :owner_affiliation, source: :user
  has_one :address, as: :addressable, dependent: :destroy    
  has_many :courses, dependent: :destroy
  has_many :products, dependent: :destroy
  has_many :events, through: :courses
  has_many :instructors, -> { distinct }, through: :events
  has_many :appearances, through: :courses
  has_many :affiliations, dependent: :destroy
  has_many :users, through: :affiliations, extend: School::Users
  has_many :orders, dependent: :nullify

  accepts_nested_attributes_for :address
  has_attached_file :header_image
  has_attached_file :logo_image

  validates :name, presence: true
  validates :identifier, presence: true, uniqueness: true, exclusion: { in: %w(www signup) }
  validates :gplus_url, :facebook_url, :twitter_url, format: URI.regexp([ 'http', 'https' ]), allow_blank: true
  validates_attachment :header_image, :logo_image, content_type: { content_type: %w(image/jpeg image/jpg image/png) }
  validates_address :address
  
  delegate :name, :email, to: :owner, prefix: true, allow_nil: true
  delegate :address, :address_2, :city, :state, :zipcode, to: :address, prefix: true, allow_nil: true

  def self.as_role(role)
    role = Role.unstringify(role.to_s)
    select(arel_table[Arel.star], "#{role} AS role")
  end  
  
  def alerts
    Alert.new(self)
  end
  
  def address_required
    true
  end
  
  # has_many/through for placements join was intermittently generating errors
  # and we only need to return list of placements scoped by the school
  def placements
    Placement.joins(:product).where(Product.arel_table[:school_id].eq(id))
  end
  
  alias_method :address_address_tmp, :address_address
  def address_address
    address_address_tmp
  end
  
  def tz
    ActiveSupport::TimeZone.new(timezone)
  end
  
  concerning :Discovery do
    included do
      def self.discover(id)
        find_by('schools.id = :id OR schools.identifier = :id', id: id) ||
          raise(ActiveRecord::RecordNotFound, "Couldn't find #{self} with id=#{id} or identifier=#{id}")
      end
  
      def self.discover_merchant(id)
        discover(id.to_s.sub(/_school$/, ''))
      end
    end
  end
  
  concerning :Identifier do
    included do
      before_save :set_identifier
      before_validation :set_identifier
    end
    
    def identifier=(identifier)
      identifier = identifier.downcase.gsub(/[^a-z0-9]/, '') if identifier.is_a?(String)
      super
    end
    
    protected
      def set_identifier
        self.identifier ||= name
      end
  end
end
