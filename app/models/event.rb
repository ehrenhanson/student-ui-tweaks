class Event < ActiveRecord::Base
  include Event::Recurrence
  
  attr_accessor :registered
  
  belongs_to :instructor
  belongs_to :course
  has_one :school, through: :course
  has_many :exclusions, dependent: :destroy
  has_many :order_items, dependent: :nullify
  has_many :registrations, dependent: :destroy
  
  validates :starts_at, :ends_at, :instructor, presence: true
  
  delegate :name, :description, to: :course, allow_nil: true
  delegate :name, :user_name, :products, to: :course, prefix: true, allow_nil: true
  delegate :id, :name, :profile_image, :profile_image?, to: :instructor, prefix: true, allow_nil: true
  
  def parent?
    true
  end
  
  def excluded?
    false
  end
  
  def instructor=(instructor)
    instructor = instructor.becomes(Instructor) if instructor.is_a?(User)
    super(instructor)
  end
  
  # Method compatable with Event::Proxy
  def event_instance
    self
  end
end
