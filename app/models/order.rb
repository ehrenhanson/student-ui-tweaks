class Order < ActiveRecord::Base
  include Checkout
  include CreditCardAccessors
  
  belongs_to :user
  belongs_to :school
  has_one :order_item, dependent: :destroy
  
  before_save :save_associated, if: :user_dirty?

  delegate :billing_address, to: :user, prefix: true

  def user_billing_address_attributes=(attributes)
    @user_dirty = true
    user.billing_address_attributes = attributes
  end
    
  protected
    def user_dirty?
      @user_dirty
    end
    
    def save_associated
      user.save
    end
end
