module SchoolRegistration::Merchant
  extend ActiveSupport::Concern
  
  included do
    cattr_accessor(:merchant_processor) { Braintree::MerchantAccount }
  end
  
  def register_merchant_account!
    result = merchant_processor.create(account_attributes)
    result.success? || begin
      errors[:merchant_account] = result.message
      raise(ActiveRecord::RecordInvalid.new(self))
    end
  end
  
  protected
    def account_attributes
      first_name, last_name = FullNameSplitter.split(user.name)
      
      {
        master_merchant_account_id: '9snyxvf7356cb6kr',
        id: "#{school.identifier}_school",
        individual: {
          first_name: first_name,
          last_name: last_name,
          email: user.email,
          phone: user.phone,
          date_of_birth: user.date_of_birth.to_s,
          ssn: ssn,
          address: {
            street_address: user.billing_address.address,
            locality: user.billing_address.city,
            region: user.billing_address.state,
            postal_code: user.billing_address.zipcode
          }
        },
        business: {
          legal_name: school.legal_name,
          dba_name: school.name,
          tax_id: tax_id,
          address: {
            street_address: school.address.address,
            locality: school.address.city,
            region: school.address.state,
            postal_code: school.address.zipcode
          }
        },
        funding: {
          destination: Braintree::MerchantAccount::FundingDestination::Bank,
          account_number: account_number,
          routing_number: routing_number
        },
        tos_accepted: school.tos_accepted   
      }
    end
end