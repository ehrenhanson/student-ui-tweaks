class Event::OccurrenceEnumerator
  include Enumerable
  
  attr_reader :start_date, :end_date
  
  def initialize(start_date, end_date, events)
    @start_date, @end_date, @events = start_date.to_date, end_date.to_date, events
  end
  
  def each(&block)
    return to_enum unless block_given?

    range.each do |date|
      @events.each { |event| proxy(event, date, &block) }
    end
  end
  
  def to_date_set
    range.each.with_object(Set.new) do |date,set|
      @events.each { |event| set << date if event.occurring?(date) }
    end
  end
  
  protected
    def range
      start_date..end_date
    end

    def proxy(event, date)
      if event.occurring?(date)
        # Yield an event proxy instance so that the starts_at/ends_at values can be
        # modified without affecting the original event instance. This negates the
        # need to dup the event.
        yield Event::Proxy.new(event, date)
      end
    end    
end
