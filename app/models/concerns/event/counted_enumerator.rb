class Event::CountedEnumerator
  include Enumerable
  
  attr_accessor :filter

  def initialize(events, registrations, start_date, end_date, max = 100)
    @events, @registrations,  = events, registrations
    @start_date, @end_date = start_date, end_date
    @date_span = end_date - start_date
    @max, @count, @miss = max, 0, 0
  end
  
  def each
    return to_enum unless block_given?    

    current = nil
    
    accumulate do |start_date,end_date|
      events = Event::RegisteredEnumerator.new(@events, @registrations, start_date, end_date)
      
      events.each do |event|
        yield(current = event) if !filter || filter.call(event)
        break unless more?
      end
    end
    
    @last = current
  end
  
  def last
    @last || each {} && @last
  end
  
  private
    def accumulate
      start_date, end_date = @start_date, @end_date

      while @count < @max && @miss < 10
        count = @count

        yield(start_date, end_date)
        start_date, end_date = end_date + 1, end_date + @date_span

        @miss += 1 if count == @count
      end
    end
    
    def more?
      @count += 1
      @count < @max
    end
end