class Event::InclusiveOccurrenceEnumerator < Event::OccurrenceEnumerator
  protected
    def proxy(event, date)
      if event.occurring?(date) || excluded = event.excludes?(date)
        yield Event::Proxy.new(event, date, excluded)
      end
    end
end