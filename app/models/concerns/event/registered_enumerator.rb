class Event::RegisteredEnumerator
  include Enumerable
  
  attr_reader :events, :registrations, :start_date, :end_date
  
  def initialize(events, registrations, start_date, end_date)
    @events = events.between(start_date, end_date)
    @registrations = registrations ? registrations.set(start_date, end_date) : Set.new
  end
  
  def each
    return to_enum unless block_given?
    
    events.each do |event|
      event.registered = @registrations.include?(event)
      yield(event)
    end
  end
end