module Event::Recurrence
  extend ActiveSupport::Concern
  
  class LargeDateSpan < StandardError; end
  
  REPEAT_NONE = 1
  REPEAT_DAILY = 2
  REPEAT_WEEKLY = 4
  REPEAT_MONTHLY = 8
  
  SUNDAY = 1
  MONDAY = 2
  TUESDAY = 4
  WEDNESDAY = 8
  THURSDAY = 16
  FRIDAY = 32
  SATURDAY = 64
  
  module ClassMethods
    def between(start_date, end_date, options = {})
      raise LargeDateSpan, "#{end_date} is too far from #{start_date}" if end_date - start_date > 730
      
      scope = includes(:exclusions)
      scope = scope.where('exclude_on BETWEEN ? AND ? OR exclude_on IS NULL', start_date, end_date)
      scope = scope.references(:exclusions)
      scope = scope.where('((starts_at BETWEEN :start_date AND :end_date OR ends_at BETWEEN :start_date AND :end_date) AND repeat_type = 0) OR (starts_at <= :end_date AND (repeat_until >= :start_date OR repeat_until IS NULL))', start_date: start_date, end_date: end_date)
    
      unless options[:include_exclusions]
        Event::OccurrenceEnumerator.new(start_date, end_date, scope)
      else
        Event::InclusiveOccurrenceEnumerator.new(start_date, end_date, scope)
      end
    end
    
    def find_on(id, date = nil)
      event = find(id)
      date ||= id.to_s[/.+?\-(.*)/, 1]
      
      if date.nil?
        event
      elsif event.occurring?(date)
        event.assume(date, true)
      else
        error = "Couldn't find Event with #{primary_key}=#{id} and date=#{date}"
        raise ActiveRecord::RecordNotFound, error
      end
    end
  end
  
  def assume(date, date_verified_as_occurring = false)
    date = DateParser.new(date).to_date
    
    if date_verified_as_occurring || occurring?(date)
      Event::Proxy.new(self, date)
    else
      self
    end
  end
    
  def starts_on
    @starts_on ||= starts_at.to_date
  end
  
  def ends_on
    @ends_on ||= ends_at.to_date
  end
  
  def occurring?(date)
    date = date.to_date    
    return false if excludes?(date)
    
    case repeat_type
    when REPEAT_NONE
      starts_on == date || ends_on == date
    when REPEAT_DAILY
      true
    when REPEAT_WEEKLY, REPEAT_MONTHLY
      expression.include?(date)
    end
  end
  
  def excludes?(date)
    exclusions.any? { |exclusion| exclusion.exclude_on == date }
  end
  
  def next_occurrence(current_time = Time.now.utc)
    date = current_time.to_date
    limit = [ repeat_until, date + 60 ].compact.min
    
    while date < limit
      if occurring?(date)
        if repeat_type == REPEAT_NONE
          event = self
        else            
          event = Event::Proxy.new(self, date)
        end
        
        return event if event.starts_at > current_time
      end

      # Increment the date to the next day
      date += 1
    end
  end
  
  private
    def expression
      @expression ||= begin
        case repeat_type
        when REPEAT_WEEKLY
          7.times.each_with_object(Runt::Union.new) do |index,expression|
            expression.add(Runt::DIWeek.new(index)) if repeat_interval & (1 << index) > 0
          end
        when REPEAT_MONTHLY
          Runt::DIMonth.new(starts_on.week_of_month, starts_on.wday) |
            Runt::DIMonth.new(ends_on.week_of_month, ends_on.wday)
        end
      end
    end
end