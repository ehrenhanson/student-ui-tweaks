class Event::Proxy < DelegateClass(Event)
  def initialize(event, date, excluded = false)
    @date, @excluded = date, excluded
    super(event)
  end
  
  def event_instance
    __getobj__
  end
  
  def starts_at
    @starts_at ||= start_time
  end
  
  def ends_at
    @ends_at ||= end_time
  end
  
  def starts_on
    @starts_on ||= starts_at.to_date
  end
  
  def ends_on
    @ends_on ||= ends_at.to_date
  end
  
  def registrations
    Registration.where(event_id: id, starts_on: starts_on)
  end
  
  def parent?
    false
  end
  
  def excluded?
    !!@excluded
  end
  
  def to_param
    [ id, starts_on ].join('-')
  end
  
  private
    def start_time
      time = __getobj__.starts_at
      Time.utc(@date.year, @date.mon, @date.day, time.hour, time.min, time.sec)
    end
    
    def end_time
      duration = __getobj__.ends_at - __getobj__.starts_at
      start_time + duration
    end
end  
