module Order::Checkout
  extend ActiveSupport::Concern
  
  def total
    order_item.price
  end
  
  def save_and_generate_credit!(card)
    credit = nil
    
    transaction(requires_new: true) do
      save!
      credit = order_item.generate_credit!
      Payment.new(self, card).process!
    end

    credit
  end
end