module Order::CreditCardAccessors
  def credit_card
    @credit_card ||= CreditCard.new
  end

  def credit_card_attributes=(attributes)
    credit_card.attributes = attributes
  end
end