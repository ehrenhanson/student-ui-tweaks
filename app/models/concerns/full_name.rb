module FullName
  extend ActiveSupport::Concern
  
  def first_name
    @first_name ||= FullNameSplitter.split(name).first if name
  end
  
  def first_name=(first_name)
    @first_name = first_name
    self.name = [ @first_name, last_name ].join(' ')
    @first_name
  end
  
  def last_name
    @last_name ||= FullNameSplitter.split(name).second if name
  end
  
  def last_name=(last_name)
    @last_name = last_name
    self.name = [ first_name, @last_name ].join(' ')
    @last_name
  end
end