class Role::Type < DelegateClass(Fixnum)
  def self.from_string(string)
    new(Role::ROLES.index(string) || -1)
  end
  
  def self.new(value)
    value ? super(value) : nil
  end
  
  # Generate connivence methods to test role types. Methods take on
  # the name of the role with a question mark. Example method generated
  # for the admin role where 5 is the corresponding value:
  #
  # def admin?
  #   self == 5
  # end
  Role::ROLES.each.with_index do |role,index|
    define_method("#{role}?") do
      self == index
    end
  end
  
  def to_str
    Role::ROLES[self] || 'unknown'
  end
end
