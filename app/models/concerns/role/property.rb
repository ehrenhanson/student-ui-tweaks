module Role::Property
  extend ActiveSupport::Concern
  
  def role
    Role::Type.new(read_attribute(:role))
  end
end