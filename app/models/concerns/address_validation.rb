module AddressValidation
  extend ActiveSupport::Concern
  
  included do
    cattr_accessor(:address_associations) { Set.new }
    attr_accessor :address_required
    before_validation :set_address_required_on_address
  end
  
  module ClassMethods
    def validates_address(*associations)
      self.address_associations += associations
      
      associations.each do |association|
        method = "#{association}_required".to_sym
        
        class_eval(%{
          def #{method}
            @_#{method} || address_required
          end
          
          def #{method}=(value)
            @_#{method} = value
          end
        })
        
        validates(association, presence: true, if: method)
        validates_associated(*associations, if: method)        
      end
    end
  end
  
  private  
    def set_address_required_on_address
      address_associations.each do |name|
        address = association(name).reader
        address.address_required = send("#{name}_required") if address
      end
    end
end