module Registration::RegisteredSet
  extend ActiveSupport::Concern
  
  class Set < ::Set
    def <<(registration)
      super([ registration.event_id, registration.starts_on ])
    end
  
    def include?(event)
      super([ event.id, event.starts_on ])
    end
  end
  
  module ClassMethods
    def set(start_date, end_date)
      registrations = select(:event_id, :starts_on).between(start_date, end_date)
      registrations.inject(Registration::RegisteredSet::Set.new) do |set,registration|
        set << registration
      end
    end
  end
end
