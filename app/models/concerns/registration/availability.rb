class Registration::Availability
  attr_reader :registrations, :busy
  
  def self.find(scope, start_date, end_date)
    new(scope.includes(:event).between(start_date, end_date))
  end
  
  def initialize(registrations)
    @registrations = registrations
    @busy = Hash.new { |hash,key| hash[key] = Hash.new(&hash.default_proc) }
    
    fill_busy_schedule!(registrations)
  end
  
  def as_json(*args)
    @busy.as_json(*args)
  end
  
  protected
    def fill_busy_schedule!(registrations)
      registrations.each do |registration|
        range = registration.assumed_event.starts_on..registration.assumed_event.ends_on
        range.each do |date|
          @busy[date.year][date.month][date.day] = true
        end
      end
    end
end