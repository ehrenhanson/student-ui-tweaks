module Credit::Remaining
  extend ActiveSupport::Concern
  
  UNLIMITED = 1 / 0.0
  
  class Tally
    def initialize(attributes)
      @attributes = HashWithIndifferentAccess.new(0)
      @attributes.merge!(attributes)
    end
    
    def calculate
      if @attributes[:unlimited]
        UNLIMITED
      else
        @attributes[:available] - @attributes[:used]
      end
    end
  end
  
  module ClassMethods
    def find_remaining
      t1 = Credit.arel_table
      t2 = Registration.arel_table
      
      used_column = t2[:id].count
      used_column.distinct = true
      used_column.alias = 'used'

      joins(t1.join(t2, Arel::Nodes::OuterJoin).on(t1[:id].eq(t2[:credit_id])).join_sources)
        .group(t1[:id])
        .select(t1[Arel.star], used_column)
    end
    
    def unused
      t1 = Credit.arel_table
      t2 = Registration.arel_table
            
      having((t1[:value] - t2[:id].count).gt(0).or(t1[:unlimited].eq(true)))
    end
    
    # TODO: Determine if credits should be combined
    #
    # def count_remaining
    #   t1 = Credit.arel_table
    #   t2 = Registration.arel_table
    #   
    #   value_column = t1[:value].sum
    #   value_column.distinct = true
    #   value_column.alias = 'available'
    #   
    #   query = find_remaining.except(:select, :group, :having)
    #     .select(value_column, t2[:id].count.as('used'), t1[:unlimited].maximum.as('unlimited'))
    #   
    #   available, used, unlimited = connection.select_one(query).values_at('available', 'used', 'unlimited')
    #   
    #   if [ true, 't', 'true' ].include?(unlimited)
    #     UNLIMITED
    #   else
    #     (available || 0) - (used || 0)
    #   end
    # end
  end
  
  def available
    if unlimited?
      UNLIMITED
    else
      value
    end
  end
  
  def used
    if read_attribute(:used)
      read_attribute(:used)
    else
      Registration.where(credit_id: self).count
    end
  end
  
  def remaining
    available - used
  end
end