module Role
  extend ActiveSupport::Concern
  
  class InvalidRole < ArgumentError; end
  
  ROLES = %w(user student faculty owner master admin)
  
  included do
    validates :role, numericality: { less_than: ROLES.size, greater_than_or_equal_to: 0 }
  end
  
  # Create role type accessor methods to access the type by
  # name. Example: Role.student => Role::Type.new(1)
  ROLES.each.with_index do |role,index|
    class_eval %{
      def self.#{role}
        Role::Type.new(#{index})
      end        
    }
  end
  
  def self.unstringify(string)
    Role::Type.from_string(string)
  end
  
  def self.unstringify!(string)
    role = unstringify(string)
    raise(InvalidRole) if role < 0
    role
  end
end