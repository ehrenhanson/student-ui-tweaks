module Product::Credits
  extend ActiveSupport::Concern
  
  module ClassMethods
    def excluding(credits)
      ids = Array.wrap(credits).map(&:product_id)
      
      if ids.empty?
        all
      else
        where('id NOT IN (?)', ids)
      end
    end  
  end
end