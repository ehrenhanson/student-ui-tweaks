class Alert::Base
  include Rails.application.routes.url_helpers
  
  attr_reader :attributes, :school, :user_count
  
  def self.find(school, max_age)
    scope = self.scope(school)
    
    raise Alert::InvalidScope unless %W(user_id user_name alert_created_at).all? do |column_name|
      scope.select_values.join.include?(column_name)
    end
        
    scope.send(Babelq.translate.aliased_conditions, 'alert_created_at >= ?', max_age).order('alert_created_at DESC').map do |object|
      new(school, object.attributes)
    end
  end
  
  def initialize(school, attributes)
    @school = school
    @user_count = 1
    @attributes = attributes.with_indifferent_access
  end
  
  def bump!
    @user_count += 1
  end
  
  def user_link    
    name = "<a href='#{api_v1_school_user_path(school.to_param, user_id)}'>#{user_name}</a>"
    name += " and #{user_count - 1} other #{pluralize('person', 'people')}" if user_count > 1
    name
  end
  
  def same_as?(alert)
    return false unless alert
    
    ignored_attributes = %W(user_id user_name alert_created_at)    
    alert.attributes.except(*ignored_attributes) == attributes.except(*ignored_attributes)
  end
  
  def call
    timestamp = case alert_created_at
    when String
      Time.parse(alert_created_at)
    when Time
      alert_created_at
    else
      raise ArgumentError, "#{alert_created_at} is not a valid timestamp type."
    end
    
    [ timestamp, format ]
  end
  
  def method_missing(method, *args)
    if attributes.has_key?(method)
      attributes[method]
    else
      super
    end
  end
  
  private
    def pluralize(single, plural)
      if user_count > 2
        plural
      else
        single
      end
    end
end