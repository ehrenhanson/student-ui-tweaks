module User::RegistrationState
  extend ActiveSupport::Concern
  extend StateMachine::Concern
  
  included do
    attr_reader :transition
  end
  
  state_machine :state, initial: :created do
    before_transition any => any do |model,transition|
      model.instance_variable_set(:'@transition', transition.event)
    end

    after_transition any => any do |model,transition|
      model.instance_variable_set(:'@transition', nil)
    end
    
    before_transition :created => :invited, do: :set_invite_token
    
    state :created, :authorized
    
    state :invited do
      validates_presence_of :invite_token
    end

    state :registered do
      validates :email, presence: true
      validates :password, presence: true, if: lambda { |m| m.transition == :register }
      validates :terms_accepted, acceptance: { accept: true }
    end
    
    event :register do
      transition [ :created, :invited, :authorized ] => :registered
    end
    
    event :authorize do
      transition [ :created, :invited ] => :authorized
    end
    
    event :invite do      
      transition :created => :invited
    end
  end
end