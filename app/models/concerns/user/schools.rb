module User::Schools
  def with_role
    greatest = Babelq.translate.greatest('users.role', 'affiliations.role')
    joins(:users).select("DISTINCT schools.*, #{greatest} AS role")
  end
end
