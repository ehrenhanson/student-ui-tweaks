module User::Merge
  extend ActiveSupport::Concern
  
  included do
    cattr_accessor :merge_associations
  end
  
  module ClassMethods
    def define_merge_associations(*args)
      self.merge_associations = args
    end
  end
  
  def merge(object)
    raise ArgumentError, "Argument must be a #{self.class} instance (was #{object.class})" unless object.is_a?(self.class)
    return self if object == self
    
    transaction do
      merge_associations.each { |association| apply_association(object, association) }
      
      # Call destroy on a new instance so that the model doesn't try to
      # destroy the associations we reassigned
      object.class.destroy(object)
    end 

    reload    
  end
  
  private
    def apply_association(object, association)
      reflection = self.class.reflect_on_association(association)
      
      case reflection.macro
      when :has_many, :has_one
        if reflection.options.has_key?(:through)
          raise ArgumentError, "Merging association #{association} should be done through the #{reflection.options[:through]} association"
        end
        
        begin
          reflection.klass.where(reflection.foreign_key => object.id).update_all(reflection.foreign_key => id)
        rescue ActiveRecord::RecordNotUnique
        end
      else
        raise ArgumentError, "Merging association #{association} of type #{reflection.macro} is not implemented."
      end
    end
end