module User::Authentication
  extend ActiveSupport::Concern
  
  module ClassMethods
    def authenticate(email, password)
      user = User.where(arel_table[:email].lower.eq(email.to_s.downcase)).first
      user.authenticate(password) if user
    end

    def authenticate_with_omniauth(auth)
      where(auth_provider: auth.provider, auth_uid: auth.uid).first || create do |user|
        user.name = auth.info.name
        user.auth_provider = auth.provider
        user.auth_uid = auth.uid
        user.state = :authorized
      end
    end
  end

  private
    def using_auth_provider?
      auth_provider || auth_uid
    end
end
