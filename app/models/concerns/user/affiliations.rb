module User::Affiliations
  def min_role(role)
    role = role.__getobj__ if role.respond_to?(:__getobj__)
    where('affiliations.role >= ?', role)
  end
end