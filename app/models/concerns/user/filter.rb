module User::Filter
  extend ActiveSupport::Concern
  
  module ClassMethods
    def filter(roles)
      RoleFilter.new(self, roles).all
    end
    
    def with_school(id)
      return self unless id

      subquery = Affiliation
        .select(:role)
        .joins(:school)
        .where('affiliations.user_id = users.id AND (schools.id = :id OR schools.identifier = :id)', id: id)
        
      greatest = Babelq.translate.greatest("(#{subquery.to_sql})", 'users.role')
      select(%(users.*, IFNULL(#{greatest}, 0) AS role))
    end
  end
end