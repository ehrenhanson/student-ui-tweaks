module User::Tokens
  extend ActiveSupport::Concern
  
  included do
    before_create :reset_login_token
    before_update :reset_login_token, if: :password_digest_changed?  
  end
  
  def reset_login_token
    self.login_token = SecureRandom.hex
    self
  end
  
  def reset_login_token!
    reset_login_token.update_attribute(:login_token, login_token)
  end
  
  def activate_reset_password_token!
    self.reset_password_token = SecureRandom.hex
    update_attribute(:reset_password_token, reset_password_token)
  end
  
  def set_invite_token
    self.invite_token = SecureRandom.hex
  end
end