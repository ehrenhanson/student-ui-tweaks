module School::MerchantState
  extend ActiveSupport::Concern
  extend StateMachine::Concern
  
  included do
    scope :enabled, -> { where('merchant_state != ?', :disabled) }
  end
  
  state_machine :merchant_state, initial: :pending do
    state :pending
    state :approved
    state :declined
    state :disabled
    
    event :approve do
      transition :pending => :approved
    end
    
    event :decline do
      transition :pending => :declined
    end
  end
end