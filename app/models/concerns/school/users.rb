module School::Users
  def with_role
    greatest = Babelq.translate.greatest('users.role', 'affiliations.role')
    select("users.*, #{greatest} AS role")
  end
end