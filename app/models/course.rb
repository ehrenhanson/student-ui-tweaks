class Course < ActiveRecord::Base
  include Role::Property
  
  belongs_to :school
  has_many :events, dependent: :destroy
  has_many :instructors, through: :events
  has_many :placements, dependent: :destroy
  has_many :products, through: :placements
  has_many :appearances, dependent: :destroy
  
  validates :name, presence: true
  validates :level, numericality: { greater_than: 0 }
  
  delegate :name, :profile_image, :profile_image?, to: :user, prefix: true, allow_nil: true

  def next_event(time = Time.now.utc)
    @next_event ||= events.map { |event| event.next_occurrence(time) }.compact.min { |event| event.starts_at }
  end
  
  def class_description
    class_description_builder.full_description
  end
  
  def event_description
    class_description_builder.event_description
  end
  
  def product_description
    class_description_builder.product_description
  end
  
  private
    def class_description_builder
      @class_description_builder ||= ClassDescription.new(self)
    end
end
