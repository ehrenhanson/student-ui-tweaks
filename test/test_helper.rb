ENV["RAILS_ENV"] = "test"
require File.expand_path("../../config/environment", __FILE__)
require "rails/test_help"

# To add Capybara feature tests add `gem "minitest-rails-capybara"`
# to the test group in the Gemfile and uncomment the following:
# require "minitest/rails/capybara"

# Uncomment for awesome colorful output
# require "minitest/pride"

class ActiveSupport::TestCase
  def setup_school_context(school)
    @request.host = "#{school.identifier}.viewcy.com"
    @request.env['HTTP_REFERER'] = "http://#{school.identifier}.viewcy.com/"    
  end

  def mock_payment_gateway(success, errors = nil)
    response = mock()
    response.expects(:success?).returns(success)
    response.stubs(:errors).returns([ stub(errors) ]) if errors
    
    Payment.gateway = mock()
    Payment.gateway.stubs(:sale).returns(response)
  end
end
