# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :credit do
    value 1
    
    association :product
    association :user, :john, :sequenced
    
    trait :no_default_value do
      value nil
    end
  end
end
