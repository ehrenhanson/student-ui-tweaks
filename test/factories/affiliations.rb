# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :affiliation do
    role 1
    
    association :user, :john, :sequenced
    association :school
    
    trait :owner do
      association :user, :john, :address, :sequenced
      role Role.owner
    end

    trait :basic_user do
      association :user, factory: :duplicate_user
    end    
  end
end
