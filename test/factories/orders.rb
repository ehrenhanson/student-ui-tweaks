# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
    association :school
    association :user, :john, :sequenced, :address
    
    trait :without_address do
      association :user, :john, :sequenced
    end
    
    trait :with_items do
      before(:create) do |order|
        product = create(:product, school: order.school)
        create(:order_item, order: order, product: product)
      end
    end
  end
end
