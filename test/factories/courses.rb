# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :course do
    name "Hot Yoga 101"
    description "Learn the basics of this 3 part tutorial"
    level 1
    
    association :school, :affiliation, :with_owner
    
    trait :sequenced do
      sequence(:name) { |n| "Course #{n}" }
    end
    
    trait :with_event do
      after(:build) do |course|
        course.events << build(:event)
      end

      before(:create) do |course|
        course.events << create(:event)
      end
    end
  end
end
