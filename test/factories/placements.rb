# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :placement do
    after(:build) do |placement|
      school = build(:school)
      placement.course ||= build(:course, :with_event, school: school)
      placement.product ||= build(:product, school: school)
    end
  end
end
