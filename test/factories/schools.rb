# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :school do
    sequence(:name) { |n| "Hot Yoga By #{n}" }
    description "Experience hot yoga like you never have before."
    address { build(:address) }
    timezone Timezone::DEFAULT_TIME_ZONE
    
    trait :with_owner do
      after(:build) do |school|
        school.affiliations << build(:affiliation, role: Role.owner, school: school)
      end
    end

    trait :affiliation do
      after(:create) do |school|
        create(:affiliation, school: school)
      end
    end
    
    trait :courses do
      after(:create) do |school|
        create_list(:course, 2, :with_event, school: school)
      end
    end
    
    trait :courses_with_events do
      after(:create) do |school|
        create_list(:course, 2, :with_event, school: school)
      end
    end
    
    trait :header_image do
      header_image File.new(Rails.root.join('test/fixtures/landscape.jpg'))
    end
  end
  
  factory :school_for_placements, parent: :school do
    after(:create) do |school|
      create(:course, school: school)
      create(:product, school: school)
    end
  end
end
