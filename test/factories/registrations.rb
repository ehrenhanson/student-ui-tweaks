# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :registration do
    association :user, :john, :sequenced
    association :event, :daily
    
    after(:build) do |registration|
      registration.starts_on ||= registration.event.starts_on + 14
    end
  end
end
