# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :appearance do
    name "MyString"
    email "MyString"
    notes "MyText"
    payment_status 1
    
    association :course, :with_event
    association :affiliation
  end
end
