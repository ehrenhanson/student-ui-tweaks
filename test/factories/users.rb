# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    trait :john do
      name 'John Smith'
      email 'jsmith@example.com'
      password 'password123'
      password_confirmation 'password123'
      terms_accepted true
    end
    
    trait :jane do
      name 'Jane Doe'
      email 'jane@example.com'
      password 'password321'
      password_confirmation 'password321'
      terms_accepted true
    end
    
    trait :address do
      association :billing_address, factory: :billing_address
    end    
    
    trait :sequenced do
      sequence(:email) { |n| "jsmith#{n}@example.com" }
    end
    
    trait :courses do
      after(:build) do |user|
        user.events = build_list(:event, 5)
      end
    end
    
    trait :admin do
      role Role.admin
    end
    
    trait :with_credit_for_multiple_courses do
      before(:create) do |user|
        school = FactoryGirl.create(:school)
        product = FactoryGirl.create(:product)
        
        product.placements.destroy_all
        2.times do
          course = FactoryGirl.create(:course, school: school)
          FactoryGirl.create(:placement, product: product, course: course)
        end
        
        FactoryGirl.create(:credit, product: product, user: user)
      end
    end
  end
  
  factory :duplicate_user, class: User do
    name 'Duplicate User'
    
    after(:build) do |user|
      user.billing_address = build(:billing_address)
      user.events = build_list(:event, 5)
      user.affiliations = build_list(:affiliation, 4)
    end
  end
end
