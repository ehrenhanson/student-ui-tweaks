# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    name "Monday Class"
    description "The class that takes place on Monday"
    location "123 Wall St., Kingston, NY"
    starts_at "2013-09-16 20:00:00"
    ends_at "2013-09-16 21:00:00"

    association :instructor, factory: [ :user, :john, :sequenced ]

    trait :daily do
      repeat_type 2
    end
    
    trait :weekly do
      repeat_type 4
      repeat_interval 8
    end
        
    trait :biweekly do
      repeat_type 4
      repeat_interval 10
    end
    
    trait :monthly do
      repeat_type 8
    end
    
    trait :repeat_until do
      repeat_until "2013-09-30"
    end
    
    trait :exclusion do
      after(:create) { |event| FactoryGirl.create(:exclusion, event: event) }
    end
    
    trait :overnight do
      starts_at "2013-09-16 22:00:00"
      ends_at "2013-09-17 2:00:00"
    end
  end
end
