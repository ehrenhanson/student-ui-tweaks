# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :empty_address, class: Address do
  end
  
  factory :address do
    sequence(:address) { |n| "Address #{n}" }
    city 'Kingston'
    state 'NY'    
    country 'US'
    zipcode '12401'
  end
  
  factory :billing_address, parent: :address, class: BillingAddress do
  end
end
