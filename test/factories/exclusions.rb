# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :exclusion do    
    exclude_on "2013-09-18"
    
    association :event, :daily
  end
end
