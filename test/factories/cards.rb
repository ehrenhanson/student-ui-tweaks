FactoryGirl.define do
  factory :card, class: Hash do
    number '5105105105105100'
    cvv '123'
    exp_month '10'
    exp_year '2020'
    
    initialize_with { attributes.stringify_keys }
  end
end
