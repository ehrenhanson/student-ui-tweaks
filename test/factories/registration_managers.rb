FactoryGirl.define do
  factory :registration_manager do
    ignore do
      affiliation { create(:affiliation) }
      course { create(:course, school: affiliation.school)}
      event { create(:event, course: course) }
    end
    event_id { event.id }
    
    trait :purchase do
      ignore do
        product { create(:product, school: affiliation.school) }
      end
      
      product_id { product.id }
      card { attributes_for(:card) }
      address { attributes_for(:address) }
    end
    
    trait :credit do
      ignore do
        product { create(:product, school: affiliation.school) }
        credit { create(:credit, user: affiliation.user, product: product) }
      end

      credit_id { credit.id }
    end
    
    initialize_with { new(affiliation.school, affiliation.user, attributes) }
  end
end