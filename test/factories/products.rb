# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    name "Ten Pack"
    description "Buy ten credits for Yoga"
    credit_value 10
    price "9.99"
    association :school, :affiliation

    after(:create) do |product|      
      FactoryGirl.create(:placement, product: product, course: build(:course, school: product.school))
      FactoryGirl.create(:placement, product: product, course: build(:course, school: product.school))
    end
    
    trait :course do
      after(:build) do |product|
        course = build(:course, :sequenced, school: product.school)
        build(:placement, product: product, course: course)
      end
    end
  end
end
