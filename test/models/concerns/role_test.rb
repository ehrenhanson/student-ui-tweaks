require "test_helper"

class MockModel
  include ActiveModel::Validations
  include Role
  
  attr_accessor :role
end

describe Role do
  before do
    @model = MockModel.new
  end
  
  describe "role generators" do
    it "must create a user role" do
      Role.user.user?.must_equal true
    end
    
    it "must create a student role" do
      Role.student.student?.must_equal true
    end
    
    it "must create a faculty role" do
      Role.faculty.faculty?.must_equal true
    end
    
    it "must create an owner role" do
      Role.owner.owner?.must_equal true
    end
    
    it "must create a master role" do
      Role.master.master?.must_equal true
    end
    
    it "must create an admin role" do
      Role.admin.admin?.must_equal true
    end
  end
  
  describe "validation of roles" do
    it "must change roles" do
      @model.role = 2
      @model.valid?.must_equal true
    end
    
    it "must not be below min role index" do
      @model.role = -1
      @model.valid?.must_equal false
    end
    
    it "must not exceed max role index" do
      @model.role = 100
      @model.valid?.must_equal false
    end
  end
  
  describe "unstringify" do
    it "must convert user to value" do
      Role.unstringify('user').must_equal 0
    end
    
    it "must convert student to value" do
      Role.unstringify('student').must_equal 1
    end
    
    it "must convert faculty to value" do
      Role.unstringify('faculty').must_equal 2
    end
    
    it "must convert owenr to value" do
      Role.unstringify('owner').must_equal 3
    end
    
    it "must convert master to value" do
      Role.unstringify('master').must_equal 4
    end
    
    it "must convert admin to value" do
      Role.unstringify('admin').must_equal 5
    end
    
    it "must convert unknown value to nil" do
      Role.unstringify('unknown').must_equal -1
    end
  end
  
  describe "unstringify!" do
    it "must convert student to value" do
      Role.unstringify!('student').must_equal 1
    end
    
    it "must raise InvalidRole error when not a valid role" do
      proc { Role.unstringify!('unknown') }.must_raise Role::InvalidRole
    end
  end
end