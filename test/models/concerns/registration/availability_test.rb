require 'test_helper'

describe Registration::Availability do
  before do
    @registration = FactoryGirl.create(:registration)
  end
  
  it "must find registrations" do
    availability = Registration::Availability.find(Registration, @registration.starts_on, @registration.starts_on)
    availability.registrations.must_include @registration
  end
  
  it "must create busy schedule" do
    availability = Registration::Availability.new([ @registration ])
    availability.busy.must_equal({ 2013 => { 9 => { 30 => true } } })
  end
  
  it "must return busy JSON" do
    availability = Registration::Availability.new([ @registration ])
    availability.to_json.must_equal "{\"2013\":{\"9\":{\"30\":true}}}"
  end
end
