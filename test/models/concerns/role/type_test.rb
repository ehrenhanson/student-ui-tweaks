require "test_helper"

describe Role::Type do
  let(:student) { Role.student }
  let(:admin) { Role.admin }

  it "must convert a student role string to a type instance" do
    Role::Type.from_string('student').must_equal student
  end
  
  it "must convert an admin role string to a type instance" do
    Role::Type.from_string('admin').must_equal admin
  end
  
  it "must provide test methods for student role types" do
    student.student?.must_equal true
  end
  
  it "must provide test methods for admin role types" do
    admin.admin?.must_equal true
  end

  it "must not claim to be a student when an admin" do
    admin.student?.must_equal false
  end
  
  it "must not claim to be an admin when a student" do
    student.admin?.must_equal false
  end

  it "must convert student role to to string" do
    student.to_str.must_equal 'student'
  end
  
  it "must convert admin role to to string" do
    admin.to_str.must_equal 'admin'
  end
end
