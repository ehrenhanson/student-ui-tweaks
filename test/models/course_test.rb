require "test_helper"

describe Course do
  before do
    @course = FactoryGirl.build(:course)
  end

  it "must be valid" do
    @course.valid?.must_equal true
  end
  
  it "must fetch the next event" do
    @course.events += [ FactoryGirl.build(:event, starts_at: Time.utc(2013, 10, 1)), FactoryGirl.build(:event, :daily, starts_at: Time.utc(2013, 9, 27)) ]
    @course.next_event(Time.utc(2013, 10, 27)).repeat_type.must_equal Event::REPEAT_DAILY
  end
  
  it "must fetch the next non-repeating event" do
    @course.events += [ FactoryGirl.build(:event, starts_at: Time.utc(2013, 10, 27)) ]
    @course.next_event(Time.utc(2013, 10, 1)).repeat_type.must_equal Event::REPEAT_NONE
  end
  
  describe "validation" do
    describe "of name" do
      it "must require a name" do
        @course.name = nil
        @course.valid?.must_equal false
      end
    end
    
    describe "of level" do
      it "must require a level" do
        @course.level = nil
        @course.valid?.must_equal false
      end
      
      it "must require a level greater than 0" do
        @course.level = 0
        @course.valid?.must_equal false
      end
    end
  end
  
  describe "association" do
    it "must destroy events" do
      event = FactoryGirl.create(:event, course: @course)
      @course.destroy!
      proc { event.reload }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must destroy placements" do
      placement = FactoryGirl.create(:placement, course: @course)      
      @course.destroy!
      proc { placement.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
end
