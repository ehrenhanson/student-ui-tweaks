require "test_helper"

describe Product do
  before do
    @product = FactoryGirl.build(:product)
  end

  it "must be valid" do
    @product.valid?.must_equal true
  end
  
  it "must return cheapest product" do
    FactoryGirl.create(:product, price: 20)
    cheapest = FactoryGirl.create(:product, price: 2)
    Product.cheapest.first.must_equal cheapest
  end
  
  describe "association" do
    it "must destroy credits" do
      credit = FactoryGirl.create(:credit, product: @product)
      @product.destroy!
      proc { credit.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
  
  describe "course_ids" do
    before do
      @placements = FactoryGirl.create_list(:placement, 4, product: @product)
    end
    
    it "must return course_ids" do
      @product.course_ids.must_equal @placements.map(&:id)
    end

    it "must return assigned course_ids" do
      @product.course_ids = [ 1, 4 ]
      @product.course_ids.must_equal [ 1, 4 ]
    end
    
    it "must save assigned course_ids" do
      @product.course_ids = [ 1, 4 ]
      @product.save!
      @product.placements.map(&:course_id).must_equal [ 1, 4 ]
    end
  end
  
  describe Product::Credits do
    before do
      @products = FactoryGirl.create_list(:product, 5)
      @credit = FactoryGirl.create(:credit, product: @products.first)
    end
    
    it "must include all products" do
      Product.count.must_equal @products.size
    end
    
    it "must include all products when given an empty exclusion set" do
      Product.excluding([]).count.must_equal @products.size
    end
    
    it "must exclude associated products when given a credit" do
      Product.excluding(@credit).count.must_equal @products.size - 1
    end
    
    it "must exclude associated products when given a list of credits" do
      @credit_2 = FactoryGirl.create(:credit, product: @products[1])
      Product.excluding([ @credit, @credit_2 ]).count.must_equal @products.size - 2
    end
    
    it "must not include the related product" do
      Product.excluding(@credit).wont_include @products.first
    end
  end
end
