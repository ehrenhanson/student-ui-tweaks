require "test_helper"

describe RoleFilter do
  it "must initialize with user context" do
    filter = RoleFilter.new(User, 'student')
    filter.must_be_kind_of(RoleFilter)
  end
  
  it "must return filter query" do
    filter = RoleFilter.new(User, 'student')
    filter.all.must_be_kind_of ActiveRecord::Relation
  end
  
  it "must set equality" do
    filter = RoleFilter.new(User, 'student')
    filter.all.where_values.map(&:to_sql).join.must_equal "#{Babelq.translate.greatest('users.role', 'affiliations.role')} = 1"
  end
  
  it "must set greater than or equal" do
    filter = RoleFilter.new(User, '>student')
    filter.all.where_values.map(&:to_sql).join.must_equal "#{Babelq.translate.greatest('users.role', 'affiliations.role')} >= 1"
  end
  
  it "must set less than or equal" do
    filter = RoleFilter.new(User, '<student')
    filter.all.where_values.map(&:to_sql).join.must_equal "#{Babelq.translate.greatest('users.role', 'affiliations.role')} <= 1"
  end
  
  it "must add conditions for multiple roles" do
    filter = RoleFilter.new(User, 'student,faculty')
    filter.all.where_values.map(&:to_sql).join.must_equal "(#{Babelq.translate.greatest('users.role', 'affiliations.role')} = 1 OR #{Babelq.translate.greatest('users.role', 'affiliations.role')} = 2)"
  end
end
