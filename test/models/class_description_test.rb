require "test_helper"

describe ClassDescription do
  before do
    @course = FactoryGirl.create(:course)
    @description = ClassDescription.new(@course, Date.new(2013, 9, 1))
  end
  
  describe "#event_part" do
    it "must return event part with no events" do
      @description.event_part.must_equal [ 0, 'No classes are scheduled at this time' ]
    end
  
    it "must return event part with daily events" do
      FactoryGirl.create(:event, :daily, course: @course)
      @description.event_part.must_equal [ 1, 'Daily classes available' ]
    end
  
    it "must return event part with weekly events" do
      FactoryGirl.create(:event, :weekly, course: @course)
      @description.event_part.must_equal [ 2, 'Weekly: Wednesday' ]
    end
  
    it "must return event part with biweekly events" do
      FactoryGirl.create(:event, :biweekly, course: @course)
      @description.event_part.must_equal [ 2, 'Weekly: Monday and Wednesday' ]
    end
    
    it "must return event part with next event" do
      FactoryGirl.create(:event, course: @course)
      @description.event_part.must_equal [ 8, 'Next class on September 16th, 2013' ]
    end
  end
  
  describe "#product_part" do
    it "must return product part with no products" do
      @description.product_part.must_equal [ 0, 'No credits are available for purchase at this time' ]
    end

    it "must return product part with multi-credit product" do
      FactoryGirl.create(:placement, course: @course)
      @description.product_part.must_equal [ 2, '10 classes for $9.99' ]
    end
    
    it "must return product part with single-credit product" do
      placement = FactoryGirl.create(:placement, course: @course)
      placement.product.update_attributes!(credit_value: 1)
      @description.product_part.must_equal [ 4, '$9.99/class' ]
    end
    
    it "must return product part with unlimited product" do
      placement = FactoryGirl.create(:placement, course: @course)
      placement.product.update_attributes!(credit_value: 1, unlimited: true)
      @description.product_part.must_equal [ 1, 'Full pass for $9.99' ]
    end
    
    it "must return product part with the lowest-cost product" do
      placement = FactoryGirl.create(:placement, course: @course)
      placement = FactoryGirl.create(:placement, course: @course)
      placement.product.update_attributes!(price: 1)
      @description.product_part.must_equal [ 2, '10 classes for $1.00' ]
    end
  end
  
  describe "#full_description" do
    it "must return course description" do
      @description.full_description.must_equal @course.description
    end
    
    it "must return course description" do
      FactoryGirl.create(:event, :daily, course: @course)
      FactoryGirl.create(:placement, course: @course)
      @description.full_description.must_equal 'Daily classes available, 10 classes for $9.99'
    end
  end
end