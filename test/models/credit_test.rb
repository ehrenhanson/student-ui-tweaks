require "test_helper"

describe Credit do
  before do
    @credit = FactoryGirl.build(:credit, :no_default_value)
  end

  it "must be valid" do
    @credit.valid?.must_equal true
  end
  
  it "must create a credit assigning value from product" do
    @credit.save!
    @credit.value.must_equal 10
  end
  
  it "must retain value even if product value changes" do
    @credit.save!
    @credit.product.update_attributes!(credit_value: 2)    
    @credit.reload.value.must_equal 10
  end
  
  it "must create an affiliation" do
    @credit.save!
    @credit.user.affiliations.map(&:school).must_include @credit.product.school
  end
    
  describe 'of_school' do
    before do
      @credits = FactoryGirl.create_list(:credit, 5)
    end
    
    it "must return credits for the given school" do
      school = @credits.first.courses.first.school
      Credit.of_school(school).count.must_equal 1
    end
    
    it "wont return credits for school without credits" do
      school = FactoryGirl.create(:school)
      Credit.of_school(school).count.must_equal 0
    end
  end
  
  describe 'of_course' do
    before do
      @credits = FactoryGirl.create_list(:credit, 5)
    end
    
    it "must return credits for the given school" do
      course = @credits.first.courses.first
      Credit.of_course(course).count.must_equal 1
    end
    
    it "wont return credits for school without credits" do
      course = FactoryGirl.create(:course)
      Credit.of_course(course).count.must_equal 0
    end
  end
  
  describe Credit::Remaining do
    before do
      @credit = FactoryGirl.create(:credit, value: 10)
    end
    
    it "must have remaining equal to value" do
      @credit.remaining.must_equal 10
    end
    
    it "must have remaining equal to value minus used credits" do
      FactoryGirl.create_list(:registration, 4, credit: @credit)
      @credit.remaining.must_equal 6
    end
    
    it "must have remaining equal to value minus used credits when preloaded" do
      FactoryGirl.create_list(:registration, 4, credit: @credit)
      @credit = Credit.find_remaining.first
      @credit.remaining.must_equal 6
    end
    
    it "must have unlimited remaining credits when unlimited flag is set" do
      FactoryGirl.create_list(:registration, 4, credit: @credit)
      @credit.update_attributes!(unlimited: true)
      @credit.remaining.must_equal Credit::UNLIMITED
    end
  end
end
