require "test_helper"

describe Event::RegisteredEnumerator do
  let(:events) { Event::RegisteredEnumerator.new(Event.all, Registration.all, Date.new(2014, 1, 1), Date.new(2014, 1, 14)) }
  let(:unregistered_events) { Event::RegisteredEnumerator.new(Event.all, nil, Date.new(2014, 1, 1), Date.new(2014, 1, 14)) }
  let(:template) { Array.new(14, false).tap { |registrations| registrations[4] = true } }
  let(:unregistered_template) { Array.new(14, false) }
  
  before do
    event = FactoryGirl.create(:event, :daily)    
    FactoryGirl.create(:registration, event: event, starts_on: Date.new(2014, 1, 5))
  end
  
  it "must assign registered variable" do
    events.map(&:registered).must_equal template
  end
  
  it "must default to unregistered" do
    unregistered_events.map(&:registered).must_equal unregistered_template
  end
end