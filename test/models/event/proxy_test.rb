require "test_helper"

describe Event::Proxy do
  before do
    @event = FactoryGirl.create(:event, :daily, :exclusion)
    @proxy = Event::Proxy.new(@event, Date.new(2013, 10, 1))
  end
  
  after do
    @event.destroy
  end
  
  it "must return event instance" do
    @proxy.event_instance.must_be_kind_of Event
  end
  
  it "must use ID and date for to_param" do
    @proxy.to_param.must_equal "#{@event.id}-2013-10-01"
  end
  
  it "must not be a parent" do
    @proxy.parent?.must_equal false
  end
  
  it "must return name" do
    @proxy.name.must_equal @event.name
  end
  
  it "must return description" do
    @proxy.description.must_equal @event.description
  end
  
  it "must return location" do
    @proxy.location.must_equal @event.location
  end
  
  it "must return repeat_type" do
    @proxy.repeat_type.must_equal @event.repeat_type
  end
  
  it "must return repeat_interval" do
    @proxy.repeat_interval.must_equal @event.repeat_interval
  end
  
  it "must return repeat_until" do
    @proxy.repeat_until.must_equal @event.repeat_until
  end
  
  it "must return created_at" do
    @proxy.created_at.must_equal @event.created_at
  end
  
  it "must return updated_at" do
    @proxy.updated_at.must_equal @event.updated_at
  end
  
  it "must return exclusions" do
    @proxy.exclusions.size.must_equal @event.exclusions.size
  end
  
  it "must return starts_at with the new date" do
    @proxy.starts_at.must_equal Time.utc(2013, 10, 1, @event.starts_at.hour, @event.starts_at.min)
  end
  
  it "must return ends_at with the new date" do
    @proxy.ends_at.must_equal Time.utc(2013, 10, 1, @event.ends_at.hour, @event.ends_at.min)
  end
  
  it "must span midnight" do
    date = Date.new(2013, 10, 1)
    @event = FactoryGirl.build(:event, :overnight)
    @proxy = Event::Proxy.new(@event, date)
    
    @proxy.starts_at.must_equal Time.utc(2013, 10, 1, 22)
    @proxy.ends_at.must_equal Time.utc(2013, 10, 2, 2)
    @proxy.starts_on.must_equal date
    @proxy.ends_on.must_equal date + 1
  end
  
  it "must have registrations assocaition" do
    @event.registrations.must_be_kind_of ActiveRecord::Associations::CollectionProxy
  end
end
