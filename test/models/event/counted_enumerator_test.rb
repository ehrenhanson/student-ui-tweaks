require "test_helper"

describe Event::CountedEnumerator do
  let(:enumerator) { Event::CountedEnumerator.new(Event.all, nil, Date.new(2014, 1, 1), Date.new(2014, 1, 14)) }
  
  before do
    FactoryGirl.create(:event, :weekly)
  end
  
  it "must return 100 events" do
    enumerator.inject(0) { |acc,_| acc += 1 }.must_equal 100
  end
  
  it "must count 100 events" do
    enumerator.count.must_equal 100
  end
  
  it "must return last item" do
    enumerator.last.must_be_kind_of Event
  end
end
