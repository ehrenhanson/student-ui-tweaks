require "test_helper"

describe Event::OccurrenceEnumerator do
  let(:start_date) { Date.new(2013, 10, 1) }
  let(:end_date) { Date.new(2013, 10, 31) }
  
  before do
    @event = FactoryGirl.create(:event, :daily, :exclusion)
    @enumerator = Event::OccurrenceEnumerator.new(start_date, end_date, Event.all)
  end
  
  after do
    @event.destroy
  end
  
  it "must iterate over events" do
    @enumerator.each do |event|
      # Event::Proxy DelegateClass defines __getobj__ to fetch the original object.
      # If the method is not defined, it indicates we are not getting the class
      # that should be expected.
      event.respond_to?(:__getobj__).must_equal true
      event.__getobj__.must_be_kind_of Event
    end
  end
  
  it "must have events" do
    count = 0    
    @enumerator.each { count += 1 }
    count.must_be :>, 0
  end
  
  it "must have a start_date" do
    @enumerator.start_date.must_equal start_date
  end
  
  it "must be enumerable" do
    @enumerator.each.must_be_kind_of Enumerator
  end
end