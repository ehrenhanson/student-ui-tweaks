require "test_helper"

describe SchoolRegistration do
  before do
    @user = FactoryGirl.build(:user, :john, :sequenced)
    @school_registration = SchoolRegistration.new(@user)
  end
  
  after do
    User.destroy_all
    School.destroy_all
  end
  
  let(:school_attributes) { FactoryGirl.attributes_for(:school, name: 'test').merge!(address_attributes: FactoryGirl.attributes_for(:address)) }
  let(:user_attributes) {{ billing_address_attributes: FactoryGirl.attributes_for(:address) }}
  let(:school_registration_attributes) {{
    school_attributes: school_attributes,
    user_attributes: user_attributes,
    ssn: '123-321-201-100',
    tax_id: '10220121-210',
    account_number: '123223',
    routing_number: '1202'
  }}
  
  it "must serve affiliation model" do
    @school_registration.affiliation.must_be_kind_of Affiliation
  end
  
  it "must serve user model" do
    @school_registration.user.must_equal @user
  end
  
  it "must serve school model" do
    @school_registration.school.must_be_kind_of School
  end
  
  it "must set owner role" do
    @school_registration.affiliation.role.must_equal Role.owner
  end
  
  it "must require billing address" do
    @school_registration.user.billing_address_required.must_equal true
  end
  
  it "wont be persisted" do
    @school_registration.persisted?.wont_equal true
  end
  
  it "must assign attributes" do
    @school_registration.attributes = { ssn: '123-342-124-111' }
    @school_registration.ssn.must_equal '123-342-124-111'
  end
  
  it "must forward user attributes" do
    @school_registration.attributes = { user_attributes: { name: 'John' } }
    @school_registration.user.name.must_equal 'John'
  end
  
  it "must raise ActiveRecord::RecordInvalid if invalid" do
    proc { @school_registration.save! }.must_raise ActiveRecord::RecordInvalid
  end
  
  it "wont save if invalid" do
    @school_registration.save.wont_equal true
  end
  
  describe SchoolRegistration::Merchant do
    def MockProcessor(successful)
      stub(create: stub(success?: successful, message: 'An error has occurred'))
    end
    
    it "must save if valid" do
      SchoolRegistration.merchant_processor = MockProcessor(true)
      @school_registration.attributes = school_registration_attributes
      @school_registration.save.must_equal true
    end
    
    it "wont save if Braintree account is invalid" do
      SchoolRegistration.merchant_processor = MockProcessor(false)
      @school_registration.attributes = school_registration_attributes
      @school_registration.save.wont_equal true
    end
  end
end