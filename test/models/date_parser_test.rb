require "test_helper"

describe DateParser do
  describe Date do
    it "must convert Date to Date" do
      DateParser.new(Date.today).to_date.must_equal Date.today
    end
  
    it "must convert Time to Date" do
      DateParser.new(Time.now).to_date.must_equal Date.today
    end
  
    it "must convert date string to Date" do
      DateParser.new('2013-10-01').to_date.must_equal Date.new(2013, 10, 1)
    end
  
    it "must convert time string to Date" do
      DateParser.new('2013-10-05 01:10:22').to_date.must_equal Date.new(2013, 10, 5)
    end
  
    it "must convert date array to Date" do
      DateParser.new(%w(2013 10 01)).to_date.must_equal Date.new(2013, 10, 1)
    end
  
    it "must raise invalid date on invalid type input" do
      proc { DateParser.new(1).to_date }.must_raise(DateParser::InvalidDate)
    end
  
    it "must raise invalid date on invalid string input" do
      proc { DateParser.new('Today').to_date }.must_raise(DateParser::InvalidDate)
    end
  
    it "must convert Date to Date" do
      DateParser.new(Date.today).to_date.must_equal Date.today
    end
  end
  
  describe Time do
    it "must convert Time to Time" do
      time = Time.now
      DateParser.new(time).to_time.must_equal time
    end
  
    it "must convert date string to Time" do
      DateParser.new('2013-10-01').to_time.must_equal Time.mktime(2013, 10, 1)
    end
  
    it "must convert time string to Time" do
      DateParser.new('2013-10-05 01:10:22').to_time.must_equal Time.mktime(2013, 10, 5, 1, 10, 22)
    end
  
    it "must convert date array to Time" do
      DateParser.new(%w(2013 10 01)).to_time.must_equal Time.mktime(2013, 10, 1)
    end
  
    it "must raise invalid date on invalid type input" do
      proc { DateParser.new(1).to_time }.must_raise(DateParser::InvalidDate)
    end
  
    it "must raise invalid date on invalid string input" do
      proc { DateParser.new('Today').to_time }.must_raise(DateParser::InvalidDate)
    end
  end
end
