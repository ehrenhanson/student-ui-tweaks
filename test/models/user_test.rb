require "test_helper"

describe User do
  let(:john) { FactoryGirl.build(:user, :john) }
  let(:jane) { FactoryGirl.build(:user, :jane) }
  
  before do
    User.destroy_all
    @user = john
  end
  
  it "must include role module" do
    User.included_modules.include?(Role).must_equal true
  end
  
  it "must combine the first and last name parts into a full name" do
    @user.name.must_equal 'John Smith'
  end
  
  it "must accept a profile image" do
    image = File.new(Rails.root.join('test/fixtures/rails.png'))
    @user.update_attributes!(profile_image: image)
    @user.profile_image.url(:thumb).must_match /system/
  end

  it "must assign a reset password token" do
    @user = FactoryGirl.create(:user, :john)
    @user.activate_reset_password_token!
    @user.reset_password_token.wont_be :nil?
  end
  
  it "must return profile image url" do
    @user.profile_image_url.must_be_kind_of String
  end
  
  describe "with_school" do
    before do
      @affiliation = FactoryGirl.create(:affiliation)
      @school = @affiliation.school
      @newuser = @affiliation.user
    end
    
    it "must find the user" do
      User.with_school(@school).find(@newuser).must_equal @newuser
    end
    
    it "must find the user even if not affiliated with the school" do
      @user.save!
      User.with_school(@school).find(@user).must_equal @user
    end
    
    it "must inerhit the affilation role" do
      @affiliation.update_attributes!(role: Role.owner)
      User.with_school(@school).find(@newuser).role.must_equal Role.owner
    end
    
    it "must keep the user role" do
      @newuser.update_attributes!(role: Role.admin)
      @affiliation.update_attributes!(role: Role.owner)
      User.with_school(@school).find(@newuser).role.must_equal Role.admin
    end
  end

  describe "validations" do
    it "must be valid" do
      @user.valid?.must_equal true
    end
  
    describe "of name name" do
      it "must require a name" do
        @user.name = nil
        @user.valid?.must_equal false
      end
    end
    
    describe "of email" do
      it "must be a unique address" do
        jane.save!
        @user.email = jane.email
        @user.valid?.must_equal false
      end
    end
  
    describe "of email in created state" do
      it "does not require an email" do
        @user.email = nil
        @user.valid?.must_equal true
      end      
    end
  
    describe "of email in registered state" do
      before do
        @user.state = 'registered'
      end
      
      it "must require an email" do
        @user.email = nil
        @user.valid?.must_equal false
      end
  
      it "must require a valid email" do
        @user.email = 'invalid>address@'
        @user.valid?.must_equal false
      end
    end
      
    describe "of email in authorized state" do
      before do
        @user.state = 'authorized'
      end
      
      it "must not require an email when using auth provider" do
        @user.email = nil
        @user.auth_provider = 'twitter'
        @user.auth_uid = '12345'
        @user.valid?.must_equal true
      end
    end
    
    describe "of password in created state" do
      it "must not require a password upon create" do
        @user.instance_variable_set(:@password, nil)
        @user.valid?.must_equal true
      end
      
      it "must not require a password upon update" do
        @user.save!
        @user.instance_variable_set(:@password, nil)
        @user.valid?.must_equal true
      end
    end
    
    describe "of password in registered state" do
      it "must require a password" do
        @user.instance_variable_set(:'@password', nil)
        @user.register.wont_equal true
      end
      
      it "wont require a password after it has been assigned" do
        @user.instance_variable_set(:'@password', 'Foobar')
        @user.register.must_equal true
        @user.instance_variable_set(:'@password', nil)
        @user.save.must_equal true
      end
    end
    
    describe "of password in authroized state" do
      before do
        @user.state = 'authorized'
      end
      
      it "must not require a password when using auth provider" do
        @user.password = nil
        @user.auth_provider = 'twitter'
        @user.auth_uid = '12345'
        @user.valid?.must_equal true
      end
    end
  end
  
  describe "association" do
    it "must destroy affiliations" do
      affiliation = FactoryGirl.create(:affiliation, user: @user)
      @user.destroy!
      proc { affiliation.reload }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must destroy events" do
      event = FactoryGirl.create(:event, instructor: @user)
      @user.destroy!
      proc { event.reload }.must_raise ActiveRecord::RecordNotFound
    end
  
    it "must destroy credits" do
      credit = FactoryGirl.create(:credit, user: @user)
      @user.destroy!
      proc { credit.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
  
  describe "authentication" do
    before do
      john.save!
      jane.save!
    end
    
    it "must successfully authenticate" do
      User.authenticate(@user.email, 'password123').must_equal @user
    end
    
    it "must successfully authenticate mixed-case email" do
      User.authenticate('JSmith@Example.Com', 'password123').must_equal @user
    end
    
    it "must fail to authenticate" do
      User.authenticate(@user.email, 'password321').wont_equal true
      User.authenticate(jane.email, 'password123').wont_equal true
      User.authenticate(jane.email, nil).wont_equal true
      User.authenticate(nil, 'password123').wont_equal true
      User.authenticate(nil, nil).wont_equal true
    end
  end
  
  describe "provider authentication" do
    let(:auth) { OmniAuth.mock_auth_for(:twitter) }
    let(:bob) { User.authenticate_with_omniauth(auth) }
    
    it "must return a user" do
      bob.must_be_instance_of User
    end
    
    it "must create a new user account" do
      bob.persisted?.must_equal true
    end
    
    it "must reuse an existing user account" do
      joe = User.authenticate_with_omniauth(auth)
      joe.must_equal bob
    end
    
    it "must assign the user details from the auth info" do
      bob.name.must_equal 'Example User'
      bob.auth_provider.must_equal auth.provider
      bob.auth_uid.must_equal auth.uid
    end
  end
  
  describe "schools with role" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, user: @user)
    end
    
    it "must provide a role method" do
      @user.schools.with_role.first.respond_to?(:role).must_equal true
    end
    
    it "must use the role defined by the affiliation if greater than the user" do
      @user.role.must_be :<, @affiliation.role
      @user.schools.with_role.first.role.must_equal @affiliation.role
    end
    
    it "must use the role defined by the user if greater than the affiliation" do
      @user.update_attributes(role: 5)
      @user.role.must_be :>, @affiliation.role
      @user.schools.with_role.first.role.must_equal @user.role
    end
    
    it "must not return duplicates" do
      @affiliation.update_attributes!(role: Role.owner)
      affiliation_2 = FactoryGirl.create(:affiliation, user: @user, role: Role.owner)
      FactoryGirl.create(:affiliation, school: @affiliation.school, role: Role.student)
            
      @user.schools.with_role.map(&:id).must_equal @user.schools.map(&:id)
    end
  end
  
  describe "filter" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, user: @user)
      @school = @affiliation.school
    end

    it "must select matching roles" do
      User.filter('student').wont_be :empty?
    end
    
    it "must not select non-matching roles" do
      User.filter('admin').must_be :empty?
    end
    
    it "must select matching roles when promoted by user role" do
      @user.update_attributes!(role: Role.admin)
      User.filter('admin').wont_be :empty?
    end
    
    it "must use affiliations from all schools" do
      FactoryGirl.create(:affiliation, role: Role.owner, user: @user)
      User.filter('owner').size.must_equal 1
    end
    
    it "must select matching role or greater as a match" do
      User.filter('>student').must_include @user
    end
    
    it "must select matching role or greater as greater" do
      @user.update_attributes!(role: Role.admin)
      User.filter('>student').must_include @user
    end
    
    it "must not select greater if not a match" do
      User.filter('>faculty').wont_include @user
    end
    
    it "must select matching role or less as match" do
      @user.update_attributes!(role: Role.admin)
      User.filter('<admin').must_include @user
    end
    
    it "must select matching role or less as less" do
      User.filter('<admin').must_include @user
    end
    
    it "must not select less if not a match" do
      @user.update_attributes!(role: Role.admin)
      User.filter('<owner').wont_include @user
    end
    
    it "must select matching role with others" do
      User.filter('student,admin').must_include @user
    end
    
    it "wont select matching role with others if not of role" do
      User.filter('owner,admin').wont_include @user
    end
  end
  
  describe "min_role" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, user: @user)
      @course = FactoryGirl.create(:course, school: @affiliation.school)
    end
    
    it "must select all affiliated courses" do
      @user.affiliated_courses.min_role(Role.user).size.must_be :>, 0
    end
    
    it "must select no affiliated courses" do
      @user.affiliated_courses.min_role(Role.faculty).size.must_be :zero?
    end
    
    it "must select all affiliated courses with role greater than or equal to faculty" do
      @affiliation.update_attributes!(role: Role.faculty)
      @user.affiliated_courses.min_role(Role.faculty).size.must_be :>, 0
    end
  end
  
  describe "login_token" do
    before do
      @user = FactoryGirl.create(:user, :john)
      @token = @user.login_token
    end
  
    it "must create login token" do
      @token.wont_be :empty?
    end
  
    it "wont change login token on save" do
      @user.update_attribute(:name, 'Robert')
      @user.reload.login_token.must_equal @token
    end
    
    it "must change login token when password changes" do
      @user.update_attribute(:password, SecureRandom.hex)      
      @user.reload.login_token.wont_equal @token
    end
    
    it "must create a login token when password changes" do
      @user.update_attribute(:password, SecureRandom.hex)      
      @user.reload.login_token.wont_be :empty?
    end
    
    it "must reset the login token" do
      @user.reset_login_token.login_token.wont_equal @token
    end
    
    it "must reset and save the login token" do
      @user.reset_login_token!
      @user.reload.login_token.wont_equal @token
    end
  end
  
  describe "merge" do
    before do
      @user = FactoryGirl.create(:user, :john, :courses)
      @duplicate = FactoryGirl.create(:duplicate_user)
    end
    
    it "must merge successfully" do
      @user.merge(@duplicate).must_equal @user
    end
    
    [ :billing_address, :events, :affiliations ].each do |association|
      if User.reflect_on_association(association).collection?
        it "must include #{association}" do
          @user.merge(@duplicate)
          @user.send(association).must_include @duplicate.send(association).first
        end
      else
        it "must merge #{association}" do
          @user.merge(@duplicate)
          @user.send(association).wont_be :blank?
          @user.send(association).must_equal @duplicate.send(association)
        end
      end
    end
    
    it "must append data in merge" do
      User.merge_associations.must_include :events
      
      a = @user.events.size
      a.must_be :>, 0
      b = @duplicate.events.size      
      b.must_be :>, 0

      @user.merge(@duplicate)
      @user.events.size.must_equal a + b
    end
    
    it "must remove the duplicate user" do
      @user.merge(@duplicate)
      proc { @duplicate.reload }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must require duplicate to be a user instance" do
      proc { @user.merge(FactoryGirl.create(:school)) }.must_raise ArgumentError
    end
    
    it "must handle merging self" do
      @user.merge(@user).must_equal @user
      @user.reload.must_be_kind_of User
    end
  end
  
  describe FullName do
    it "must return first_name" do
      @user.first_name.must_equal 'John'
    end
    
    it "must return last_name" do
      @user.last_name.must_equal 'Smith'
    end
    
    it "must assign first_name" do
      @user.first_name = 'Bob'
      @user.name.must_equal 'Bob Smith'
    end
    
    it "must assign last_name" do
      @user.last_name = 'Jones'
      @user.name.must_equal 'John Jones'
    end
  end
end
