require "test_helper"

describe AppearanceAlert do
  let(:school) { FactoryGirl.create(:school) }
  let(:alert) { AppearanceAlert.new(school, user_id: 1, user_name: 'Jane Doe', course_name: 'Course 1') }
  
  it "must supply a scope" do
    AppearanceAlert.scope(school).must_be_kind_of ActiveRecord::Relation
  end
  
  it "must descibe a appearance alert" do
    alert.format.must_match %r{<a href='.+?'>Jane Doe</a> attended Course 1}
  end
  
  it "must descibe a bumped appearance alert" do
    alert.bump!
    alert.user_count.must_equal 2
    alert.format.must_match %r{<a href='.+?'>Jane Doe</a> and 1 other person attended Course 1}
  end
end