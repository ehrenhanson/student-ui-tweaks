require "test_helper"

describe OrderAlert do
  let(:school) { FactoryGirl.create(:school) }
  let(:alert) { OrderAlert.new(school, user_id: 1, user_name: 'Jane Doe', product_name: 'Product 1', purchase_amount: 1) }
  
  it "must supply a scope" do
    OrderAlert.scope(school).must_be_kind_of ActiveRecord::Relation
  end
  
  it "must descibe a perchase alert" do
    alert.format.must_match %r{<a href='.+?'>Jane Doe</a> purchased Product 1 for \$1}
  end
  
  it "must descibe a bumped perchase alert" do    
    alert.bump!
    alert.user_count.must_equal 2
    alert.format.must_match %r{<a href='.+?'>Jane Doe</a> and 1 other person purchased Product 1 for \$1}
  end
end