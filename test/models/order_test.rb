require "test_helper"

describe Order do
  before do
    @order = FactoryGirl.build(:order)
  end

  it "must be valid" do
    @order.valid?.must_equal true
  end
  
  it "must assign user billing address" do
    @order.user_billing_address_attributes = { address: 'Test Value' }
    @order.user.billing_address.address.must_equal 'Test Value'
  end
  
  it "must save the associated user when changing billing address" do
    @order = FactoryGirl.create(:order, :without_address)
    @order.user_billing_address_attributes = FactoryGirl.attributes_for(:address)
    @order.save.must_equal true
    User.find(@order.user).billing_address.wont_be :nil?
  end
  
  describe Order::Checkout do
    def MockProcessor(successful)
      response = mock()
      response.stubs(:success?).returns(successful)
      response.stubs(:errors).returns([ stub(message: 'Test Message') ]) unless successful
      
      stub(sale: response)
    end
    
    before do
      @order = FactoryGirl.create(:order, :with_items)
      @card = FactoryGirl.build(:card)
      
      Payment.gateway = MockProcessor(true)
    end
    
    it "must provide total" do
      @order.total.must_equal 10
    end
    
    it "must generate credit" do
      @order.save_and_generate_credit!(@card).must_be_kind_of Credit
    end
    
    it "must raise payment error on failed transaction" do
      Payment.gateway = MockProcessor(false)
      proc { @order.save_and_generate_credit!(@card) }.must_raise Payment::PaymentError
    end
    
    it "must not create credits on failed transaction" do
      Payment.gateway = MockProcessor(false)
      @order.save_and_generate_credit!(@card) rescue nil
      Credit.count.must_equal 0
    end
  end
  
  describe Order::CreditCardAccessors do
    it "must provide a credit card object" do
      @order.credit_card.must_be_kind_of CreditCard
    end
    
    it "must assign credit card attributes" do
      @order.credit_card_attributes = { number: '1234567890' }
      @order.credit_card.number.must_equal '1234567890'
    end
    
    it "must assign partial credit card attributes" do
      @order.credit_card.expiration_month = '09'
      @order.credit_card_attributes = { number: '1234567890' }
      @order.credit_card.expiration_month.must_equal '09'
    end
  end
end
