require "test_helper"

describe Registration do
  before do
    @registration = FactoryGirl.build(:registration)
  end

  it "must be valid" do
    @registration.valid?.must_equal true
  end
  
  it "must have starts_on assigned" do
    @registration.starts_on.must_be :present?
  end
  
  it "must have starts_on assigned to a recurrence date" do
    @registration.starts_on.wont_equal @registration.event.starts_on
  end
  
  it "must return assumed event" do
    @registration.assumed_event.starts_on.must_equal @registration.starts_on
  end
  
  it "must return events between dates" do
    @registration.save!
    Registration.between(@registration.starts_on - 1.week, @registration.starts_on + 1.week).must_include @registration
  end
  
  it "must associate with school" do
    Registration.under_school(FactoryGirl.create(:school)).must_be :empty?
  end
  
  describe "#rsvp?" do
    it "must be true when no credit is present" do
      @registration.rsvp?.must_equal true
    end
    
    it "must be false when a credit is present" do
      @registration.credit = FactoryGirl.build(:credit)
      @registration.rsvp?.must_equal false
    end
  end
  
  describe Registration::Set do
    before do
      @registration = FactoryGirl.create(:registration)
      @date_range = [ @registration.starts_on - 1, @registration.starts_on + 1 ]
      @set = Registration.set(*@date_range)
    end
    
    it "must return a Participation::Set" do
      @set.must_be_kind_of Registration::Set
    end
    
    it "must populate the set" do
      @set.empty?.wont_equal true
    end
    
    it "must include the event in the set" do
      events = Event.between(*@date_range)

      events.include?(@registration.event).must_equal true
      events.any? { |event| @set.include?(event) }.must_equal true
    end
    
    it "wont include the event on a future date" do
      future_date_rage = @date_range.map { |date| date + 7 }
      events = Event.between(*future_date_rage)

      events.include?(@registration.event).must_equal true
      events.any? { |event| @set.include?(event) }.wont_equal true
    end
  end
end
