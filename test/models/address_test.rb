require "test_helper"

describe Address do
  before do
    Address.destroy_all
    @address = FactoryGirl.build(:address)
  end
  
  it "must be valid" do
    @address.valid?.must_equal true
  end
  
  it "must be valid if all fields are empty" do
    Address.new.valid?.must_equal true
  end
  
  it "wont be empty if one field is empty" do
    @address.address = ''
    @address.valid?.wont_equal true
  end
  
  it "must be valid while empty with addressable object" do
    @address = FactoryGirl.build(:empty_address)
    @address.valid?.must_equal true
  end
  
  it "wont be valid while empty with addressable object address_required set" do
    @address = FactoryGirl.build(:empty_address)
    @address.address_required = true
    @address.valid?.wont_equal true
  end
  
  describe BillingAddress do
    before do
      @address = FactoryGirl.build(:billing_address)
    end
    
    it "must default to billing address" do
      @address.address_type.must_equal 'billing'
    end
    
    it "must select billing addresses" do
      @address.save!
      FactoryGirl.create(:address)
      
      Address.count.must_equal 2
      BillingAddress.count.must_equal 1
    end
  end
end
