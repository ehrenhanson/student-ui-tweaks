require "test_helper"

describe Event do
  before do
    @event = FactoryGirl.build(:event)
  end

  it "must be valid" do
    @event.valid?.must_equal true
  end
  
  it "must have an instructor" do
    @event.instructor.must_be_kind_of Instructor
  end
  
  it "must be a parent" do
    @event.parent?.must_equal true
  end
  
  it "must allow registered assignment" do
    @event.registered = true
    @event.registered.must_equal true
  end
  
  it "must allow registered reading" do
    @event.registered.must_equal nil
  end
  
  it "must have registrations assocaition" do
    @event.registrations.must_be_kind_of ActiveRecord::Associations::CollectionProxy
  end
    
  # describe "of instructor" do
  #   it "must require an instructor" do
  #     @event.instructor = nil
  #     @event.valid?.must_equal false
  #   end
  #   
  #   it "must use the custom error message" do
  #     @event.instructor = nil
  #     @event.valid?
  #     @event.errors.to_a.join.must_equal 'Instructor is required and needs role >= faculty'
  #   end
  # end  
  
  describe Event::Recurrence do
    it "must convert starts_at to date" do
      @event.starts_on.must_be_kind_of Date
    end
  
    it "must convert ends_at to date" do
      @event.ends_on.must_be_kind_of Date
    end
    
    it "must assume a future occurring date" do
      @date = Date.today
      @event = FactoryGirl.build(:event, :daily)
      @event.assume(@date).starts_on.must_equal @date
    end
    
    it "wont assume a future occurring date if not occurring" do
      @event = FactoryGirl.build(:event, :weekly)
      @date = @event.starts_on + 1.day
      @event.assume(@date).starts_on.wont_equal @date
    end
  
    describe "between" do
      it "must select daily events" do
        run_between(Date.new(2013, 9, 1), Date.new(2013, 9, 30), :daily).must_equal 30
      end
    
      it "must select daily events excluding exclusions" do
        run_between(Date.new(2013, 9, 1), Date.new(2013, 9, 30), :daily, :exclusion).must_equal 29
      end

      it "must select weekly events" do
        run_between(Date.new(2013, 9, 1), Date.new(2013, 9, 30), :weekly).must_equal 4
      end
    
      it "must select bi-weekly events" do
        run_between(Date.new(2013, 9, 1), Date.new(2013, 9, 30), :biweekly).must_equal 9
      end

      it "must select bi-weekly events excluding exclusions" do
        run_between(Date.new(2013, 9, 1), Date.new(2013, 9, 30), :biweekly, :exclusion).must_equal 8
      end
    
      it "must select bi-weekly events in the next month" do
        run_between(Date.new(2013, 10, 1), Date.new(2013, 10, 31), :biweekly).must_equal 9
      end
    
      it "must select monthly events" do
        run_between(Date.new(2013, 10, 1), Date.new(2013, 10, 31), :monthly).must_equal 1
      end
    
      it "wont select events in the past" do
        run_between(Date.new(2013, 8, 1), Date.new(2013, 8, 30), :daily).must_equal 0
      end
    
      it "wont select events after the repeat until date" do
        run_between(Date.new(2013, 11, 1), Date.new(2013, 11, 30), :daily, :repeat_until).must_equal 0
      end
    
      it "must assign the recurring start/end times" do
        FactoryGirl.create(:event, :weekly)
        events = Event.between(Date.new(2013, 10, 1), Date.new(2013, 10, 31))

        valid_times = [
          Time.utc(2013, 10, 2, 20, 0),
          Time.utc(2013, 10, 9, 20, 0),
          Time.utc(2013, 10, 16, 20, 0),
          Time.utc(2013, 10, 23, 20, 0),
          Time.utc(2013, 10, 30, 20, 0)
        ]
      
        events.each do |event|
          time = valid_times.shift
          event.starts_at.must_equal time
          event.ends_at.must_equal time + 1.hour
        end
      end
    
      it "must proxy event methods" do
        original_event = FactoryGirl.create(:event, :weekly)
        events = Event.between(Date.new(2013, 10, 1), Date.new(2013, 10, 31))
      
        events.each do |event|
          event.name.must_equal original_event.name
          event.description.must_equal original_event.description
        end
      end
    end
    
    describe "find_on" do
      before do
        @event = FactoryGirl.create(:event, :daily)        
      end
      
      it "must find the event" do
        event = Event.find_on(@event.id, Date.new(2013, 10, 1))
        event.must_equal @event
      end
      
      it "wont find the event with an invalid date" do
        @event.update_attributes!(repeat_type: 0)
        proc { Event.find_on(@event.id, Date.new(2000, 1, 1)) }.must_raise(ActiveRecord::RecordNotFound)
      end
      
      it "must assign the given date" do
        event = Event.find_on(@event.id, Date.new(2013, 10, 1))
        event.starts_at.must_equal Time.utc(2013, 10, 1, @event.starts_at.hour, @event.starts_at.min)
      end
      
      it "must find event with only the ID" do
        event = Event.find_on(@event.id)
        event.must_equal @event
      end
      
      it "must find event with compound ID" do
        event = Event.find_on("#{@event.id}-2013-10-01")
        event.starts_at.must_equal Time.utc(2013, 10, 1, @event.starts_at.hour, @event.starts_at.min)
      end
    end
  
    describe "occurring" do
      it "must be occurring on the defined start date" do
        @event.occurring?(@event.starts_at).must_equal true
      end
    
      it "must be occurring on the defined end date" do
        @event.occurring?(@event.ends_at).must_equal true
      end
    
      it "must occur daily" do
        @event.repeat_type = Event::REPEAT_DAILY
        @event.occurring?(Date.new(2013, 9, 20)).must_equal true
      end
    
      it "must occur daily excluding exclusions" do
        @event.repeat_type = Event::REPEAT_DAILY
        FactoryGirl.build(:exclusion, event: @event)
        @event.occurring?(Date.new(2013, 9, 18)).must_equal true
      end
    
      it "must occur weekly on Friday" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::FRIDAY
        @event.occurring?(Date.new(2013, 9, 20)).must_equal true
      end
    
      it "must occur weekly on Sunday" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::SUNDAY
        @event.occurring?(Date.new(2013, 9, 22)).must_equal true
      end
    
      it "must occur weekly on Monday and Friday" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::MONDAY | Event::FRIDAY
        @event.occurring?(Date.new(2013, 9, 20)).must_equal true
        @event.occurring?(Date.new(2013, 9, 23)).must_equal true
        @event.occurring?(Date.new(2013, 9, 21)).must_equal false
      end
    
      it "must occur weekdays" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::MONDAY | Event::TUESDAY | Event::WEDNESDAY | Event::THURSDAY  | Event::FRIDAY
      
        (Date.new(2013, 9, 1)..Date.new(2013, 9, 30)).each do |date|
          @event.occurring?(date).must_equal !(date.wday == 0 || date.wday == 6)
        end
      end
    
      it "must occur monthly" do
        @event.repeat_type = Event::REPEAT_MONTHLY
        @event.occurring?(Date.new(2013, 10, 21)).must_equal true
      end
    
      it "must occur across multiple days" do
        @event.ends_at += 1.day
        @event.occurring?(@event.starts_at).must_equal true
        @event.occurring?(@event.ends_at).must_equal true
      end
    
      # TODO: Test recurrence across multiple days
    end
  
    describe "excluded" do
      it "must exclude excluded dates" do
        exclusion = FactoryGirl.create(:exclusion)
        exclusion.event.excludes?(exclusion.exclude_on).must_equal true
      end
    
      it "must not excluded dates that are not excluded" do
        exclusion = FactoryGirl.create(:exclusion)
        exclusion.event.excludes?(Date.new(2013, 10, 1)).must_equal false
      end
      
      it "must enumerate excluded events when queried between if reqeusted" do
        run_between(Date.new(2013, 9, 18), Date.new(2013, 9, 18), :daily, :exclusion).must_equal 0
        run_between(Date.new(2013, 9, 18), Date.new(2013, 9, 18), :daily, :exclusion, include_exclusions: true).must_equal 1
      end
      
      it "must flag as being excluded" do
        exclusion = FactoryGirl.create(:exclusion)
        Event.between(exclusion.exclude_on, exclusion.exclude_on, include_exclusions: true).first.excluded?.must_equal true
      end
    end
    
    describe "next occurence" do
      it "must occur on the same day when non-repeating" do
        @event.repeat_type = Event::REPEAT_NONE
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 16)
      end
      
      it "wont occur in the future when non-repeating" do
        @event.repeat_type = Event::REPEAT_NONE
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 17, 1))
        occurrence.must_equal nil
      end
      
      it "must occur on the same day when the event occurs after the current time" do
        @event.repeat_type = Event::REPEAT_DAILY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 16)
      end
      
      it "must occur on the next day when the event occurs before the current time" do
        @event.repeat_type = Event::REPEAT_DAILY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 21))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 17)
      end
      
      it "must occur in the same week" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::WEDNESDAY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 18)
      end
      
      it "must occur in the following week" do
        @event.repeat_type = Event::REPEAT_WEEKLY
        @event.repeat_interval = Event::SUNDAY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 22)
      end
      
      it "must occur in the same month" do
        @event.repeat_type = Event::REPEAT_MONTHLY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 16, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 9, 16)
      end
      
      it "must occur in the next month" do
        @event.repeat_type = Event::REPEAT_MONTHLY
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 17, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 10, 21)
      end

      it "must occur before the end of the repeat period" do
        @event.repeat_type = Event::REPEAT_MONTHLY
        @event.repeat_until = Date.new(2013, 10, 22)
        occurrence = @event.next_occurrence(Time.utc(2013, 9, 17, 1))
        occurrence.starts_at.to_date.must_equal Date.new(2013, 10, 21)
      end
      
      it "wont occur after the repeat period ends" do
        @event.repeat_type = Event::REPEAT_MONTHLY
        @event.repeat_until = Date.new(2013, 10, 22)
        occurrence = @event.next_occurrence(Time.utc(2013, 11, 17, 1))
        occurrence.must_equal nil
      end
    end
  end
  
  describe "association" do
    it "must destroy exclusions" do
      exclusion = FactoryGirl.create(:exclusion, event: @event)
      @event.destroy!
      proc { exclusion.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
  
  private
    def run_between(start_date, end_date, *traits)
      options = traits.extract_options!
      
      Event.destroy_all
      FactoryGirl.create(:event, *traits)      

      Event.between(start_date, end_date, options).inject(0) { |count,_| count + 1 }
    end
end
