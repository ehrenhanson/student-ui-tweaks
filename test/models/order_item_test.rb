require "test_helper"

describe OrderItem do
  before do
    @order_item = FactoryGirl.build(:order_item)
  end

  it "must be valid" do
    @order_item.valid?.must_equal true
  end
  
  describe OrderItem::Price do
    it "must inherit price for product" do
      @order_item.price = nil
      @order_item.price.must_equal @order_item.product_price
    end
    
    it "must provide local price" do
      @order_item.price = '10.5'
      @order_item.price.must_equal BigDecimal.new('10.5')
    end
  end
  
  describe OrderItem::Purchasing do
    it "must create a credit" do
      @order_item.generate_credit!
      Credit.count.must_equal 1      
    end
  end
end
