require "test_helper"

describe RegistrationState do
  before do
    @school = FactoryGirl.create(:school, :with_owner, :courses_with_events)
    @state = RegistrationState.new(@school, @school.owner, @school.events.first.id, @school.events.first.starts_on)
  end
  
  it "must have a school" do
    @state.school.must_equal @school
  end
  
  it "must have a user" do
    @state.user.must_equal @school.owner
  end
  
  it "must have an event" do
    @state.event.must_equal @school.events.first
  end
  
  it "must have credits" do
    @state.credits.must_respond_to :each
  end
  
  it "must have products" do    
    @state.credits.must_respond_to :each
  end
  
  it "must have countires" do
    @state.countries.must_be_kind_of Array
  end
end
