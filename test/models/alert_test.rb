require "test_helper"

describe Alert do
  before do
    @order = FactoryGirl.create(:order, :with_items)
    @school = @order.school
    @product = @order.order_item.product
    @alerts = Alert.new(@school)
  end
  
  after do
    # Work around transactional fixtures not applying to objects created in before block
    School.destroy_all
    User.destroy_all
  end
  
  it "must filter time" do
    time = Time.now
    @alerts.filter(time).max_age.must_equal time
  end
  
  it "must return a purchase alert" do
    time, message = @alerts.each.first
    time.must_be_kind_of Time
    message.must_match %r{purchased Ten Pack for \$10}
  end
  
  it "must return multiple alerts" do
    FactoryGirl.create(:order_item, order: @order, product: @product, price: 100)
    
    @alerts.inject(0) do |count,(time,message)|
      time.must_be_kind_of Time
      message.must_match %r{purchased Ten Pack for \$\d+}
      count + 1
    end.must_equal 2
  end
  
  it "must group a similar alert" do
    FactoryGirl.create(:order_item, order: @order, product: @product)

    time, message = @alerts.each.first
    time.must_be_kind_of Time
    message.must_match %r{1 other person}    
  end
  
  it "must group many similar alerts" do
    FactoryGirl.create(:order_item, order: @order, product: @product)
    FactoryGirl.create(:order_item, order: @order, product: @product)
    FactoryGirl.create(:order_item, order: @order, product: @product)

    time, message = @alerts.each.first
    time.must_be_kind_of Time
    message.must_match %r{3 other people}
  end
  
  it "must be scoped by school" do
    school = FactoryGirl.create(:school)
    @alerts.size.must_equal 1
    Alert.new(school).size.must_equal 0
  end
end