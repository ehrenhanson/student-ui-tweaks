require "test_helper"

describe CreditCard do
  before do
    @credit_card = CreditCard.new(number: '123456789', expiration_month: '01', expiration_year: '14')
  end
  
  it "must assign attributes" do
    @credit_card.number.must_equal '123456789'
    @credit_card.expiration_month.must_equal '01'
    @credit_card.expiration_year.must_equal '14'
  end
end