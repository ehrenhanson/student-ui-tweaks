require "test_helper"

describe School do
  before do
    @school = FactoryGirl.build(:school)
  end

  it "must be valid" do
    @school.valid?.must_equal true
  end
  
  it "must provide alerts" do
    @school.alerts.must_be_kind_of Alert
  end
  
  it "must provide school owner" do
    affiliation = FactoryGirl.create(:affiliation, role: Role.owner, school: @school)
    @school.owner.must_equal affiliation.user
  end
  
  it "must provide instructors" do
    course = FactoryGirl.create(:course, school: @school)
    FactoryGirl.create_list(:event, 5, course: course)
    @school.reload.instructors.size.must_equal 5
  end
  
  it "must provide unique instructors" do
    user = FactoryGirl.create(:user, :jane)
    course = FactoryGirl.create(:course, school: @school)
    FactoryGirl.create_list(:event, 5, course: course, instructor: user)
    @school.reload.instructors.size.must_equal 1
  end
  
  it "must find placements" do
    product = FactoryGirl.create(:product, school: @school)
    placement = FactoryGirl.create(:placement, product: product)
    @school.placements.find(placement).must_equal placement
  end
  
  it "must filter placements by school" do
    placement = FactoryGirl.create(:placement)
    proc { @school.placements.find(placement) }.must_raise ActiveRecord::RecordNotFound
  end
  
  it "cannot have a reserved identifier" do
    @school.identifier = 'signup'
    @school.valid?.must_equal false
  end
  
  describe "identifier" do    
    it "must be assigned upon save" do
      @school.save!      
      @school.identifier.must_match /^hotyogaby\d+/
    end
    
    it "must not implicitly overwrite an existing value" do
      @school.identifier = 'hotyogaschool'
      @school.save!      
      @school.identifier.must_equal 'hotyogaschool'
    end
    
    it "must be able to explicitly overwrite an existing value" do
      @school.save!      
      @school.identifier = 'hotyogaschool'
      @school.save!
      @school.identifier.must_equal 'hotyogaschool'
    end
    
    it "must restrict the identifer to the valid character set" do
      @school.identifier = 'Learn<Math>1.2.3'
      @school.identifier.must_equal 'learnmath123'
    end
  end
  
  describe "validation" do
    describe "of name" do
      it "must require a name" do
        @school.name = nil
        @school.valid?.must_equal false
      end
    end
    
    describe "of identifer" do
      it "must require an identifier" do
        @school.name = nil
        @school.identifier = nil
        @school.valid?.must_equal false
      end
      
      it "must be unique" do
        @school.save!
        duplicate = FactoryGirl.build(:school, name: @school.name)        
        duplicate.valid?.must_equal false
        proc { duplicate.save! }.must_raise ActiveRecord::RecordInvalid
      end
    end
    
    describe "of address" do
      it "must require valid address fields" do
        address = @school.build_address
        address.address = '123 Fake St.'
        @school.valid?.must_equal false
      end
      
      it "must require valid address fields" do
        address = @school.build_address
        address.address = '123 Fake St.'
        address.city = 'Kingston'
        address.state = 'NY'
        address.zipcode = '12345'
        address.country = 'USA'
        @school.valid?.must_equal true
      end
      
      it "must require a valid address even if all fields are blank" do
        address = @school.build_address
        address.address = ''
        @school.valid?.must_equal false
      end
    end
  end
  
  describe "association" do
    it "must destroy affiliations" do
      affiliation = FactoryGirl.create(:affiliation, school: @school)
      @school.destroy!
      proc { affiliation.reload }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must destroy courses" do
      course = FactoryGirl.create(:course, school: @school)
      @school.destroy!
      proc { course.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
  
  describe "users with role" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, school: @school)
      @user = @affiliation.user
    end
    
    it "must provide a role method" do
      @school.users.with_role.first.respond_to?(:role).must_equal true
    end
    
    it "must use the role defined by the affiliation if greater than the user" do
      @user.role.must_be :<, @affiliation.role
      @school.users.with_role.first.role.must_equal @affiliation.role
    end
    
    it "must use the role defined by the user if greater than the affiliation" do
      @user.update_attributes(role: 5)
      @user.role.must_be :>, @affiliation.role
      @school.users.with_role.first.role.must_equal @user.role
    end
  end
  
  describe "users with filter" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, school: @school)
      @user = @affiliation.user
    end

    it "must select matching roles" do
      @affiliation.role.must_equal Role.student
      @school.users.filter('student').wont_be :empty?
    end
    
    it "must not select non-matching roles" do
      @affiliation.role.wont_equal Role.admin
      @school.users.filter('admin').must_be :empty?
    end
    
    it "must select matching roles when promoted by user role" do
      @user.update_attributes(role: Role.admin)
      @school.users.filter('admin').wont_be :empty?
    end
    
    it "must not use affiliations from other schools" do
      FactoryGirl.create(:affiliation, role: Role.owner)
      @school.users.filter('owner').must_be :empty?
    end
  end
  
  describe "as_role" do
    before do
      FactoryGirl.create_list(:school, 5)
    end
    
    it "must assign the role to every school instance" do
      School.as_role('admin').all? { |school| school.role == Role.admin }.must_equal true
    end
  end
  
  describe School::Discovery do
    before do
      @school = FactoryGirl.create(:school)
    end
    
    it "must find the school by identifier" do
      School.discover(@school.identifier).must_equal @school
    end
    
    it "must find the school by id" do
      School.discover(@school.id).must_equal @school
    end
    
    it "must raise not found if not matched" do
      proc { School.discover('nothing') }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must find the school from the merchant ID" do
      School.discover_merchant("#{@school.identifier}_school").must_equal @school
    end
  end
end
