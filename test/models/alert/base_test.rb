require "test_helper"

describe Alert::Base do
  let(:school) { FactoryGirl.build(:school) }
  
  it "must require fields (user_id, user_name, alert_created_at)" do
    class Good < Alert::Base
      def self.scope(school)
        school.users.select('users.id AS user_id, users.name AS user_name, schools.created_at AS alert_created_at')
      end
    end
    
    Good.find(school, 1.day.ago).wont_equal nil
  end
  
  it "must raise an error when required fields are not present" do
    class Bad < Alert::Base
      def self.scope(school)
        school.users
      end
    end
    
    proc { Bad.find(school, 1.day.ago) }.must_raise Alert::InvalidScope
  end
  
  it "must default to having one user" do
    base = Alert::Base.new(school, {})
    base.user_count.must_equal 1
  end
  
  it "must be able to bump user count" do
    base = Alert::Base.new(school, {})
    base.bump!
    base.user_count.must_equal 2
  end
  
  it "must successfuly compare similar alerts" do
    a1 = Alert::Base.new(school, user_id: 1, value: 2)
    a2 = Alert::Base.new(school, user_id: 2, value: 2)
    
    a1.same_as?(a2).must_equal true
  end
  
  it "must fail to compare dissimilar alerts" do
    a1 = Alert::Base.new(school, user_id: 1, value: 2)
    a2 = Alert::Base.new(school, user_id: 2, value: 3)
    
    a1.same_as?(a2).wont_equal true
  end
  
  it "must pass missing methods to the attributes hash" do
    alert = Alert::Base.new(school, a: 1, b: 2, c: 3)
    alert.a.must_equal 1
    alert.b.must_equal 2
    alert.c.must_equal 3
  end
  
  it "must raise an error if the missing method is also not found in the attributes hash" do
    alert = Alert::Base.new(school, a: 1, b: 2, c: 3)
    proc { alert.d }.must_raise NoMethodError
  end
  
  it "must provide a user link available to subclasses" do
    class Good < Alert::Base
      def format
        user_link
      end
    end

    alert = Good.new(stub(to_param: 1), user_id: 1, user_name: 'Mary Thompson')
    alert.format.must_match "<a href='/api/v1/schools/1/users/1'>Mary Thompson</a>"
  end
  
  it "must provide a user link and 1 other person" do
    class Good < Alert::Base
      def format
        user_link
      end
    end

    alert = Good.new(stub(to_param: 1), user_id: 1, user_name: 'Mary Thompson')
    alert.bump!
    alert.format.must_match "<a href='/api/v1/schools/1/users/1'>Mary Thompson</a> and 1 other person"
  end
  
  it "must provide a user link and 2 other people" do
    class Good < Alert::Base
      def format
        user_link
      end
    end

    alert = Good.new(stub(to_param: 1), user_id: 1, user_name: 'Mary Thompson')
    alert.bump!
    alert.bump!
    alert.format.must_match "<a href='/api/v1/schools/1/users/1'>Mary Thompson</a> and 2 other people"
  end
  
  it "must return the time and message upon call" do
    class Simple < Alert::Base
      def format
        '1'
      end
    end
    
    original_time = Time.now.utc
    alert = Simple.new(school, alert_created_at: original_time.to_s)
    time, message = alert.call
    
    time.to_i.must_equal original_time.to_i
    message.must_equal '1'
  end
end
