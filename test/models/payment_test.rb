require "test_helper"

describe Payment do
  before do
    @order = FactoryGirl.create(:order, :with_items)
    @payment = Payment.new(@order, FactoryGirl.build(:card))
  end
  
  it "must have order" do
    @payment.order.must_equal @order
  end
  
  it "must have user" do
    @payment.user.must_equal @order.user
  end
  
  it "must have card info" do
    @payment.card.must_be_kind_of Hash
  end
  
  it "must successfully complete a transaction" do
    mock_payment_gateway(true)
    @payment.process!.must_equal true
  end
  
  it "must raise an error on a failed transaction" do
    mock_payment_gateway(false, message: 'Test Message')
    proc { @payment.process! }.must_raise Payment::PaymentError
  end
end
