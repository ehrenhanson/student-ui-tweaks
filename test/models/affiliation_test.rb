require "test_helper"

describe Affiliation do
  before do
    @affiliation = FactoryGirl.build(:affiliation)
  end

  it "must be valid" do
    @affiliation.valid?.must_equal true
  end
  
  it "must include role module" do
    Affiliation.included_modules.include?(Role).must_equal true
  end
  
  it "must be unique" do
    @affiliation.save!
    @affiliation.dup.save.wont_equal true
  end
  
  describe "#update_active" do
    before do
      @affiliations = FactoryGirl.create_list(:affiliation, 5)
    end
    
    it "must update active" do
      Affiliation.update_active_school(@affiliations.first.school_id)
      Affiliation.find_by(active: true).must_equal @affiliations.first
    end
    
    it "must remove active from previous actives" do
      Affiliation.update_active_school(@affiliations.first.school_id)
      Affiliation.update_active_school(@affiliations.second.school_id)
      Affiliation.where(active: true).count.must_equal 1
      Affiliation.find_by(active: true).must_equal @affiliations.second
    end
  end
end
