require "test_helper"

describe RegistrationManager do
  let(:manager) { FactoryGirl.build(:registration_manager) }
  
  describe "#initialize" do
    it "must initialize school" do
      manager.school.must_be_instance_of School
    end
  
    it "must initialize user" do
      manager.user.must_be_instance_of User
    end
  
    it "must initialize event" do
      manager.event.must_be_instance_of Event
    end
  end
  
  describe "#save" do
    let(:purchase_manager) { FactoryGirl.build(:registration_manager, :purchase) }
    let(:purchase_manager_without_address) { FactoryGirl.build(:registration_manager, :purchase, address: nil) }
    let(:credit_manager) { FactoryGirl.build(:registration_manager, :credit) }
    
    it "must successfully save registration" do
      manager.save.must_equal true
      Registration.count.must_equal 1
      Registration.first.credit.must_be_nil
    end
    
    it "must assign the registration on save" do
      manager.save.must_equal true
      manager.registration.must_be_instance_of Registration
    end
    
    it "wont save purchase when address is missing" do
      mock_payment_gateway(true)
      purchase_manager_without_address.save.must_equal false
    end
    
    it "must successfully save registration with purchase" do
      mock_payment_gateway(true)
      purchase_manager.save.must_equal true
      Registration.count.must_equal 1
      Registration.first.credit.wont_be_nil
    end
    
    it "wont save registration with failed purchase" do
      mock_payment_gateway(false, message: 'Failed')
      purchase_manager.save.must_equal false
      Registration.count.must_equal 0
    end
    
    it "must successfully save registration with credit" do
      credit_manager.save.must_equal true
      Registration.count.must_equal 1
      Registration.first.credit.wont_be_nil
    end
  end
end
