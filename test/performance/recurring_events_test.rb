require 'test_helper'
require 'rails/performance_test_help'

class RecurringEventsTest < ActionDispatch::PerformanceTest
  # Refer to the documentation for all available options
  # self.profile_options = { runs: 5, metrics: [:wall_time, :memory],
  #                          output: 'tmp/performance', formats: [:flat] }
  
  setup do
    @events = FactoryGirl.create_list(:event, 100, :biweekly)
  end
  
  teardown do
    Event.destroy_all
  end

  test "one day" do
    Event.between(Date.new(2013, 9, 16), Date.new(2013, 9, 16)).each do |event|
      event
    end
  end
    
  test "one week" do
    Event.between(Date.new(2013, 9, 15), Date.new(2013, 9, 21)).each do |event|
      event
    end
  end
  
  test "one month" do
    Event.between(Date.new(2013, 9, 1), Date.new(2013, 9, 30)).each do |event|
      event
    end
  end
end
