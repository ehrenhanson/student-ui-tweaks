require "test_helper"

describe UserMailer do
  before do
    @user = FactoryGirl.build(:user, :john)
    @user.activate_reset_password_token!
  end
  
  it "must create reset password email" do
    mail = UserMailer.reset_password(@user)
    mail.from.must_include 'noreply@viewcy.com'
    mail.to.must_include @user.email    
  end
  
  it "must deliver reset password email" do
    UserMailer.reset_password(@user).deliver
    ActionMailer::Base.deliveries.wont_be :empty?
  end
end
