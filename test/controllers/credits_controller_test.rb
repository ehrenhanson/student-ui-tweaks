require 'test_helper'

describe CreditsController do
  before do
    @user = FactoryGirl.create(:user, :john)
    @school = FactoryGirl.create(:school)
    @credits = FactoryGirl.create_list(:credit, 5, user: @user)
    @credits.first(3).each { |credit| credit.courses.update_all(school_id: @school) }

    @controller.stubs(:current_user).returns(@user)
    setup_school_context(@school)
  end
  
  describe "#index;js" do
    it "must succeed" do
      get :index, format: :js
      assert_response :success
    end
    
    it "must return credits" do
      get :index, format: :js
      assigns(:credits).must_equal @credits.first(3)
    end

    it "must render template" do
      get :index, format: :js
      assert_template 'index'
    end
  end
end