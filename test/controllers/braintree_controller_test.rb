require 'test_helper'

describe BraintreeController do
  describe "#accept" do
    before do
      @school = FactoryGirl.create(:school)
    end
    
    let :success do
      stub :merchant_account => stub(:id => "#{@school.identifier}_school"), kind: Braintree::WebhookNotification::Kind::SubMerchantAccountApproved
    end
    
    let :failed do
      stub :merchant_account => stub(:id => "#{@school.identifier}_school"), kind: Braintree::WebhookNotification::Kind::SubMerchantAccountDeclined
    end
    
    it "must succeed" do
      Braintree::WebhookNotification.expects(:parse).returns(success)      
      post :accept, bt_signature: 1, bt_payload: 1
      assert_response :success
    end
    
    it "must approve school" do
      Braintree::WebhookNotification.expects(:parse).returns(success)      
      post :accept, bt_signature: 1, bt_payload: 1
      @school.reload.approved?.must_equal true
    end
    
    it "must decline school" do
      Braintree::WebhookNotification.expects(:parse).returns(failed)      
      post :accept, bt_signature: 1, bt_payload: 1
      @school.reload.declined?.must_equal true
    end
  end
end
