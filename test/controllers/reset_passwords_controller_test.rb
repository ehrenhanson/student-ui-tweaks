require "test_helper"

describe ResetPasswordsController do
  before do
    @user = FactoryGirl.create(:user, :john, reset_password_token: '1')
  end
  
  describe "#show" do
    it "must succeed" do
      get :show, id: @user.reset_password_token
      assert_response :success
    end
    
    it "must be not found when given invalid ID" do
      get :show, id: '0'
      assert_response :redirect
      assert_redirected_to new_reset_password_url
    end
  end
  
  describe "#new" do
    it "must succeed" do
      get :new
      assert_response :success
    end
  end
  
  describe "#create" do
    it "must succeed" do
      post :create, email: @user.email
      assert_response :success
      assert_template 'create'
    end
    
    it "must assign a new password token" do
      User.any_instance.expects(:activate_reset_password_token!)
      post :create, email: @user.email
    end
    
    it "must fail with invalid email" do
      post :create, email: 'invalid@address'
      assert_response :success
      assert_template 'new'
    end
  end
  
  describe "#update" do
    it "must succeed" do
      put :update, id: @user.reset_password_token, password: 'testing123', password_confirmation: 'testing123'
      assert_response :redirect
      assert_redirected_to new_session_url
    end
    
    it "must change the password" do
      put :update, id: @user.reset_password_token, password: 'testing123', password_confirmation: 'testing123'
      User.authenticate(@user.email, 'testing123').must_equal @user
    end
    
    it "must clear the reset password token" do
      put :update, id: @user.reset_password_token, password: 'testing123', password_confirmation: 'testing123'
      @user.reload.reset_password_token.must_be :nil?
    end
    
    it "must fail if the reset password token is invalid" do
      proc { put :update, id: '0', password: 'testing123', password_confirmation: 'testing123' }.must_raise ActiveRecord::RecordNotFound
    end
  end
end
