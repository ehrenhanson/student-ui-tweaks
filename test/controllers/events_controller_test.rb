require "test_helper"

describe EventsController do
  describe "#index" do
    before do
      @course = FactoryGirl.create(:course)
      FactoryGirl.create(:event, :daily, course: @course)
      FactoryGirl.create(:event, :biweekly, course: @course)
      
      setup_school_context(@course.school)
    end
    
    it "must succeed" do
      get :index, course_id: @course, start: '2014-01-01', end: '2014-01-14', format: :json
      assert_response :success
    end
  end
end
