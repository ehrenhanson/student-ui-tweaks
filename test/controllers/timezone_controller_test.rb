require "test_helper"

class TimezoneController < ApplicationController
  def zone
    @zone = Time.zone
    head :ok
  end
end

def route(&block)
  with_routing do |map|
    map.draw do
      get :zone, to: 'timezone#zone'
    end
  
    yield
  end
end

describe TimezoneController do
  before do
    @controller = TimezoneController.new
    @controller.request = stub(parameters: {})
  end  

  it "must setup a default time zone" do
    route do |map|
      get :zone
      assigns(:zone).name.must_equal 'Eastern Time (US & Canada)'
    end
  end
  
  it "must use school's timezone" do
    @school = FactoryGirl.build_stubbed(:school, timezone: 'American Samoa')
    @controller.stubs(:current_school).returns(@school)
    
    route do
      get :zone
      assigns(:zone).name.must_equal 'American Samoa'
    end
  end
end
