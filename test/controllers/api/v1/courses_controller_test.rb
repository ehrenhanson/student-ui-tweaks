require "test_helper"

describe Api::V1::CoursesController do
  describe "in school context" do
    before do
      @course = FactoryGirl.create(:course, :with_event)
      @school = @course.school
      @user = @course.instructors.first
      @affiliation = FactoryGirl.create(:affiliation, school: @school, user: @user)
      @controller.stubs(:current_user).returns(@user)
    end
    
    describe "routes" do
      it "must generate member route" do
        api_v1_school_course_path(@school, @course).must_equal "/api/v1/schools/#{@school.to_param}/courses/#{@course.to_param}"
      end
    end
    
    describe "#index" do
      it "must succeed" do
        get :index, format: :json, school_id: @school
        assert_response :success
      end
    
      it "must return valid JSON" do
        get :index, format: :json, school_id: @school
        JSON.parse(response.body).must_be_kind_of Hash
        JSON.parse(response.body)['objects'].must_be_kind_of Array
      end
    
      it "must provide user attributes" do
        get :index, format: :json, school_id: @school
        course = JSON.parse(response.body)['objects'].first.with_indifferent_access
        course.values_at(:id, :name, :description, :level).all?.must_equal true
      end
    end
    
    describe "#show" do
      it "must succeed" do
        get :show, school_id: @course.school, id: @course, format: :json
        assert_response :success
      end
      
      it "must return valid JSON" do
        get :show, school_id: @course.school, id: @course, format: :json
        JSON.parse(response.body)
      end
    end
  
    describe "#create" do
      let(:attributes) { FactoryGirl.attributes_for(:course).merge!(instructor_id: @user.id) }
  
      describe "as admin" do
        before do
          @user.update_attributes!(role: Role.admin)
        end
        
        it "must create a new course" do
          post :create, format: :json, school_id: @school, course: attributes
          assert_response :created
          assigns(:course).new_record?.must_equal false
        end
        
        it "must render the show template" do
          post :create, format: :json, school_id: @school, course: attributes
          assert_template 'show'
        end
      
        it "must return JSON" do
          post :create, format: :json, school_id: @school, course: attributes
          JSON.parse(response.body)['name'].wont_be :blank?
        end
  
        it "must assign location header" do
          post :create, format: :json, school_id: @school, course: attributes
          response['Location'].must_equal api_v1_school_course_url(@school, assigns(:course))
        end
        
        it "must handle validation errors" do
          post :create, format: :json, school_id: @school, course: { name: '' }
          assert_response :unprocessable_entity
          JSON.parse(response.body).has_key?('name').must_equal true
        end
      end
    
      describe "as faculty" do
        before do
          @user.update_attributes!(role: Role.faculty)
        end
    
        it "must create a new course" do
          post :create, format: :json, school_id: @school, course: attributes
          assert_response :created
          assigns(:course).new_record?.must_equal false
        end
  
        it "must render the show template" do
          post :create, format: :json, school_id: @school, course: attributes
          assert_template 'show'
        end
      
        it "must return JSON" do
          post :create, format: :json, school_id: @school, course: attributes
          JSON.parse(response.body)['name'].wont_be :blank?
        end      
        
        it "must assign location header" do
          post :create, format: :json, school_id: @school, course: attributes
          response['Location'].must_equal api_v1_school_course_url(@school, assigns(:course))
        end
  
        it "must handle validation errors" do
          post :create, format: :json, school_id: @school, course: { name: '' }
          assert_response :unprocessable_entity
          JSON.parse(response.body).has_key?('name').must_equal true
        end
      end
  
      describe "as user" do
        it "must not be able to create a new course" do
          post :create, format: :json, school_id: @school, course: attributes
          assert_response :forbidden
        end
      end
    end

    describe "#update" do
      describe "as admin" do
        before do
          @user.update_attributes!(role: Role.admin)
        end
    
        it "must update course" do
          put :update, format: :json, school_id: @course.school, id: @course, course: { name: 'New Course' }
          assert_response :no_content
          @course.reload.name.must_equal 'New Course'
        end
  
        it "must handle validation errors" do
          put :update, format: :json, school_id: @course.school, id: @course, course: { name: '' }
          assert_response :unprocessable_entity
          JSON.parse(response.body).has_key?('name').must_equal true
        end
      end
  
      describe "as owner" do
        before do
          @user.update_attributes!(role: Role.owner)
        end
    
        it "must update course" do
          put :update, format: :json, school_id: @course.school, id: @course, course: { name: 'New Course' }
          assert_response :no_content
          @course.reload.name.must_equal 'New Course'
        end
  
        it "must handle validation errors" do
          put :update, format: :json, school_id: @course.school, id: @course, course: { name: '' }
          assert_response :unprocessable_entity
          JSON.parse(response.body).has_key?('name').must_equal true
        end
      end
  
      describe "as user" do
        it "must not be able to update a course" do
          put :update, format: :json, school_id: @course.school, id: @course, course: FactoryGirl.attributes_for(:school)
          assert_response :forbidden
        end
      end
    end
    
    describe "#destroy" do
      it "must succeed" do
        @user.update_attributes!(role: Role.faculty)
        delete :destroy, format: :json, school_id: @course.school, id: @course
        assert_response :no_content
      end
      
      it "must fail as student" do
        @user.update_attributes!(role: Role.student)
        delete :destroy, format: :json, school_id: @course.school, id: @course
        assert_response :forbidden
      end
    end
  end
end
