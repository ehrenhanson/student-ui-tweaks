require 'test_helper'

class Api::V1::RegistrationsControllerTest < ActionController::TestCase
  before do
    @affiliation = FactoryGirl.create(:affiliation, role: Role.faculty)
    @user = @affiliation.user
    @school = @affiliation.school
    @course = FactoryGirl.create(:course, :with_event, school: @school)
    @event = @course.events.first
    @registration = Registration.create(event: @event, starts_on: @event.starts_on, user: @user)
    @user.update_attributes!(role: Role.admin)
    @controller.stubs(:current_user).returns(@user)    
  end
  
  describe "#index" do
    it "must succeed" do
      get :index, format: :json, school_id: @school, event_id: @event
      assert_response :success
    end
    
    it "must must return a user" do
      get :index, format: :json, school_id: @school, event_id: @event
      JSON.parse(response.body)['objects'].first['user'].must_be :present?
    end  
  end
end
