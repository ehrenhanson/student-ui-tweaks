require "test_helper"

describe Api::V1::UsersController do
  describe "in admin context" do
    before do
      @affiliation = FactoryGirl.create(:affiliation, role: Role.faculty)
      @user = @affiliation.user
      @school = @affiliation.school
      @user.update_attributes!(role: Role.admin)
      @controller.stubs(:current_user).returns(@user)
    end
    
    describe "routes" do
      it "must generate member route" do
        api_v1_school_user_path(@school, @user).must_equal "/api/v1/schools/#{@school.to_param}/users/#{@user.to_param}"
      end
    end
    
    describe "#index" do
      it "must succeed" do
        get :index, format: :json, school_id: @school
        assert_response :success
      end
      
      it "must fail for non-admin user" do
        @user.update_attributes!(role: Role.student)
        get :index, format: :json, school_id: @school
        assert_response :forbidden
      end
      
      it "must return valid JSON" do
        get :index, format: :json, school_id: @school
        JSON.parse(response.body).must_be_kind_of Hash
        JSON.parse(response.body)['objects'].must_be_kind_of Array
      end
      
      it "must provide user attributes" do
        get :index, format: :json, school_id: @school
        user = JSON.parse(response.body)['objects'].first.with_indifferent_access
        user.values_at(:id, :name, :email).all?.must_equal true
      end
      
      it "must return role as a string value" do
        get :index, format: :json, school_id: @school
        user = JSON.parse(response.body)['objects'].first.with_indifferent_access
        user[:role].must_equal 'admin'
      end
    end
    
    describe "#show" do
      before do
        @student = FactoryGirl.create(:affiliation, school: @school).user
      end
      
      it "must succeed" do
        get :show, format: :json, school_id: @school, id: @student
        assert_response :success
      end
      
      it "must return valid JSON" do
        get :show, format: :json, school_id: @school, id: @student
        JSON.parse(response.body).must_be_kind_of Hash
      end
      
      it "must provide user attributes" do
        get :show, format: :json, school_id: @school, id: @student
        user = JSON.parse(response.body).with_indifferent_access
        user.values_at(:id, :name, :email).all?.must_equal true
      end
      
      it "must provide the requested user" do
        get :show, format: :json, school_id: @school, id: @student
        user = JSON.parse(response.body).with_indifferent_access
        user[:id].must_equal @student.id
      end
      
      it "must not provide the current user for non-admins" do
        @user.update_attributes!(role: Role.student)
        get :show, format: :json, school_id: @school, id: @student
        assert_response :forbidden
      end
    end
    
    describe "#create" do
      it "must create a new user" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane)
        assert_response :created
        assigns(:user).new_record?.must_equal false
      end
      
      it "must render the show template" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane)
        assert_template 'show'
      end
      
      it "must return JSON" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane)
        JSON.parse(response.body)['name'].must_equal 'Jane Doe'
      end
      
      it "must assign location header" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane)
        response['Location'].must_equal api_v1_school_user_url(@school, assigns(:user))
      end
      
      it "must handle validation errors" do
        post :create, format: :json, school_id: @school, user: { email: 'jane@example.com' }
        assert_response :unprocessable_entity
        JSON.parse(response.body).has_key?('name').must_equal true
      end
      
      it "must create a new user" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane), role: 'student'
        assert_response :created
      end
      
      it "must create a new user" do
        attributes = FactoryGirl.attributes_for(:user, :jane)
        attributes.delete(:name)
        attributes.merge!(first_name: 'Jane', last_name: 'Doe')
        
        post :create, format: :json, school_id: @school, user: attributes, role: 'student'
        assert_response :created
      end
      
      it "must create an affiliation for new user" do
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane), school_id: @school, role: 'student'
        assigns(:user).affiliations.any? { |a| a.school == @school }.must_equal true
      end
      
      it "must not create an affiliation with a greater role" do
        @user.update_attributes!(role: Role.user)
        post :create, format: :json, school_id: @school, user: FactoryGirl.attributes_for(:user, :jane), school_id: @school, role: 'admin'
        assert_response :forbidden
      end
    end
    
    describe "#invite" do
      before do
        @student = FactoryGirl.create(:affiliation, :basic_user, school: @school).user
      end
      
      it "must succeed" do
        post :invite, format: :json, school_id: @school, id: @student, email: 'duplicate@example.com'
        assert_response :no_content
      end
      
      it "must update email address" do
        post :invite, format: :json, school_id: @school, id: @student, email: 'duplicate@example.com'
        @student.reload.invite_token.wont_be :blank?
      end
      
      it "must be in invite state" do
        post :invite, format: :json, school_id: @school, id: @student, email: 'duplicate@example.com'
        @student.reload.invited?.must_equal true
      end
      
      it "must merge with existing user" do
        post :invite, format: :json, school_id: @school, id: @student, email: @user.email
        proc { @student.reload }.must_raise ActiveRecord::RecordNotFound
      end
      
      it "must be in invite state after merging" do
        post :invite, format: :json, school_id: @school, id: @student, email: @user.email
        @user.reload.invited?.must_equal true
      end
      
      it "must return an error if user has already been invited" do
        @user.register!
        post :invite, format: :json, school_id: @school, id: @student, email: @user.email
        assert_response :unprocessable_entity
      end
    end
    
    describe "#update" do
      before do
        @jane = FactoryGirl.create(:user, :jane)
      end
      
      it "must create a new user" do
        put :update, format: :json, school_id: @school, id: @jane.id, user: { name: 'Mary' }
        assert_response :no_content
        @jane.reload.name.must_equal 'Mary'
      end
      
      it "must handle validation errors" do
        put :update, format: :json, school_id: @school, id: @jane.id, user: { name: nil }
        assert_response :unprocessable_entity
        @jane.reload.name.must_equal 'Jane Doe'
      end
    end
  end
end
