require "test_helper"

describe Api::V1::ProductsController do
  before do
    @product = FactoryGirl.create(:product, :course)
    @course = @product.courses.first
    @school = @course.school
    @user = @school.affiliations.first.user
    
    @user.update_attributes!(role: Role.admin)
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "course scope" do
    describe "#index" do
      it "must succeed" do
        get :index, format: :json, school_id: @school, course_id: @course
        assert_response :success
      end
      
      it "must return valid JSON" do
        get :index, format: :json, school_id: @school, course_id: @course
        JSON.parse(response.body).must_be_kind_of Hash
        JSON.parse(response.body)['objects'].must_be_kind_of Array
      end
      
      it "must provide user attributes" do
        get :index, format: :json, school_id: @school, course_id: @course
        product = JSON.parse(response.body)['objects'].first.with_indifferent_access
        product.values_at(:id, :name, :description, :credit_value, :price).all?.must_equal true
      end
      
      it "must return course_ids" do
        get :index, format: :json, school_id: @school, course_id: @course
        product = JSON.parse(response.body)['objects'].first.with_indifferent_access
        product['course_ids'].must_be_kind_of Array
      end
    end
  end

  describe "product scope" do
    describe "#index" do
      it "must succeed as admin" do
        @user.update_attributes!(role: Role.admin)
        get :index, format: :json, school_id: @school
        assert_response :success
      end
      
      it "must succeed as faculty" do
        @user.update_attributes!(role: Role.faculty)
        get :index, format: :json, school_id: @school
        assert_response :success
      end
      
      it "must return valid JSON" do
        @user.update_attributes!(role: Role.admin)
        get :index, format: :json, school_id: @school
        JSON.parse(response.body).must_be_kind_of Hash
        JSON.parse(response.body)['objects'].must_be_kind_of Array
      end
      
      it "must provide user attributes" do
        @user.update_attributes!(role: Role.admin)
        get :index, format: :json, school_id: @school
        product = JSON.parse(response.body)['objects'].first.with_indifferent_access
        product.values_at(:id, :name, :description, :credit_value, :price).all?.must_equal true
      end
    end
  end
  
  describe "#show" do
    it "must succeed" do
      get :show, format: :json, school_id: @school, id: @product
      assert_response :success
    end
    
    it "must render template" do
      get :show, format: :json, school_id: @school, id: @product
      assert_template 'show'
    end
  end
  
  describe "#create" do
    it "must create product" do
      post :create, format: :json, school_id: @school, product: FactoryGirl.attributes_for(:product)
      assert_response :created
    end
    
    it "must create product entry" do
      post :create, format: :json, school_id: @school, product: FactoryGirl.attributes_for(:product)
      assigns(:product).persisted?.must_equal true
    end
    
    it "must create course_ids" do
      attributes = FactoryGirl.attributes_for(:product).merge!(course_ids: [ 1, 2 ])
      post :create, format: :json, school_id: @school, product: attributes
      assigns(:product).course_ids.must_equal [ 1, 2 ]
    end
    
    it "must return errors" do
      post :create, format: :json, school_id: @school, product: { name: nil }
      assert_response :unprocessable_entity
    end
  end
  
  describe "#update" do
    it "must create product" do
      put :update, format: :json, school_id: @school, id: @product, product: FactoryGirl.attributes_for(:product)
      assert_response :no_content
    end
    
    it "must return errors" do
      put :update, format: :json, school_id: @school, id: @product, product: { name: nil }
      assert_response :unprocessable_entity
    end
  end
  
  describe "#update" do
    it "must delete product" do
      delete :destroy, format: :json, school_id: @school, id: @product
      assert_response :no_content
    end
    
    it "must delete product instance" do
      delete :destroy, format: :json, school_id: @school, id: @product
      proc { @product.reload }.must_raise ActiveRecord::RecordNotFound
    end
  end
end
