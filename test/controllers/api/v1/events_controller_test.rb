require "test_helper"

describe Api::V1::EventsController do
  before do
    @course = FactoryGirl.create(:course)
    @school = @course.school
    @affiliation = FactoryGirl.create(:affiliation, school: @school, role: Role.faculty)
    @user = @affiliation.user
    
    @user.update_attributes!(role: Role.admin)
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "#index" do
    before do
      FactoryGirl.create_list(:event, 2, :daily, course: @course)
      FactoryGirl.create_list(:event, 2, :weekly, course: @course)
      FactoryGirl.create_list(:event, 4, :monthly, course: @course)
    end
    
    it "must succeed" do
      get :index, format: :json, school_id: @school, start: Date.new(2013, 9, 1), end: Date.new(2013, 9, 30)
      assert_response :success
    end
    
    it "must return valid JSON" do
      get :index, format: :json, school_id: @school, start: Date.new(2013, 9, 1), end: Date.new(2013, 9, 30)
      JSON.parse(response.body).must_be_kind_of Hash
      JSON.parse(response.body)['objects'].must_be_kind_of Array
    end
    
    it "must require valid start and end dates" do
      get :index, format: :json, school_id: @school, start: 0, end: 0
      assert_response :bad_request
    end
    
    it "must require valid end date" do
      get :index, format: :json, school_id: @school, start: Date.today, end: 0
      assert_response :bad_request
    end
    
    it "must require valid start date" do
      get :index, format: :json, school_id: @school, start: 0, end: Date.today
      assert_response :bad_request
    end
        
    it "must provide default dates when none are given" do
      get :index, format: :json, school_id: @school
      assert_response :success
    end
    
    it "must provide default date when end date not given" do
      get :index, format: :json, school_id: @school, start: Date.today
      assert_response :success
    end
    
    it "must select excluded events" do
      event = FactoryGirl.create(:event, :daily, :exclusion, course: @course)
      date = event.exclusions.first.exclude_on
      get :index, format: :json, school_id: @school, start: date - 5, end: date + 5
      JSON.parse(response.body)['objects'].any? { |item| item['excluded'] == true }.must_equal true
    end
    
    
  end
  
  describe "#show" do
    before do
      @event = FactoryGirl.create(:event, course: @course)
    end
    
    it "must succeed" do
      get :show, school_id: @school, id: @event, start: @event.starts_on, format: :json
      assert_response :success
    end
    
    it "must require a valid start date" do
      get :show, school_id: @school, id: @event, start: 0, format: :json
      assert_response :bad_request
    end
    
    it "must accept start date as compound ID" do
      get :show, school_id: @school, id: Event::Proxy.new(@event, @event.starts_on), format: :json
      assert_response :success
    end
    
    it "must accept ID" do
      get :show, school_id: @school, id: @event, format: :json
      assert_response :success
    end
  end
  
  describe "#create" do
    before do
      @event = FactoryGirl.create(:event, course: @course)
    end
    
    let(:attributes) { FactoryGirl.attributes_for(:event).merge!(instructor_id: @user.id) }
    
    it "must succeed" do
      post :create, format: :json, school_id: @course.school, course_id: @course, event: attributes
      assert_response :created
    end
    
    it "must create new event record" do
      post :create, format: :json, school_id: @course.school, course_id: @course, event: attributes
      Event.find(assigns(:event).id).must_be_kind_of Event      
    end
    
    it "must return JSON" do
      post :create, format: :json, school_id: @course.school, course_id: @course, event: attributes
      JSON.parse(response.body)['name'].wont_be :empty?
    end
    
    it "must fail if input is invalid" do
      post :create, format: :json, school_id: @course.school, course_id: @course, event: { name: 'Event 1' }
      assert_response :unprocessable_entity
    end
    
    it "must not be creatable by students" do
      @user.update_attributes!(role: Role.student)
      post :create, format: :json, school_id: @course.school, course_id: @course, event: attributes
      assert_response :forbidden
    end
    
    it "must return the instructor" do
      post :create, format: :json, school_id: @school, course_id: @course, event: attributes
      JSON.parse(response.body)['instructor_id'].wont_be :blank?
    end
    
    # it "must fail if instructor is not faculty" do
    #   user = FactoryGirl.create(:affiliation, school: @school).user
    #   post :create, format: :json, school_id: @school, course_id: @course, event: attributes.merge!(instructor_id: user.id)
    #   assert_response :unprocessable_entity
    # end    
  end
  
  describe "#update" do
    before do
      @event = FactoryGirl.create(:event, course: @course)
    end
    
    it "must succeed" do
      put :update, format: :json, school_id: @course.school, id: @event, event: { name: 'Updated Event Name'}
      assert_response :no_content
    end
    
    it "must update event" do
      time = Time.mktime(2014, 9, 1)
      put :update, format: :json, school_id: @course.school, id: @event, event: { starts_at: time }
      assigns(:event).starts_at.must_equal time
    end
    
    it "must fail if input is invalid" do
      put :update, format: :json, school_id: @course.school, id: @event, event: { starts_at: nil }
      assert_response :unprocessable_entity
    end
    
    it "must not be updatable by students" do
      @user.update_attributes!(role: Role.student)
      put :update, format: :json, school_id: @course.school, id: @event, event: { name: 'Updated Event Name'}
      assert_response :forbidden
    end
  end
  
  describe "#destroy" do
    before do
      @event = FactoryGirl.create(:event, course: @course)
    end
    
    it "must succeed" do
      delete :destroy, school_id: @school, id: @event, format: :json
      assert_response :no_content
    end
    
    it "must destroy the record" do
      delete :destroy, school_id: @school, id: @event, format: :json
      proc { @event.reload }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must fail as student" do
      @user.update_attributes!(role: Role.student)
      delete :destroy, school_id: @school, id: @event, format: :json
      assert_response :forbidden
    end
  end
end
