require "test_helper"

describe Api::V1::AlertsController do
  before do
    @user = FactoryGirl.create(:user, :john, role: Role.admin)    
    @order = FactoryGirl.create(:order, :with_items)
    @product = @order.order_item.product
    @school = @product.school
    @alerts = Alert.new(@school)
    
    @controller.stubs(:current_user).returns(@user)    
  end
  
  after do
    # Work around transactional fixtures not applying to objects created in before block
    School.destroy_all
    User.destroy_all
  end
  
  describe "#index" do
    it "must succeed" do
      get :index, format: :json, school_id: @school, max_age: 1.day.ago
      assert_response :success
    end
    
    it "must return valid JSON" do
      get :index, format: :json, school_id: @school, max_age: 1.day.ago
      JSON.parse(response.body).must_be_kind_of Array
    end
    
    it "must provide time and message" do
      get :index, format: :json, school_id: @school, max_age: 1.day.ago
      time, message = JSON.parse(response.body).first      
      Time.parse(time).must_be_kind_of Time
      message.must_match %r{purchased Ten Pack}
    end
  end
end
