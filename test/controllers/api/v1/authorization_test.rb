require "test_helper"

class AuthorizationController < Api::V1::ApiController
  authorize :faculty
  
  def index
    head :ok
  end
end

describe AuthorizationController do
  setup do
    Viewcy::Application.routes.draw do
      resources :authorization
    end
  end
  
  teardown do
    load Rails.root.join('config/routes.rb')
  end
  
  before do
    @affiliation = FactoryGirl.create(:affiliation)
    @user = @affiliation.user
    @school = @affiliation.school
  end

  it "wont succeed without a session" do
    get :index, school_id: @school, format: :json
    assert_response :forbidden
  end
    
  it "wont succeed by default" do
    get :index, { school_id: @school, format: :json }, user_id: @user.id
    assert_response :forbidden
  end
  
  it "must succeed as admin" do
    @user.update_attributes!(role: Role.admin)
    get :index, { school_id: @school, format: :json }, user_id: @user.id
    assert_response :success
  end
  
  it "must succeed as owner" do
    @affiliation.update_attributes!(role: Role.owner)
    get :index, { school_id: @school, format: :json }, user_id: @user.id
    assert_response :success
  end
  
  it "must succeed as faculty" do
    @affiliation.update_attributes!(role: Role.faculty)
    get :index, { school_id: @school, format: :json }, user_id: @user.id
    assert_response :success
  end
  
  it "wont succeed as student" do
    @affiliation.update_attributes!(role: Role.student)
    get :index, { school_id: @school, format: :json }, user_id: @user.id
    assert_response :forbidden
  end
end