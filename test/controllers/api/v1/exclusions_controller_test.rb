require "test_helper"

describe Api::V1::ExclusionsController do
  before do
    @course = FactoryGirl.create(:course)
    @school = @course.school
    @event = FactoryGirl.create(:event, :daily, course: @course)
    @proxy = Event::Proxy.new(@event, @event.starts_on)
    
    @user = FactoryGirl.create(:user, :john, :admin)
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "#create" do
    it "must succeed" do
      post :create, format: :json, school_id: @school, event_id: @proxy
      assert_response :created
    end
    
    it "must create the exclusion record" do
      post :create, format: :json, school_id: @school, event_id: @proxy
      @event.reload.exclusions.wont_be :empty?
    end
  end
  
  describe "#destroy" do
    before do
      @event.exclusions.create!(exclude_on: @event.starts_on)
    end
    
    it "must succeed" do
      delete :destroy, format: :json, school_id: @school, event_id: @proxy
      assert_response :no_content
    end
    
    it "must remove the exclusion record" do
      delete :destroy, format: :json, school_id: @school, event_id: @proxy
      @event.reload.exclusions.must_be :empty?
    end
  end
end
