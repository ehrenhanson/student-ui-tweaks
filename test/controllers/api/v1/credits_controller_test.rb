require 'test_helper'

describe Api::V1::CreditsController do
  before do
    @user = FactoryGirl.create(:user, :john, :sequenced, :with_credit_for_multiple_courses)
    @credit = @user.credits.first
    @school = @credit.product.school
    
    @admin = FactoryGirl.create(:user, :john, :sequenced, :admin)
    @controller.stubs(:current_user).returns(@admin)
  end
  
  describe "#index" do
    it "must succeed" do
      get :index, format: :json, school_id: @school, user_id: @user
      assert_response :success
    end
    
    it "must not return users not affiliated with the school" do
      proc {
        get :index, format: :json, school_id: @school, user_id: @admin
      }.must_raise ActiveRecord::RecordNotFound
    end
    
    it "must return credits" do
      get :index, format: :json, school_id: @school, user_id: @user
      JSON.parse(response.body)['objects'].must_be :present?
    end
  end
  
  describe "#update" do
    it "must succeed" do
      put :update, format: :json, school_id: @school, user_id: @user, id: @credit, credit: { value: 99 }
      assert_response :success
    end
    
    it "must update value" do
      put :update, format: :json, school_id: @school, user_id: @user, id: @credit, credit: { value: 99 }
      @credit.reload.value.must_equal 99
    end
    
    it "must return error" do
      put :update, format: :json, school_id: @school, user_id: @user, id: @credit, credit: { value: nil }
      assert_response :unprocessable_entity
    end
  end
end
