require "test_helper"

describe Api::V1::SchoolAttachmentsController do
  before do
    @user = FactoryGirl.create(:user, :john, :admin)
    @school = FactoryGirl.create(:school, owner: @user)
    @controller.stubs(:current_user).returns(@user)
  end
  
  let(:attachment) { fixture_file_upload('landscape.jpg', 'image/jpeg') }
  
  describe "#create" do
    it "must upload header image" do
      post :create, school_id: @school, header_image: attachment
      assert_response :success
    end
    
    it "must return header image URL" do
      post :create, school_id: @school, header_image: attachment
      JSON.parse(@response.body)['header_image_url'].wont_match %r{missing}
      JSON.parse(@response.body)['logo_image_url'].must_match %r{missing}
    end
    
    it "must upload logo image" do
      post :create, school_id: @school, logo_image: attachment
      assert_response :success
    end
    
    it "must return logo image URL" do
      post :create, school_id: @school, logo_image: attachment
      JSON.parse(@response.body)['logo_image_url'].wont_match %r{missing}
      JSON.parse(@response.body)['header_image_url'].must_match %r{missing}      
    end
    
    it "must upload header and logo image" do
      post :create, school_id: @school, header_image: attachment, logo_image: attachment
      assert_response :success
    end
    
    it "must return header and logo image" do
      post :create, school_id: @school, header_image: attachment, logo_image: attachment
      JSON.parse(@response.body)['logo_image_url'].wont_match %r{missing}
      JSON.parse(@response.body)['header_image_url'].wont_match %r{missing}      
    end
  end
  
  describe "#destroy" do
    before do
      @school.update_attributes!(logo_image: attachment, header_image: attachment)
    end
    
    it "must remove attachment" do
      delete :destroy, school_id: @school, id: 'logo_image'
      assert_response :success
    end
    
    it "must remove attachment from model" do
      delete :destroy, school_id: @school, id: 'logo_image'
      @school.reload.logo_image.must_be :blank?
    end
    
    it "must not remove other attachments" do
      delete :destroy, school_id: @school, id: 'logo_image'
      @school.reload.header_image.wont_be :blank?
    end
    
    it "must not allow removal of undefined attachments" do
      delete :destroy, school_id: @school, id: 'name'
      @school.reload.name.wont_be :blank?
    end
  end
end
