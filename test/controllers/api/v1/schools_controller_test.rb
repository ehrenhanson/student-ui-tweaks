require "test_helper"

describe Api::V1::SchoolsController do
  before do
    School.destroy_all    
    @affiliation = FactoryGirl.create(:affiliation)
    @school = @affiliation.school
    @user = @affiliation.user
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "routes" do
    it "must generate member route" do
      api_v1_school_path(@school).must_equal "/api/v1/schools/#{@school.to_param}"
    end
  end
  
  describe "#index" do
    describe "as json" do
      it "must succeed" do
        get :index, format: :json
        assert_response :success
      end
      
      it "must return valid JSON" do
        get :index, format: :json
        JSON.parse(response.body).must_be_kind_of Hash
        JSON.parse(response.body)['objects'].must_be_kind_of Array
      end
      
      it "must provide school attributes" do
        get :index, format: :json
        school = JSON.parse(response.body)['objects'].first.with_indifferent_access
        school.values_at(:id, :name, :description, :role).all?.must_equal true
      end
      
      it "must return role as a string value" do
        get :index, format: :json
        school = JSON.parse(response.body)['objects'].first.with_indifferent_access
        school[:role].must_equal 'student'
      end
    
      describe "scoping" do
        before do
          FactoryGirl.create_list(:affiliation, 4, user: @user)
          FactoryGirl.create_list(:affiliation, 5)
        end
      
        it "must return scoped schools for user" do
          get :index, format: :json
          assigns(:schools).size.must_equal 5
        end
      
        it "must return all schools for admin" do
          @user.update_attributes!(role: Role.admin)
          get :index, format: :json
          assigns(:schools).size.must_equal 10
        end
      end
    end
  end
  
  describe "#show" do
    describe "as json" do
      it "must succeed" do
        get :show, format: :json, id: @school
        assert_response :success
      end
      
      it "must return valid JSON" do
        get :show, format: :json, id: @school
        JSON.parse(response.body).must_be_kind_of Hash
      end
      
      it "must provide school attributes" do
        get :show, format: :json, id: @school
        school = JSON.parse(response.body).with_indifferent_access
        school.values_at(:id, :name, :description, :role).all?.must_equal true
      end
      
      describe "scoping" do
        before do
          @nonaffiliation = FactoryGirl.create(:affiliation)
        end
        
        it "must not find an unscoped school as a user" do
          proc { get :show, format: :json, id: @nonaffiliation.school }.must_raise ActiveRecord::RecordNotFound
        end
        
        it "must find an unscoped school as an admin" do
          @user.update_attributes!(role: Role.admin)
          get :show, format: :json, id: @nonaffiliation.school
          assert_response :success
        end
      end
    end
  end
  
  describe "#create" do
    describe "as admin" do
      before do
        @user.update_attributes!(role: Role.admin)
      end
      
      let(:attributes) do
        attributes = FactoryGirl.attributes_for(:school)
        attributes[:address_attributes] = FactoryGirl.attributes_for(:address)
        attributes
      end
      
      it "must create a new school" do
        post :create, format: :json, school: attributes
        assert_response :created
        assigns(:school).new_record?.must_equal false
      end
    
      it "must render the show template" do
        post :create, format: :json, school: attributes
        assert_template 'show'
      end
    
      it "must return JSON" do
        post :create, format: :json, school: attributes
        JSON.parse(response.body)['name'].wont_be :blank?
      end
      
      it "must assign location header" do
        post :create, format: :json, school: attributes
        response['Location'].must_equal api_v1_school_url(assigns(:school))
      end
      
      it "must handle validation errors" do
        post :create, format: :json, school: { name: '' }
        assert_response :unprocessable_entity
        JSON.parse(response.body).has_key?('name').must_equal true
      end
    end
    
    describe "as user" do
      it "must not be able to create a new school" do
        post :create, format: :json, school: FactoryGirl.attributes_for(:school)
        assert_response :forbidden
      end
    end
  end
  
  describe "#update" do
    describe "as admin" do
      before do
        @user.update_attributes!(role: Role.admin)
      end
      
      it "must update school" do
        put :update, format: :json, id: @school, school: { name: 'New School' }
        assert_response :no_content
        @school.reload.name.must_equal 'New School'
      end
    
      it "must handle validation errors" do
        put :update, format: :json, id: @school, school: { name: '' }
        assert_response :unprocessable_entity
        JSON.parse(response.body).has_key?('name').must_equal true
      end
    end
    
    describe "as owner" do
      before do
        @user.update_attributes!(role: Role.owner)
      end
      
      it "must update school" do
        put :update, format: :json, id: @school, school: { name: 'New School' }
        assert_response :no_content
        @school.reload.name.must_equal 'New School'
      end
    
      it "must handle validation errors" do
        put :update, format: :json, id: @school, school: { name: '' }
        assert_response :unprocessable_entity
        JSON.parse(response.body).has_key?('name').must_equal true
      end
      
      it "must use id parameter to build school_identifier" do
        put :update, format: :json, id: @school, school: { name: 'New School' }
        @controller.send(:school_identifier).must_equal @school.to_param
      end
    end
    
    describe "as user" do
      it "must not be able to create a new school" do
        put :update, format: :json, id: @school, school: FactoryGirl.attributes_for(:school)
        assert_response :forbidden
      end
    end
  end
end
