require "test_helper"

describe Api::V1::AffiliationsController do
  before do
    @user = FactoryGirl.create(:user, :john)
    @affiliation = FactoryGirl.create(:affiliation, user: @user)
    @school = @affiliation.school
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "#active" do
    before do
      @affiliations = FactoryGirl.create_list(:affiliation, 5, user: @user)
    end
    
    it "must succeed" do
      post :active, school_id: @school, format: :json
      assert_response :no_content
      @user.active_school_id.must_equal @school.id
    end
    
    it "must require a user" do
      @controller.unstub(:current_user)
      post :active, school_id: @school, format: :json
      assert_response :forbidden
    end
  end
end
