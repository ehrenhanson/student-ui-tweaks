require "test_helper"

describe Api::V1::AppearancesController do
  before do
    @appearance = FactoryGirl.create(:appearance)
    @course = @appearance.course
    @user = @course.instructors.first
    @affiliation = FactoryGirl.create(:affiliation, school: @course.school, user: @user)
    @controller.stubs(:current_user).returns(@user)
  end

  describe "#index" do
    it "must succeed as admin" do
      @user.update_attributes!(role: Role.admin)
      get :index, format: :json, school_id: @course.school, course_id: @course
      assert_response :success
    end
    
    it "must succeed as faculty" do
      @affiliation.update_attributes!(role: Role.faculty)
      get :index, format: :json, school_id: @course.school, course_id: @course
      assert_response :success
    end
  
    it "must fail as student" do
      @affiliation.update_attributes!(role: Role.student)
      proc { get :index, format: :json, school_id: @course.school, course_id: @course }.must_raise ActiveRecord::RecordNotFound
    end
  end
  
  describe "#show" do
    it "must succeed as admin" do
      @user.update_attributes!(role: Role.admin)
      get :show, format: :json, school_id: @course.school, course_id: @course, id: @appearance
      assert_response :success
    end
    
    it "must succeed as faculty" do
      @affiliation.update_attributes!(role: Role.faculty)
      get :show, format: :json, school_id: @course.school, course_id: @course, id: @appearance
      assert_response :success
    end
  
    it "must fail as student" do
      @affiliation.update_attributes!(role: Role.student)
      proc { get :show, format: :json, school_id: @course.school, course_id: @course, id: @appearance }.must_raise ActiveRecord::RecordNotFound
    end
  end
end
