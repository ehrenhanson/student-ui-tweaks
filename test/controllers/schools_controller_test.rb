require "test_helper"

describe SchoolsController do
  before do
    @school = FactoryGirl.create(:school)
    
    setup_school_context(@school)
  end
  
  describe "#show" do
    it "must succeed" do      
      get :show
      assert_response :success
    end
  end
end
