require "test_helper"

describe SessionsController do
  describe "#show" do
    before do
      @user = FactoryGirl.create(:user, :john)
    end
    
    it "must succeed" do
      @controller.expects(:current_user).returns(@user)
      get :show, format: :json
      assert_response :success
      assert_template 'create'
    end
    
    it "must be not authorized when not logged in" do
      get :show, format: :json
      assert_response :forbidden
    end
    
    it "must create a session from the remember cookie" do
      cookies.permanent.signed[:remember] = [ @user.id, @user.login_token ]
      get :show, format: :json
      assert_response :success
    end
    
    it "must not create a session from an invalid remember token" do
      cookies.permanent.signed[:remember] = [ @user.id, 'not_a_valid_token' ]
      get :show, format: :json
      assert_response :forbidden
    end
  end
  
  describe "#new" do
    it "must succeed" do
      get :new
      assert_response :success
      assert_template 'new'
    end
  end
  
  describe "#create" do
    describe "using twitter" do
      before do
        request.env['omniauth.auth'] = OmniAuth.config.add_mock(:twitter)
      end
      
      it "must succeed" do
        post :create, provider: 'twitter'
        assert_redirected_to '/'
      end
    end
    
    describe "using facebook" do
      before do
        request.env['omniauth.auth'] = OmniAuth.config.add_mock(:facebook)
      end
      
      it "must succeed" do
        post :create, provider: 'facebook'
        assert_redirected_to '/'
      end
    end
    
    describe "using password" do
      before do
        @user = FactoryGirl.create(:user, :john)
      end
      
      it "must succeed" do
        post :create, email: @user.email, password: @user.password
        assert_redirected_to '/'
      end
      
      it "must fail" do
        post :create, email: 'notvalid', password: @user.password
        assert_redirected_to new_session_url
      end
      
      it "must assign remember cookie" do
        post :create, email: @user.email, password: @user.password
        cookies.signed[:remember].must_equal [ @user.id, @user.login_token ]
      end
    end
    
    describe "using password with JSON" do
      before do
        @user = FactoryGirl.create(:user, :john)
        FactoryGirl.create_list(:affiliation, 5, user: @user)
      end
      
      it "must succeed" do
        post :create, email: @user.email, password: @user.password, format: :json
        assert_response :success
      end
      
      it "must fail" do
        post :create, email: 'notvalid', password: @user.password, format: :json
        assert_response :forbidden
      end
      
      it "must return valid JSON" do
        post :create, email: @user.email, password: @user.password, format: :json
        json = JSON.parse(response.body)

        json['user'].must_be_kind_of Hash
        json['user']['email'].must_equal @user.email
        json['schools'].must_be_kind_of Array
        json['schools'].size.must_equal 5
      end
    end
  end
  
  describe "#failure" do
    it "must redirect home" do
      get :failure
      assert_redirected_to new_session_url
    end
  end
  
  describe "#destroy" do
    before do
      @user = FactoryGirl.create(:user, :john)
    end
    
    it "must redirect home" do
      get :destroy, {}, user_id: @user.id
      assert_redirected_to school_root_url
    end
    
    it "must destroy the user session" do
      get :destroy, {}, user_id: @user.id
      session[:user_id].must_be :blank?
    end
    
    it "must remove the remember cookie" do
      token = @user.login_token
      get :destroy, {}, user_id: @user.id
      @user.reload.login_token.wont_equal token
    end
  end
end