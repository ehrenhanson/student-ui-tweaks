require "test_helper"

class PaginatedController < ApplicationController
end

describe PaginatedController do
  def params(args)
    @controller.request.expects(:parameters).returns(args)
  end
  
  before do
    @controller = PaginatedController.new
    @controller.request = stub(parameters: {})
  end
  
  it "must define a default limit" do
    @controller.limit.must_equal Pagination::DEFAULT_LIMIT
  end
  
  it "must accept an alternative limit" do
    params(limit: '5')
    @controller.limit.must_equal 5
  end
  
  it "must default to page 1" do
    @controller.page.must_equal 1
  end
  
  it "must use input for page 2" do
    params(page: '2')
    @controller.page.must_equal 2
  end
  
  it "must have a zero offset on page 1" do
    params(page: '1')
    @controller.offset.must_equal 0
  end
  
  it "must have a DEFAULT_LIMIT offset on page 2" do
    params(page: '2')
    @controller.offset.must_equal Pagination::DEFAULT_LIMIT
  end
  
  it "must have a DEFAULT_LIMIT*2 offset on page 3" do
    params(page: '3')
    @controller.offset.must_equal Pagination::DEFAULT_LIMIT * 2
  end
  
  it "must have a 10 offset on page 3" do
    params(page: '3', limit: '5')
    @controller.offset.must_equal 10
  end
end
