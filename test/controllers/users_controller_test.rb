require "test_helper"

describe UsersController do
  before do
    @school = FactoryGirl.create(:school)
    setup_school_context(@school)
  end
  
  describe "#create" do
    it "must succeed" do
      post :create, format: :json, user: FactoryGirl.attributes_for(:user, :john)
      assert_response :created
    end
    
    it "must fail on validation error" do
      post :create, format: :json, user: { name: 'Joe' }
      assert_response :unprocessable_entity
    end
    
    it "must create a new user" do
      post :create, format: :json, user: FactoryGirl.attributes_for(:user, :john)
      User.first.role.must_equal Role.user
    end
    
    it "must not allow the creation of an admin" do
      post :create, format: :json, user: FactoryGirl.attributes_for(:user, :john, role: Role.admin)
      User.first.role.must_equal Role.user
    end
    
    it "must sign the user in" do
      post :create, format: :json, user: FactoryGirl.attributes_for(:user, :john, role: Role.admin)
      @controller.session[:user_id].must_be :present?
    end
  end
end
