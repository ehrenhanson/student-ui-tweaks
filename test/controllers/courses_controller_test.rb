require "test_helper"

describe CoursesController do
  before do
    @school = FactoryGirl.create(:school, :courses)
    @course = @school.courses.first
    
    setup_school_context(@school)
  end

  describe "#index" do
    it "must succeed" do
      get :index, format: :json
      assert_response :success      
    end
    
    it "must return JSON" do
      get :index, format: :json
      JSON.parse(response.body).must_be_kind_of Array
    end
  end
  
  describe "#show" do
    it "must succeed" do
      get :show, id: @course
      assert_response :success      
    end
    
    it "must be an instructor" do 
      course = FactoryGirl.create(:course)     
      proc { get :show, id: course }.must_raise(ActiveRecord::RecordNotFound)
    end
  end
end
