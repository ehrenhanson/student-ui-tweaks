require "test_helper"

describe SchoolRegistrationsController do
  before do
    @user = FactoryGirl.create(:user, :john)
    @controller.stubs(:current_user).returns(@user)
  end
  
  describe "#new" do
    it "must succeed" do
      get :new
      assert_response :success
    end
  end
  
  describe "#create" do
    def MockProcessor(successful)
      stub(create: stub(success?: successful, message: 'An error has occurred'))
    end
    
    let(:school_attributes) { FactoryGirl.attributes_for(:school, name: 'test').merge!(address_attributes: FactoryGirl.attributes_for(:address)) }
    let(:user_attributes) {{ billing_address_attributes: FactoryGirl.attributes_for(:address) }}
    let(:school_registration_attributes) {{
      school_attributes: school_attributes,
      user_attributes: user_attributes,
      ssn: '123-321-201-100',
      tax_id: '10220121-210',
      account_number: '123223',
      routing_number: '1202'
    }}
    
    it "must create a new affiliation and school" do
      SchoolRegistration.merchant_processor = MockProcessor(true)
      post :create, school_registration: school_registration_attributes
      assert_response :redirect
    end
    
    it "must redirect to the new school" do
      SchoolRegistration.merchant_processor = MockProcessor(true)
      post :create, school_registration: school_registration_attributes
      assert_redirected_to school_root_url(subdomain: 'test')
    end
    
    it "must present the form again if validation fails" do
      SchoolRegistration.merchant_processor = MockProcessor(true)
      post :create, school_registration: { school_attributes: {}, user_attributes: {} }
      assert_template 'new'
    end
    
    it "must present the form again if Braintree registration fails" do
      SchoolRegistration.merchant_processor = MockProcessor(false)
      post :create, school_registration: school_registration_attributes
      assert_template 'new'
    end
  end
end
