require "test_helper"

describe RegistrationsController do
  before do
    @user = FactoryGirl.create(:user, :john)
    @event = FactoryGirl.create(:event, course: FactoryGirl.build(:course))
    @registration = FactoryGirl.create(:registration, event: @event, user: @user)

    @controller.stubs(:current_user).returns(@user)    
    setup_school_context(@event.school)
  end
  
  describe "#new" do
    it "must setup registration state data" do
      placement = FactoryGirl.create(:placement, course: @event.course)
      credit = FactoryGirl.create_list(:credit, 5, user: @user, product: placement.product)
      
      xhr :get, :new, event_id: @event, start: @event.starts_on, format: :json
      assert_response :success
    end
    
    it "must notify unauthorized users" do
      @controller.stubs(:current_user).returns(nil)
      xhr :get, :new, event_id: @event, start: @event.starts_on, format: :json
      assert_response :forbidden
    end
  end
  
  describe "#create" do
    it "must succeed" do
      post :create, event_id: @event.to_param, address: FactoryGirl.attributes_for(:address), format: :json
      assert_response :success
    end

    it "must update address" do
      post :create, event_id: @event.to_param, address: FactoryGirl.attributes_for(:address, address: "New Test Address"), format: :json
      address = @user.billing_address.reload
      address.address.must_equal 'New Test Address'
    end
  end
  
  describe "#destroy" do
    it "must delete the registration record" do
      post :destroy, event_id: @event, start: @event.starts_on

      assert_response :success
      Registration.where(event: @event, starts_on: @event.starts_on).none?.must_equal true
    end
  end
end
