require "test_helper"

describe DateHelper do
  let(:date) { Date.new(2013, 11, 12) }
  let(:datetime) { Time.utc(2013, 11, 12, 1, 20) }
  
  describe "#format_date_header" do
    before do
      Date.stubs(:today).returns(date)
    end
  
    it "must format date header today" do
      format_date_header(date).must_equal 'Today <span class="date">11/12</span>'
    end

    it "must format date header tomorrow" do
      format_date_header(date + 1).must_equal 'Tomorrow <span class="date">11/13</span>'
    end
  
    it "must format date header next week" do
      format_date_header(date + 7).must_equal 'Tuesday <span class="date">11/19</span>'
    end
  end
  
  it "must return the event date with time range" do
    format_date_and_time_range(datetime, datetime + 1.hour).must_equal 'Nov. 12, 2013  1:20 AM -  2:20 AM'
  end

  it "must return the time range" do
    format_time_range(datetime, datetime + 1.hour).must_equal ' 1:20 AM -  2:20 AM'
  end

  it "must return the formatted time" do
    format_time(datetime).must_equal ' 1:20 AM'
  end
end
