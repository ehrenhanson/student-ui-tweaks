require "test_helper"

describe ApplicationHelper do
  describe "title" do
    it "must return title without assignment" do
      title.must_equal 'Viewcy'
    end
  
    it "must return title with instance vairable assignment" do
      @title = 'Title'
      title.must_equal 'Title | Viewcy'
    end
    
    it "must return title with method assignment" do
      title 'Title'
      title.must_equal 'Title | Viewcy'
    end
  end
end
