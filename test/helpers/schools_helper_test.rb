require "test_helper"

describe SchoolsHelper do
  describe "#part" do
    it "must return one part when only first argument is present" do
      part([1], []).must_match %r{<div.*col one}
    end
  
    it "must return left part on first call when both arguments are present" do
      part([1], [2]).must_match %r{<div.*col left}
    end
  
    it "must return right part on subsequent call when both arguments are present" do
      part([1], [2])
      part([2], [1]).must_match %r{<div.*col right}
    end
  
    it "must take multiple dependency arguments and accept any present" do
      part([1], nil, [], [1]).must_match %r{<div.*col left}
    end
    
    it "must not create the part if the collection is not present" do
      part([], [1]).must_equal nil
    end
  end
end
