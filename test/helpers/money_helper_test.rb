require "test_helper"

describe MoneyHelper do
  let(:value) { BigDecimal('10.55') }
  
  it "must format price" do
    format_price(value).must_equal '$10.55'
  end
  
  it "must handle nil prices" do
    format_price(nil).must_equal nil
  end
end
