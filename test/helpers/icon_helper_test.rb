require "test_helper"

describe IconHelper do
  it "must turn a symbol into an icon span" do
    icon(:email).must_equal(%(<span class="icons">a</span>))
  end
  
  it "must turn a string into an icon span" do
    icon('email').must_equal(%(<span class="icons">a</span>))
  end
  
  it "must be blank if the icon is not defined" do
    icon(:widget).must_equal(nil)
  end
  
  it "must return an icon for every defined icon" do
    IconHelper::ICON_NAMES.each do |name|
      icon(name).wont_equal(nil)
    end
  end
  
  it "must generate button with icon" do
    icon_button(:submit).must_equal %(<button name="button" type="submit"><span class="icons">r</span></button>)
  end
end
