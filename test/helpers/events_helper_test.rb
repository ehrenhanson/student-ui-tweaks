require "test_helper"

describe EventsHelper do
  it "must format date without timezone" do
    local_time(Time.new(2014, 9, 1, 13, 0)).must_equal '2014-09-01T13:00:00'
  end
  
  it "must handle nil dates" do
    local_time(nil).must_equal nil
  end
end
