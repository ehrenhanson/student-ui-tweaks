Arel::Visitors::Visitor.class_eval do
  # Extend Visitor class to treat Role::Type as Fixnum, which is
  # the class Role::Type delegates
  def visit_Role_Type(*args)
    visit_Fixnum(*args)
  end
end