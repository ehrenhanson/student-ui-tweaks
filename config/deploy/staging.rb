set :rails_env, 'staging'

set :deploy_to, "/srv/#{fetch(:application)}"

set :rvm_type, :system
set :rvm_ruby_version, "1.9.3@#{fetch(:application)}"

server 'ruby-dev.evolvingmedia.org', user: 'cap', roles: %w{web app db}

 set :ssh_options, {
   user: 'cap',
   keys: File.expand_path('~')<<"/.ssh/id_rsa",
   forward_agent: true,
   auth_methods: %w(publickey)
 }
 
namespace :deploy do
  desc 'Runs rake db:reset to reset database'
  task :reset do
    on roles(:db), in: :groups, limit: 3, wait: 10 do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:reset"
          invoke "deploy:restart"
        end
      end
    end
  end
end