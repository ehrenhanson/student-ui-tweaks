Viewcy::Application.routes.draw do
  get '/auth/logout(.:format)', to: 'sessions#destroy', as: :destroy_session
  get '/auth/refresh(.:format)', to: 'sessions#show'
  get '/auth(/:provider)', to: 'sessions#new', as: :new_session
  post '/auth/login(.:format)', to: 'sessions#create', as: :create_session
  match '/auth/:provider/callback', to: 'sessions#create', via: [ :get, :post ]
  match '/auth/failure', to: 'sessions#failure', via: [ :get, :post ]
  
  namespace :api do
    namespace :v1 do
      resources :schools do
        get 'events(/between/:start(/:end))(.:format)', to: 'events#index', as: :events_between
        post 'affiliations/active', to: 'affiliations#active'
        
        resources :alerts, only: :index

        resources :users do
          resources :appearances
          resources :credits
          
          member do
            post :invite
          end
        end
        
        resources :courses do
          resources :products, only: :index
          resources :appearances
          resources :events, only: :create
        end
        
        resources :appearances
        resources :products

        resources :events, except: :create do
          resources :exclusions, only: :create
          resources :registrations, only: :index
          delete 'exclusions', to: 'exclusions#destroy'
        end
        
        resources :school_attachments, path: 'attachments'
      end

      resources :users, only: :show      
    end
  end

  constraints subdomain: /signup/ do
    resources :school_registrations, path: '', path_names: { new: '' }, only: [ :new, :create ]
  end
  
  constraints subdomain: /.+/ do
    get '/register/regions/:id', as: :registration_regions, to: 'registrations/regions#index'
    get '/register/:event_id/:start', as: :add_registration, to: 'registrations#new'
    post '/register', to: 'registrations#create'
    post '/unregister/:event_id/:start', as: :remove_registration, to: 'registrations#destroy'

    get '/checkout', to: 'orders#show', as: :order
    patch '/checkout', to: 'orders#update', as: :update_order
    scope '/events/:event_id/:start' do
      resources :order_items, shallow: true
    end

    resources :credits, only: :index
    resources :users, only: :create
    
    resources :courses, only: [ :index, :show ] do
      resources :events, only: :index
      namespace :events do
        resources :date_set, only: :index
      end
    end
    
    get '/', to: 'schools#show', as: :school_root
  end
  
  post '/braintree/accept', to: 'braintree#accept'
  resources :reset_passwords
  root to: 'pages#index'
end
