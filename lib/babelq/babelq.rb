require File.dirname(__FILE__) + '/babelq/abstract_adapter'
require File.dirname(__FILE__) + '/babelq/sqlite_adapter'
require File.dirname(__FILE__) + '/babelq/mysql2_adapter'

module Babelq
  def self.translate
    @@adapter_class ||= "Babelq::#{ActiveRecord::Base.connection.adapter_name}Adapter".constantize
    @@adapter_class.instance
  end
end
