module Babelq
  class SQLiteAdapter < AbstractAdapter
    def aliased_conditions
      :where
    end
    
    def greatest(*columns)
      "MAX(#{columns.join(', ')})"
    end
  end
end