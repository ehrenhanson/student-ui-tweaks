module Babelq
  class Mysql2Adapter < AbstractAdapter
    def aliased_conditions
      :having
    end
        
    def greatest(*columns)
      "GREATEST(#{columns.join(', ')})"
    end
  end
end