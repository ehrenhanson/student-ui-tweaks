module ERBJS
  module AssetsHelper
    def erb_template(path)
      path = Rails.root.join('app/views', path)
      %("""\n) + ERBJS::Transpiler.new(File.read(path)).call + %(\n"""\n)
    end
  end
end

Rails.application.assets.context_class.class_eval do
  include ERBJS::AssetsHelper
end

class Sprockets::DirectiveProcessor
  def process_depend_on_view_directive(file)
    path = Rails.root.join('app/views', file)
    context.depend_on(path)
  end
end