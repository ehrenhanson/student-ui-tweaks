require File.dirname(__FILE__) + '/../../../config/application'

module ERBJS
  class Transpiler
    def initialize(source)
      @scanner = ERB::Compiler::Scanner.make_scanner(source, '<>', 0)
      @block = false
    end
    
    def call
      state = :text
      result = ''
      
      @scanner.scan do |token|
        case token
        when Symbol
        when /^<%/
          state = :code
          result << token.sub('%=', '%-')
        when /^%>/
          state = :text
          result << token
        else
          case state
          when :text
            result << token
          when :code
            unless @block              
              token = token
                .sub(/\S+?\./, '')
                .sub(/\s*(.+)\.each\s+do\s+\|(.+)\|/, ' _.each(\1, function(\2) { ')
                .sub(/if[\s\(](.*)[\s\)]/, 'if (\1) { ')
                .sub('end', '}')

              @block = true if token =~ /\.each/
            else
              token = token.sub('end', '})')
              @block = false if token =~ /end/
            end
            
            result << token
          end
        end
      end
      
      result
    end
  end
end
