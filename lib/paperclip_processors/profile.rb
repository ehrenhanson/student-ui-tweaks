class Profile < Paperclip::Processor
  def initialize(file, options = {}, thumbnail = nil)
    super
    
    extension = File.extname(@file.path)
    @basename = File.basename(@file.path, extension)

    @format = options[:format] || extension
    @target_geometry = (options[:string_geometry_parser] || Paperclip::Geometry).parse(options[:geometry])
  end
  
  def make
    target = Tempfile.new([ @basename, ".#{@format}" ])
    target.binmode
    
    size = [ @target_geometry.width, @target_geometry.height ].min.floor
    radius = size / 2
    
    convert(%[-gravity center -scale #{size} :source :target],
      source: File.expand_path(@file.path),
      target: File.expand_path(target.path)
    )
    
    convert(%[-size #{size}x#{size} xc:none -fill :target -draw 'circle #{radius},#{radius} #{radius},1' :target],
      target: File.expand_path(target.path)
    )
    
    target
  end
end