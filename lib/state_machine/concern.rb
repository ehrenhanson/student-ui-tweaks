module StateMachine::Concern
  include ActiveSupport::Concern
  
  def state_machine(*args, &block)
    @_state_machine_block = Proc.new do
      state_machine(*args, &block)
    end
  end
  
  def append_features(base)
    result = super(base)
    base.class_eval(&@_state_machine_block) if result != false && @_state_machine_block
    result
  end
end