# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140808015320) do

  create_table "addresses", force: true do |t|
    t.integer  "addressable_id"
    t.string   "addressable_type"
    t.string   "address"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "country",          default: "US"
    t.string   "zipcode"
    t.string   "address_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "addresses", ["address_type"], name: "index_addresses_on_address_type"
  add_index "addresses", ["addressable_id"], name: "index_addresses_on_addressable_id"
  add_index "addresses", ["addressable_type"], name: "index_addresses_on_addressable_type"

  create_table "affiliations", force: true do |t|
    t.integer  "user_id"
    t.integer  "school_id"
    t.integer  "role",       default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",     default: false
  end

  add_index "affiliations", ["school_id", "user_id"], name: "index_affiliations_on_school_id_and_user_id", unique: true
  add_index "affiliations", ["school_id"], name: "index_affiliations_on_school_id"
  add_index "affiliations", ["user_id"], name: "index_affiliations_on_user_id"

  create_table "appearances", force: true do |t|
    t.integer  "course_id"
    t.integer  "affiliation_id"
    t.string   "name"
    t.string   "email"
    t.text     "notes"
    t.integer  "payment_status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "appearances", ["affiliation_id"], name: "index_appearances_on_affiliation_id"
  add_index "appearances", ["course_id"], name: "index_appearances_on_course_id"

  create_table "courses", force: true do |t|
    t.integer  "school_id"
    t.string   "name"
    t.text     "description"
    t.integer  "level",       default: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "courses", ["school_id"], name: "index_courses_on_school_id"

  create_table "credits", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.integer  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "unlimited",     default: false
    t.integer  "order_item_id"
  end

  add_index "credits", ["product_id"], name: "index_credits_on_product_id"
  add_index "credits", ["user_id"], name: "index_credits_on_user_id"

  create_table "events", force: true do |t|
    t.integer  "course_id"
    t.string   "name"
    t.text     "description"
    t.text     "location"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer  "repeat_type",     default: 1
    t.integer  "repeat_interval"
    t.date     "repeat_until"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "instructor_id"
  end

  add_index "events", ["ends_at"], name: "index_events_on_ends_at"
  add_index "events", ["repeat_until"], name: "index_events_on_repeat_until"
  add_index "events", ["starts_at"], name: "index_events_on_starts_at"

  create_table "exclusions", force: true do |t|
    t.integer  "event_id"
    t.date     "exclude_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "exclusions", ["event_id"], name: "index_exclusions_on_event_id"

  create_table "order_items", force: true do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.decimal  "price",      precision: 16, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_id"
    t.date     "starts_on"
  end

  create_table "orders", force: true do |t|
    t.integer  "school_id"
    t.integer  "user_id"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "placements", force: true do |t|
    t.integer  "course_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "placements", ["course_id"], name: "index_placements_on_course_id"
  add_index "placements", ["product_id"], name: "index_placements_on_product_id"

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "credit_value"
    t.decimal  "price",        precision: 16, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "school_id"
    t.boolean  "unlimited",                             default: false
  end

  create_table "registrations", force: true do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.date     "starts_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "credit_type", default: "credit"
    t.integer  "credit_id"
  end

  add_index "registrations", ["event_id"], name: "index_registrations_on_event_id"
  add_index "registrations", ["user_id", "event_id", "starts_on"], name: "index_registrations_on_user_id_and_event_id_and_starts_on", unique: true
  add_index "registrations", ["user_id"], name: "index_registrations_on_user_id"

  create_table "schools", force: true do |t|
    t.string   "name"
    t.string   "identifier"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "address"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "country",                   default: "USA"
    t.string   "zipcode"
    t.string   "email"
    t.string   "phone"
    t.string   "gplus_url"
    t.string   "facebook_url"
    t.string   "twitter_url"
    t.string   "header_image_file_name"
    t.string   "header_image_content_type"
    t.integer  "header_image_file_size"
    t.datetime "header_image_updated_at"
    t.string   "logo_image_file_name"
    t.string   "logo_image_content_type"
    t.integer  "logo_image_file_size"
    t.datetime "logo_image_updated_at"
    t.string   "merchant_account_id"
    t.string   "legal_name"
    t.boolean  "tos_accepted"
    t.string   "merchant_state"
    t.string   "timezone",                  default: "Eastern Time (US & Canada)"
  end

  add_index "schools", ["identifier"], name: "index_schools_on_identifier", unique: true

  create_table "users", force: true do |t|
    t.string   "email"
    t.string   "password_digest"
    t.string   "auth_provider"
    t.string   "auth_uid"
    t.integer  "role",                       default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "profile_image_file_name"
    t.string   "profile_image_content_type"
    t.integer  "profile_image_file_size"
    t.datetime "profile_image_updated_at"
    t.text     "description"
    t.date     "date_of_birth"
    t.string   "phone"
    t.string   "login_token"
    t.string   "reset_password_token"
    t.string   "state"
    t.string   "invite_token"
    t.string   "name"
    t.boolean  "terms_accepted",             default: false
    t.string   "vault_token"
  end

  add_index "users", ["auth_provider", "auth_uid"], name: "index_users_on_auth_provider_and_auth_uid", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["id", "login_token"], name: "index_users_on_id_and_login_token", unique: true

end
