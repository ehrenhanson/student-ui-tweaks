# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create!([{
  name: 'David Heinemeier Hansson',
  email: 'david@example.com',
  password: 'password',
  description: 'Creator of Ruby on Rails, founder & CTO at Basecamp (formerly 37signals), best-selling author, public speaker, race-car driver, hobbyist photographer, and family man.',
  profile_image: File.new(Rails.root.join('test/fixtures/rails.png')),
  billing_address: BillingAddress.new(
    address: '1 Main St',
    city: 'Chicago',
    state: 'Illinois',
    zipcode: '60007',
    country: 'US'
  ),
  terms_accepted: true,
  state: 'registered'
},
{
  name: 'Matthew Roosevelt',
  email: 'matt@example.com',
  password: 'password',
  description: 'Aspiring student.',
  terms_accepted: true,
  state: 'registered'
},
{
  name: 'Admin Account',
  email: 'admin@example.com',
  password: 'password',
  description: 'General Admin Account',
  role: Role.admin,
  terms_accepted: true,
  state: 'registered'
}])

schools = School.create!([{
  name: 'Rails School',
  identifier: 'rails',
  description: 'Learn how to develop web applications in Ruby on Rails. Covering the basics all the way up to advanced topics, including contributing to the Rails framework open source project hosted on GitHub.',
  address: Address.new(
    address: '1 Main St',
    city: 'Chicago',
    state: 'Illinois',
    zipcode: '60007',
    country: 'US'
  ),
  email: 'rails.school@example.com',
  phone: '5555551234',
  gplus_url: 'https://plus.google.com/',
  facebook_url: 'https://facebook.com/RailsSchool',
  twitter_url: 'https://twitter.com/learnrails',
  logo_image: File.new(Rails.root.join('test/fixtures/logo.png')),
  header_image: File.new(Rails.root.join('test/fixtures/landscape.jpg')),
  merchant_state: 'approved'
},
{
  name: 'Startup School',
  identifier: 'startups',
  description: 'Learn how to build a profitable technology business',
  address: Address.new(
    address: '1 Main St',
    city: 'Chicago',
    state: 'Illinois',
    zipcode: '60007',
    country: 'US'
  ),
  email: 'startup.school@example.com',
  phone: '5555551234',
  gplus_url: 'https://plus.google.com/',
  facebook_url: 'https://facebook.com/RailsSchool',
  twitter_url: 'https://twitter.com/learnrails'
}])

Affiliation.create!([{
  user: users[0],
  school: schools[0],
  role: Role.owner
},
{
  user: users[0],
  school: schools[1],
  role: Role.owner
},
{
  user: users[1],
  school: schools[0],
  role: Role.student
}])

courses = Course.create!([{
  school: schools[0],
  name: 'ActiveRecord 101',
  description: 'Introduction to data modeling and querying SQL databases',
  level: 1
},
{
  school: schools[0],
  name: 'Models, Views, and Controllers (MVC) 204',
  description: 'Understand how to structure your application',
  level: 2
},
{
  school: schools[0],
  name: 'Long Running Processes 302',
  description: 'Learn how to deal with tasks that run for a long period of time',
  level: 2
},
{
  school: schools[1],
  name: 'SaaS: Software as a Service',
  description: 'Find out about recurring revenue models',
  level: 1
}])

Event.create!([{
  course: courses[0],
  instructor: users[0],
  name: courses[0].name,
  description: courses[0].description,
  starts_at: Time.mktime(2014, 1, 1, 13, 0, 0).utc,
  ends_at: Time.mktime(2014, 1, 1, 14, 0, 0).utc,
  repeat_type: Event::REPEAT_DAILY
},
{
  course: courses[1],
  instructor: users[0],
  name: courses[1].name,
  description: courses[1].description,
  starts_at: Time.mktime(2014, 1, 1, 14, 0, 0).utc,
  ends_at: Time.mktime(2014, 1, 1, 15, 0, 0).utc,
  repeat_type: Event::REPEAT_WEEKLY,
  repeat_interval: Event::WEDNESDAY | Event::FRIDAY
},
{
  course: courses[2],
  instructor: users[0],
  name: courses[2].name,
  description: courses[2].description,
  starts_at: Time.mktime(2014, 1, 2, 15, 0, 0).utc,
  ends_at: Time.mktime(2014, 1, 2, 16, 0, 0).utc,
  repeat_interval: Event::REPEAT_MONTHLY
},
{
  course: courses[3],
  instructor: users[0],
  name: courses[3].name,
  description: courses[3].description,
  starts_at: Time.mktime(2014, 1, 1, 1, 0, 0).utc,
  ends_at: Time.mktime(2014, 1, 1, 4, 0, 0).utc,
  repeat_type: Event::REPEAT_WEEKLY,
  repeat_interval: Event::MONDAY
}])

products = Product.create!([{
  school: schools[0],
  name: 'Product (1 Credit Pack)',
  description: 'One credit, one class',
  credit_value: 1,
  price: 10
},
{
  school: schools[0],
  name: 'Product (5 Credit Pack)',
  description: 'Five class pack',
  credit_value: 5,
  price: 40
}])

placements = Placement.create!([{
  product: products[0],
  course: courses[0]
},
{
  product: products[1],
  course: courses[0]
},
{
  product: products[0],
  course: courses[1]
}])