class AlterProductsRemoveCourseId < ActiveRecord::Migration
  def change
    remove_column :products, :course_id, :belongs_to
  end
end
