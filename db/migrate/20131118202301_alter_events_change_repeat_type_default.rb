class AlterEventsChangeRepeatTypeDefault < ActiveRecord::Migration
  def change
    change_column :events, :repeat_type, :integer, default: 1
  end
end
