class CreateAppearances < ActiveRecord::Migration
  def change
    create_table :appearances do |t|
      t.belongs_to :course, index: true
      t.belongs_to :affiliation, index: true
      t.string :name
      t.string :email
      t.text :notes
      t.integer :payment_status

      t.timestamps
    end
  end
end
