class UpdatePlacementsAddCoursesAndProducts < ActiveRecord::Migration
  class Product < ActiveRecord::Base
    self.table_name = 'products'
  end
  
  class Placement < ActiveRecord::Base
    self.table_name = 'placements'
  end
  
  def change
    Placement.transaction do
      Product.find_each do |product|
        Placement.create(product: product, course: product.course)
        print '.'
      end

      puts
    end
  end
end
