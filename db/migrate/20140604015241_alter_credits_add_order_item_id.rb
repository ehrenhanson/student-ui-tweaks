class AlterCreditsAddOrderItemId < ActiveRecord::Migration
  def change
    change_table :credits do |t|
      t.belongs_to :order_item
    end
  end
end
