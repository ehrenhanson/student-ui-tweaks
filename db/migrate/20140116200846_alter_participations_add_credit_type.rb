class AlterParticipationsAddCreditType < ActiveRecord::Migration
  def change
    change_table :participations do |t|
      t.string :credit_type, default: 'credit'
    end
  end
end
