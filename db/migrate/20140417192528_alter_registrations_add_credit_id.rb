class AlterRegistrationsAddCreditId < ActiveRecord::Migration
  def change
    change_table :registrations do |t|
      t.belongs_to :credit
    end
  end
end
