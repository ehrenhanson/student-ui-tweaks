class AlterUsersAddLoginToken < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :login_token
    end
    add_index :users, [ :id, :login_token ], unique: true
  end
end
