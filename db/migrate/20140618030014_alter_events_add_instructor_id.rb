class AlterEventsAddInstructorId < ActiveRecord::Migration
  def change
    change_table :events do |t|
      t.belongs_to :instructor
    end
  end
end
