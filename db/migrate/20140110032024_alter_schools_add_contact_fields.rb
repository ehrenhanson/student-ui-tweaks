class AlterSchoolsAddContactFields < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.string :address
      t.string :address_2
      t.string :city
      t.string :state
      t.string :country, default: 'USA'
      t.string :zipcode
      t.string :email
      t.string :phone  
    end    
  end
end
