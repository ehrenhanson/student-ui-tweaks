class UpdateUsersMigrateNames < ActiveRecord::Migration
  class User < ActiveRecord::Base
    self.table_name = 'users'
  end
  
  def up
    User.transaction do
      User.find_each do |user|
        user.name = [ user.first_name, user.last_name ].join(' ')
        user.save!
        print '.'
      end
    end
    puts
  end
  
  def down
    User.transaction do
      User.find_each do |user|
        user.first_name, user.last_name = FullNameSplitter.split(user.name)
        user.save!
        print '.'
      end
    end
    puts
  end
end
