class AlterUsersAddInviteToken < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :invite_token
    end
  end
end
