class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.string :identifier
      t.text :description

      t.timestamps
    end
    
    add_index :schools, :identifier, unique: true
  end
end
