class CreatePlacements < ActiveRecord::Migration
  def change
    create_table :placements do |t|
      t.belongs_to :course, index: true
      t.belongs_to :product, index: true

      t.timestamps
    end
  end
end
