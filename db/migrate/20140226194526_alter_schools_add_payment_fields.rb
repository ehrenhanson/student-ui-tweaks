class AlterSchoolsAddPaymentFields < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.string :merchant_account_id
      t.string :legal_name
      t.boolean :tos_accepted
    end
  end
end
