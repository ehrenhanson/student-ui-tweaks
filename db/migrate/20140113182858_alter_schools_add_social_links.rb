class AlterSchoolsAddSocialLinks < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.string :gplus_url
      t.string :facebook_url
      t.string :twitter_url
    end
  end
end
