class AlterCoursesRemoveUserId < ActiveRecord::Migration
  def change
    remove_index :courses, :user_id
    remove_column :courses, :user_id
  end
end
