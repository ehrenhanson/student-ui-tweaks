class AlterAffiliationsAddUniqueUserSchool < ActiveRecord::Migration
  def change
    add_index :affiliations, [ :school_id, :user_id ], unique: true
  end
end
