class UpdateUsersSetLoginToken < ActiveRecord::Migration
  class User < ActiveRecord::Base
    self.table_name = 'users'
  end
  
  def up
    User.transaction do
      User.find_each do |user|
        print '.'
        user.login_token = SecureRandom.hex
        user.save!
      end
    end
    puts
  end
end
