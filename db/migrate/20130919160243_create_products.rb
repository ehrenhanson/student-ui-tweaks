class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.belongs_to :course, index: true
      t.string :name
      t.string :description
      t.integer :credit_value
      t.decimal :price, precision: 16, scale: 2 

      t.timestamps
    end
  end
end
