class AlterProductsAddSchoolId < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.belongs_to :school
    end
  end
end
