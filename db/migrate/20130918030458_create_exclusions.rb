class CreateExclusions < ActiveRecord::Migration
  def change
    create_table :exclusions do |t|
      t.belongs_to :event, index: true
      t.date :exclude_on

      t.timestamps
    end
  end
end
