class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.belongs_to :product, index: true
      t.belongs_to :user, index: true
      t.decimal :amount

      t.timestamps
    end
  end
end
