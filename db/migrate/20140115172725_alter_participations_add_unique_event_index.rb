class AlterParticipationsAddUniqueEventIndex < ActiveRecord::Migration
  def change
    add_index :participations, [ :user_id, :event_id, :starts_on ], unique: true
  end
end
