class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :password_digest
      t.string :auth_provider
      t.string :auth_uid
      t.integer :role, default: 0

      t.timestamps
    end
    
    add_index :users, :email, unique: true
    add_index :users, [ :auth_provider, :auth_uid ], unique: true
  end
end
