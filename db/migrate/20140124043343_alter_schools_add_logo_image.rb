class AlterSchoolsAddLogoImage < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.attachment :logo_image
    end
  end
end
