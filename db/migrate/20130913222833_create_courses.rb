class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.belongs_to :school, index: true
      t.belongs_to :user, index: true
      t.string :name
      t.text :description
      t.integer :level, default: 1

      t.timestamps
    end
  end
end
