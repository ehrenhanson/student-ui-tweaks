class UpdateUsersSetState < ActiveRecord::Migration
  class User < ActiveRecord::Base
    self.table_name = 'users'
  end
  
  def up
    User.transaction do
      User.find_each do |user|
        if user.password_digest.present?
          user.state = 'registered'
        elsif user.auth_uid.present?
          user.state = 'authorized'
        else
          user.state = 'created'
        end
        print '.'
        user.save!
      end
      puts
    end
  end
end
