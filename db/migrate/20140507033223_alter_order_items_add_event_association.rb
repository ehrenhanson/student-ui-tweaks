class AlterOrderItemsAddEventAssociation < ActiveRecord::Migration
  def change
    change_table :order_items do |t|
      t.belongs_to :event
      t.date :starts_on
    end
  end
end
