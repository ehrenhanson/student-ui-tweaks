class CreateParticipations < ActiveRecord::Migration
  def change
    create_table :participations do |t|
      t.belongs_to :user, index: true
      t.belongs_to :event, index: true
      t.date :starts_on, index: true
      t.timestamps
    end
  end
end
