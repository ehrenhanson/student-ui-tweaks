class AlterCreditsAddUnlimited < ActiveRecord::Migration
  def change
    change_table :credits do |t|
      t.boolean :unlimited, default: false
    end
  end
end
