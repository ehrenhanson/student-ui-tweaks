class AlterSchoolsAddHeaderImage < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.attachment :header_image
    end
  end
end
