class AlterUsersAddResetPasswordToken < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :reset_password_token
    end
  end
end
