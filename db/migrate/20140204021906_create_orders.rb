class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.belongs_to :school
      t.belongs_to :user
      
      t.string :state

      t.timestamps
    end
  end
end
