class CreateAffiliations < ActiveRecord::Migration
  def change
    create_table :affiliations do |t|
      t.belongs_to :user, index: true
      t.belongs_to :school, index: true
      t.integer :role, default: 0

      t.timestamps
    end
  end
end
