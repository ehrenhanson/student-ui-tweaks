class AlterAddressesSetDefaultCountry < ActiveRecord::Migration
  def change
    change_column :addresses, :country, :string, default: 'US'
  end
end
