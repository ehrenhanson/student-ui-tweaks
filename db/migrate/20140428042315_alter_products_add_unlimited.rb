class AlterProductsAddUnlimited < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.boolean :unlimited, default: false
    end
  end
end
