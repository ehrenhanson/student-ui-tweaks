class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.belongs_to :product, index: true
      t.belongs_to :user, index: true
      t.integer :value

      t.timestamps
    end
  end
end
