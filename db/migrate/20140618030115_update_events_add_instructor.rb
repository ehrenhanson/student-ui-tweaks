class UpdateEventsAddInstructor < ActiveRecord::Migration
  class Course < ActiveRecord::Base
    self.table_name = 'courses'
    has_many :events
  end
  
  class Event < ActiveRecord::Base
    self.table_name = 'events'
    belongs_to :course 
  end
  
  def up
    Event.transaction do
      Course.find_each do |course|        
        course.events.update_all(instructor_id: course.user_id)
        print "."
      end
    end
    puts
  end
  
  def down
    Course.transaction do
      Event.find_each do |event|
        course = event.course
        course.user_id = event.instructor_id
        course.save!
        print "."
      end
    end
    puts
  end  
end
