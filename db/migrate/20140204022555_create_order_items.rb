class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.belongs_to :order
      t.belongs_to :product
      t.decimal :price, precision: 16, scale: 2

      t.timestamps
    end
  end
end
