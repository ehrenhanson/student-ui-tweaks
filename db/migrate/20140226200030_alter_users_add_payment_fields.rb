class AlterUsersAddPaymentFields < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.date :date_of_birth
      t.string :phone
    end
  end
end
