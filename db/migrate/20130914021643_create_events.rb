class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.belongs_to :course
      t.string :name
      t.text :description
      t.text :location
      t.datetime :starts_at
      t.datetime :ends_at
      t.integer :repeat_type, default: 0
      t.integer :repeat_interval
      t.date :repeat_until

      t.timestamps
    end
    
    add_index :events, :starts_at
    add_index :events, :ends_at
    add_index :events, :repeat_until
  end
end
