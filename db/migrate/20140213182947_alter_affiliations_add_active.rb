class AlterAffiliationsAddActive < ActiveRecord::Migration
  def change
    change_table :affiliations do |t|
      t.boolean :active, default: false
    end
  end
end
