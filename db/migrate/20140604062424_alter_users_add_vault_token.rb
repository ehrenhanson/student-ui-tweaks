class AlterUsersAddVaultToken < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :vault_token
    end
  end
end
