class AlterSchoolsAddTimezone < ActiveRecord::Migration
  def change
    change_table :schools do |t|
      t.string :timezone, default: Timezone::DEFAULT_TIME_ZONE
    end
  end
end
