class RenameParticipationToRegistration < ActiveRecord::Migration
  def change
    rename_table :participations, :registrations
  end
end
