class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.integer :addressable_id
      t.string :addressable_type
      
      t.string :address
      t.string :address_2
      t.string :city
      t.string :state
      t.string :country
      t.string :zipcode
      t.string :address_type
      
      t.timestamps
      
      t.index :addressable_id
      t.index :addressable_type
      t.index :address_type
    end
  end
end
