# Viewcy Core README

## Installation

*To Come* 

### Dependencies:

  * Imagemagick (http://www.imagemagick.org)

## Rake Tips 

To see all rake tasks, built in and app local 

    rake -T

Seed the database with seeds.rb
    
    rake db:seed

Reset the database fully

    rake db:reset

## Useful Console Commands

Return a users state

    User.find_by_email('user@domain.com').state

Reset a users password

    User.find_by_email('user@domain.com').update_attributes!(password: 'password')
    
## Test Information (for BrainTree testing)

    Valid Credit Card # -> 5105105105105100, 05/2019