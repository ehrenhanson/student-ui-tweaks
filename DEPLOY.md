# Deploying Viewcy-core

## Deploy Requirements:
+ Bundler

## Pre-Deploy Requirements:
+ DevOps must ensure that your ssh public key exists in the authorized_keys for the user "cap" on the target server.

# Instructions:

If you have't already, use bundler to install the capistrano gem.  Assumes Capitrano appears in your gemfile
> bundle install

Verify that you Capistrano tasks are available
> bundle exec cap -T

(Optional)
Verify that you are able to connect to the target server as the "cap" user
> bundle exec cap staging deploy:check

Full Deploy
> bundle exec cap staging deploy

Execute Capistrano tasks
> bundle exec cap <environment> <namespace:task>

# Useful tasks

> bundle exec cap staging unicorn:stop
> 
> bundle exec cap staging unicorn:start
> 
> bundle exec cap staging unicorn:restart

*note on unicorn tasks* 
The full deploy command handles a greceful restart of the unicorn master, including killing worker threads. 
It also gracefully reloads nginx.  Thus, these tasks should never need to be run afer a full deploy.
They are useful, however, in cases where the unicorn threads need to be restarted.
