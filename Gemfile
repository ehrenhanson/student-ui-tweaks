source 'https://rubygems.org'

gem 'capistrano', '3.1.0', require: false, group: :development

 #use unicorn for the app server
 gem 'unicorn'

group :development do
  gem 'capistrano-rvm',   '~> 0.1', require: false
  gem 'capistrano-rails',   '~> 1.1', require: false
  gem 'capistrano-bundler', '~> 1.1', require: false
  gem 'capistrano3-unicorn'
  gem 'capistrano3-nginx', '~> 1.0'
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.1'

group :sqlite do
  # Use sqlite3 as the database for Active Record
  gem 'sqlite3'
end

group :mysql do
  gem 'mysql2'
end

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Include Backbone JavaScript libraries
gem 'backbone-rails'
gem 'underscore-string-rails'
gem 'momentjs-rails'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Use state_machine to track model states
gem 'state_machine'

# Use simple_form to generate labeled forms
gem 'simple_form'
gem 'country_select'

# Use full name splitter to extract first/last name from supplied names
gem 'full-name-splitter'

# Lists of countries/states
gem 'carmen'

# URI replacement
gem 'addressable', require: 'addressable/uri'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.1.0'

# Use to validate emails in a ActiveModel validation routine
gem 'email_validator'

# Use omniauth to use multiple authentication providers
gem 'omniauth'
gem 'omniauth-facebook'
gem 'omniauth-twitter'

# Use runt for recurring event temporal expressions
# Note: The runt library attempts to overwrite the Fixnum time helpers defined by ActiveSupport. Moving the require to application.rb to load the library before the rest of the application resolves this.
gem 'runt', require: false

# Use week of month for generating Runt expressions
gem 'week_of_month'

# Use paperclip to handle file attachments
gem 'paperclip'
gem 'cocaine'

# Use money to format currencies
gem 'money'
gem 'monetize'

# Use braintree to accept payments
gem 'braintree'

# Development support gems
gem 'seed_dump'
gem 'rack-reverse-proxy', require: 'rack/reverse_proxy'

group :test do
  # Use minitest spec as the testing framework
  gem 'minitest-spec-rails'
  gem 'mocha', require: false

  # Use factory girl for testing data fixtures
  gem 'factory_girl_rails'
  
  # Include autotest for use in running the test suite
  gem 'autotest'
  
  # Include performance testing that was extracted out of Rails
  gem 'rails-perftest'
  gem 'ruby-prof'
end
